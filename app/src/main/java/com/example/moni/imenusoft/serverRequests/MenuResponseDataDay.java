package com.example.moni.imenusoft.serverRequests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by enuke on 10/10/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class MenuResponseDataDay implements Serializable {

    /**
     *     {
     "day_id":"0",
     "day_value":"Monday",
     "day_data":[]
     }
     */

    @SerializedName("day_id")
    @Expose
    private String day_id;

    @SerializedName("day_value")
    @Expose
    private String day_value;

    @SerializedName("day_data")
    @Expose
    private ArrayList<MenuResponseDataMeal> day_data;

    public MenuResponseDataDay(String day_id, String day_value, ArrayList<MenuResponseDataMeal> day_data) {
        this.day_id = day_id;
        this.day_value = day_value;
        this.day_data = day_data;
    }

    public MenuResponseDataDay() {
    }

    public String getDay_id() {
        return day_id;
    }

    public void setDay_id(String day_id) {
        this.day_id = day_id;
    }

    public String getDay_value() {
        return day_value;
    }

    public void setDay_value(String day_value) {
        this.day_value = day_value;
    }

    public ArrayList<MenuResponseDataMeal> getDay_data() {
        return day_data;
    }

    public void setDay_data(ArrayList<MenuResponseDataMeal> day_data) {
        this.day_data = day_data;
    }
}
