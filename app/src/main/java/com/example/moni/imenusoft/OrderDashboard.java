package com.example.moni.imenusoft;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.firebasenotification.MyFirebaseMessagingService;
import  com.example.moni.imenusoft.firebasenotification.OnNotificationReceived;
import  com.example.moni.imenusoft.firebasenotification.notificationJson.NotificationResponse;
import  com.example.moni.imenusoft.retrofit.loader.GetOrderLoader;
import  com.example.moni.imenusoft.retrofit.loader.MenuLoader;
import  com.example.moni.imenusoft.serverRequests.GetOrderRequest;
import  com.example.moni.imenusoft.serverRequests.MenuRequest;
import  com.example.moni.imenusoft.serverRequests.MenuResponse;
import  com.example.moni.imenusoft.serverRequests.MenuResponseCategory;
import  com.example.moni.imenusoft.serverRequests.MenuResponseData;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataDay;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataMeal;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.OrderDetailResponse;
import  com.example.moni.imenusoft.utils.Preference;
import  com.example.moni.imenusoft.utils.Utility;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.gson.Gson;

import java.io.Serializable;

import okhttp3.internal.Util;

/**
 * Created by enuke on 30/9/17.
 */

public class OrderDashboard extends AppCompatActivity {

    private TextView startOrder;
    private TextView logout;
    private ProgressDialog progressBar;
    MenuRequest menuRequest;
    GetOrderRequest getOrderRequestObj;
    MenuResponse menuResponseObj;

    BroadcastReceiver notificationBroadCastReceiver;


    private LoaderManager.LoaderCallbacks<MenuResponse> responseMenuCallback = new LoaderManager.LoaderCallbacks<MenuResponse>() {
        @Override
        public Loader<MenuResponse> onCreateLoader(int id, Bundle args) {
            return new MenuLoader(OrderDashboard.this, menuRequest);
        }

        @Override
        public void onLoadFinished(Loader<MenuResponse> loader, MenuResponse data) {


            progressBar.dismiss();


            if (data != null) {
                Log.d("json obj", "value :" + data.toString());
                if (data.isSuccess()) {

                    menuResponseObj = data;

                } else {

                    Utility.showSnackbar(root, data.getMassage());
                }
            }
            getSupportLoaderManager().destroyLoader(0);
        }


        @Override
        public void onLoaderReset(Loader<MenuResponse> loader) {

        }
    };
    private RelativeLayout root;
    private TextView viewOrder;
    private LoaderManager.LoaderCallbacks<OrderDetailResponse> responseGetOrderCallback = new LoaderManager.LoaderCallbacks<OrderDetailResponse>() {
        @Override
        public Loader<OrderDetailResponse> onCreateLoader(int id, Bundle args) {

            return new GetOrderLoader(OrderDashboard.this,getOrderRequestObj);
            }

        @Override
        public void onLoadFinished(Loader<OrderDetailResponse> loader, OrderDetailResponse data) {

            progressBar.dismiss();
            getSupportLoaderManager().destroyLoader(1);

            if(data!=null) {
                if (data.getSuccess()) {

                    if (data.getData()!=null){
                        Intent intent = new Intent(OrderDashboard.this, PatientList.class);
                        intent.putExtra("data", (Serializable) data.getData());
                        intent.putExtra("data_color", "default");
                        intent.putExtra("data_menu", menuResponseObj);
                        startActivity(intent);

                    } else {
                        Utility.showSnackbar(root,"No Order Found. Create some order(s)");
                    }
                } else {
                    Utility.showSnackbar(root, "Ops !! something went wrong, We are trying to fix");
                }
            } else{
                Utility.showSnackbar(root, "Some thing went wrong. Try again!!");

            }


        }

        @Override
        public void onLoaderReset(Loader<OrderDetailResponse> loader) {

        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.order_dashboard);
        initView();

        if (Utility.isInternetConnected(OrderDashboard.this)) {
            progressBar.setCancelable(false);
            progressBar.setTitle("Loading");
            progressBar.setMessage("Please wait...");
            progressBar.show();
            getServerRequest();
        } else {
            Utility.showToast(OrderDashboard.this, "No internet Connectivity");
        }


    }


    private void initView() {

        progressBar = new ProgressDialog(OrderDashboard.this);

        startOrder = findViewById(R.id.startOrder);

        startOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //
                if(menuResponseObj!=null) {
                    Intent intent = new Intent(OrderDashboard.this, Menu.class);
                    intent.putExtra("data", menuResponseObj);
                    startActivity(intent);
                } else{
                    Utility.showSnackbar(root,"Something went Wrong try again !");
                }

            }
        });

        logout = findViewById(R.id.logout);

        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.doActionLogout(OrderDashboard.this);
            }
        });
        root = findViewById(R.id.root);
        viewOrder = findViewById(R.id.viewOrder);

        viewOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utility.isInternetConnected(OrderDashboard.this)) {
                    progressBar.setCancelable(false);
                    progressBar.setTitle("Loading");
                    progressBar.setMessage("Please wait...");
                    progressBar.show();
                    getOrderDetail();
                } else {
                    Utility.showToast(OrderDashboard.this, "No internet Connectivity");
                }

            }
        });
    }

    private void getServerRequest() {

        menuRequest = new MenuRequest(Preference.getId(), Preference.getHospitalId());

        getSupportLoaderManager().initLoader(0, null, responseMenuCallback);

    }

    private void getOrderDetail() {

        getOrderRequestObj = new GetOrderRequest(Preference.getId(), Preference.getHospitalId(), "hospital");

        getSupportLoaderManager().initLoader(1, null, responseGetOrderCallback);
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MyAction");

        notificationBroadCastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                //extract our message from intent
                String msg_for_me = intent.getStringExtra("notification");
                //log our message value

                NotificationResponse notificationResponse = new Gson().fromJson(msg_for_me,NotificationResponse.class);






               if(notificationResponse.getChangedata()!=null){
                   if(notificationResponse.getChangedata().size()>0){

                       String position = "";
                       for( MenuResponseData menuResponseDataObj :menuResponseObj.getData()){

                           for(MenuResponseDataDay menuResponseDataDay :menuResponseDataObj.getWeek_data()){

                               for(MenuResponseDataMeal menuResponseDataMeal :menuResponseDataDay.getDay_data()){

                                   for(MenuResponseCategory menuResponseCategory :menuResponseDataMeal.getMeal_data()){

                                       if(menuResponseCategory.getCategory_id().equalsIgnoreCase(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getCategory_id())){
                                           menuResponseCategory.getCategory_data().clear();
                                           menuResponseCategory.getCategory_data().addAll(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getCategory_data());
                                           position = menuResponseCategory.getCategory_id();
                                       }
                                   }
                               }
                           }
                       }

                       if(position.equalsIgnoreCase("")){

                           for( MenuResponseData menuResponseDataObj :menuResponseObj.getData()){

                               for(MenuResponseDataDay menuResponseDataDay :menuResponseDataObj.getWeek_data()){

                                   for(MenuResponseDataMeal menuResponseDataMeal :menuResponseDataDay.getDay_data()){

                                       if(menuResponseDataMeal.getMeal_id().equalsIgnoreCase(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_id())){
                                           menuResponseDataMeal.getMeal_data().addAll(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data());
                                       }
                                   }
                               }
                           }

                       }

                       Utility.showSnackbar(root,position);

                   }
               }  else {
                   if(notificationResponse.getId()!=null){

                       for( MenuResponseData menuResponseDataObj :menuResponseObj.getData()){

                           for(MenuResponseDataDay menuResponseDataDay :menuResponseDataObj.getWeek_data()){

                               for(MenuResponseDataMeal menuResponseDataMeal :menuResponseDataDay.getDay_data()){

                                       menuResponseDataMeal.setMeal_data(Utility.removeDataFromArrayList(menuResponseDataMeal.getMeal_data(),notificationResponse.getId()));

                               }
                           }
                       }
                   }
               }

            }
        };
        //registering our receiver
        this.registerReceiver(notificationBroadCastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        this.unregisterReceiver(notificationBroadCastReceiver);
    }


}
