package com.example.moni.imenusoft.retrofit.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import com.example.moni.imenusoft.retrofit.CommunicationLink;
import com.example.moni.imenusoft.serverRequests.CreateOrderResponse;
import com.example.moni.imenusoft.serverRequests.OrderRequest;
import com.google.gson.GsonBuilder;

/**
 * Created by enuke on 7/11/17.
 */

public class OrderGenerateLoader extends AsyncTaskLoader<CreateOrderResponse> {

    private Context mContext;
    private String accessToken;
    private int interestId;
    OrderRequest jsonObject;

    public OrderGenerateLoader(Context context, OrderRequest jsonObject) {
        super(context);
        mContext = context;
        this. accessToken=accessToken;
        this.interestId=interestId;
        this.jsonObject = jsonObject;
    }



    @Override
    public CreateOrderResponse loadInBackground() {
        try {
            String json = new GsonBuilder().create().toJson(jsonObject);
            Log.i("request",""+json);
            CreateOrderResponse responsePlan = CommunicationLink.orderNew(jsonObject);
            return responsePlan;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
        protected void onStartLoading() {
        forceLoad();
    }

}
