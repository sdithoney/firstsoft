package com.example.moni.imenusoft.serverRequests.orderServerRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class Order_details implements Serializable {

  @SerializedName("Hospital_data")
  @Expose
  private List<Hospital_data> Hospital_data;
  @SerializedName("hospital_id")
  @Expose
  private String hospital_id;
  @SerializedName("hospital_name")
  @Expose
  private String hospital_name;


  public Order_details(){
  }
  public Order_details(List<Hospital_data> Hospital_data, String hospital_id, String hospital_name){
   this.Hospital_data=Hospital_data;
   this.hospital_id=hospital_id;
   this.hospital_name=hospital_name;
  }
  public void setHospital_data(List<Hospital_data> Hospital_data){
   this.Hospital_data=Hospital_data;
  }
  public List<Hospital_data> getHospital_data(){
   return Hospital_data;
  }
  public void setHospital_id(String hospital_id){
   this.hospital_id=hospital_id;
  }
  public String getHospital_id(){
   return hospital_id;
  }
  public void setHospital_name(String hospital_name){
   this.hospital_name=hospital_name;
  }
  public String getHospital_name(){
   return hospital_name;
  }
}