package com.example.moni.imenusoft.retrofit;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.utils.NoConnectivityException;
import  com.example.moni.imenusoft.utils.Utility;

import java.io.IOException;
import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by enuke on 7/18/17.
 */


public class ConnectivityInterceptor implements Interceptor {

    private Context mContext;

    public ConnectivityInterceptor(Context context) {
        mContext = context;
        Log.d("InternetConnectivity","true");
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!Utility.isInternetConnected(mContext)) {


            Handler handler = new Handler(Looper.getMainLooper());
            handler.post(
                    new Runnable()
                    {
                        @Override
                        public void run()
                        {
                            Toast.makeText(mContext, "No internet connection", Toast.LENGTH_SHORT).show();
                        }
                    }
            );

            throw new NoConnectivityException();
        }

        Request.Builder builder = chain.request().newBuilder();
        return chain.proceed(builder.build());
    }



}