package com.example.moni.imenusoft.utils;

/**
 * Created by enuke on 1/10/17.
 */

public class Constants {
    public static final String BASE_URL = "https://www.imenuadmin.com";
    public static final String USER_ID = "user_id";
    public static final String ID = "id";
    public static final String IS_LOGIN = "is_login";
    public static final String USER_OBJECT_JSON = "user_object";
    public static final String HOSPITAL_ID = "hospital_id";
    public static final String USER_NAME = "user_name";
    public static final String NOTIFICATION = "notification";
}
