package com.example.moni.imenusoft.serverRequests;

import com.google.gson.annotations.Expose;

/**
 * Created by enuke on 1/10/17.
 */

public class MenuRequest {

    @Expose
    String hospital_id;

    @Expose
    String user_id;

    public MenuRequest(String user_id, String hospital_id) {
        this.hospital_id = hospital_id;
        this.user_id = user_id;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
}
