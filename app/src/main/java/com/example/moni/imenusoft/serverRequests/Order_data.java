package com.example.moni.imenusoft.serverRequests;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class Order_data{
  @SerializedName("category_id")
  @Expose
  private String category_id;
  @SerializedName("week_id")
  @Expose
  private String week_id;
  @SerializedName("qty")
  @Expose
  private String qty;
  @SerializedName("day_id")
  @Expose
  private String day_id;
  @SerializedName("meal_id")
  @Expose
  private String meal_id;
  @SerializedName("menu_id")
  @Expose
  private String menu_id;

    @SerializedName("status")
    @Expose
    private String status;


  public void setCategory_id(String category_id){
   this.category_id=category_id;
  }
  public String getCategory_id(){
   return category_id;
  }
  public void setWeek_id(String week_id){
   this.week_id=week_id;
  }
  public String getWeek_id(){
   return week_id;
  }
  public void setQty(String qty){
   this.qty=qty;
  }
  public String getQty(){
   return qty;
  }
  public void setDay_id(String day_id){
   this.day_id=day_id;
  }
  public String getDay_id(){
   return day_id;
  }
  public void setMeal_id(String meal_id){
   this.meal_id=meal_id;
  }
  public String getMeal_id(){
   return meal_id;
  }
  public void setMenu_id(String menu_id){
   this.menu_id=menu_id;
  }
  public String getMenu_id(){
   return menu_id;
  }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}