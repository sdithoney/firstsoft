package com.example.moni.imenusoft;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.InputType;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.firebasenotification.notificationJson.NotificationResponse;
import  com.example.moni.imenusoft.pojo.Patient;
import  com.example.moni.imenusoft.serverRequests.MenuResponseCategory;
import  com.example.moni.imenusoft.serverRequests.MenuResponseData;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataDay;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataMeal;
import  com.example.moni.imenusoft.utils.Preference;
import  com.example.moni.imenusoft.utils.Utility;
import  com.example.moni.imenusoft.utils.ValidationManager;
import com.google.gson.Gson;

/**
 * Created by enuke on 30/9/17.
 */

public class PatientDetail extends AppCompatActivity {

    ValidationManager validationManagerObj = new ValidationManager();

    private Button Next;
    private RelativeLayout root;
    private TextView heading;
    private View viewOne;
    private TextView detail;
    private LinearLayout nameLayout;
    private EditText firstName;
    private EditText lastName;
    private LinearLayout bedLayout;
    private EditText bedNumber;
    private EditText ward;
    private EditText foodAllergies;
    private RelativeLayout checkLayout;
    private TextView pleaseTick;
    private CheckBox ch1;
    private CheckBox ch2;
    private CheckBox ch3;
    private CheckBox ch4;
    private TextView adminName;

    MenuResponseData menuResponseData;
    private RadioGroup radioGroup;
    private RadioButton male;
    private RadioButton female;
    private RadioButton others;
    private BroadcastReceiver notificationBroadCastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.patient_detail);

        menuResponseData = (MenuResponseData) getIntent().getSerializableExtra("data");

        initView();


        Next.setBackgroundColor(Utility.getColorCode(menuResponseData.getWeek_color()));
        Next.setTextColor(Color.WHITE);

        viewOne.setBackgroundColor(Utility.getColorCode(menuResponseData.getWeek_color()));

    }


    private void initView() {

        Next = findViewById(R.id.Next);

        Next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (isValidData()) {
                    Intent intent = new Intent(PatientDetail.this, OrderList.class);

                    intent.putExtra("data", menuResponseData);
                    intent.putExtra("user_data", getPatientDetail());
                    startActivity(intent);
                }
            }
        });

        root = findViewById(R.id.root);
        heading = findViewById(R.id.heading);
        viewOne = findViewById(R.id.viewOne);
        detail = findViewById(R.id.detail);
        nameLayout = findViewById(R.id.nameLayout);
        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        bedLayout = findViewById(R.id.bedLayout);
        bedNumber = findViewById(R.id.bedNumber);
        ward = findViewById(R.id.ward);
        foodAllergies = findViewById(R.id.foodAllergies);
        checkLayout = findViewById(R.id.checkLayout);
        pleaseTick = findViewById(R.id.pleaseTick);
        ch1 = findViewById(R.id.ch1);
        ch2 = findViewById(R.id.ch2);
        ch3 = findViewById(R.id.ch3);
        ch4 = findViewById(R.id.ch4);
        adminName = findViewById(R.id.adminName);


        radioGroup = findViewById(R.id.radioGroup);
        male = findViewById(R.id.male);
        female = findViewById(R.id.female);
        others = findViewById(R.id.others);

        firstName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        lastName.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        bedNumber.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        ward.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);
        foodAllergies.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_FLAG_CAP_SENTENCES);


        setUserName();
        setHeaderName();
    }

    private Patient getPatientDetail() {

        Patient patientObj = new Patient();


        patientObj.setFirstname(firstName.getText().toString());
        patientObj.setLastname(lastName.getText().toString());
        patientObj.setGender(((RadioButton) findViewById(radioGroup.getCheckedRadioButtonId())).getText().toString());
        patientObj.setBedNo(bedNumber.getText().toString());
        patientObj.setWardNo(ward.getText().toString());
        patientObj.setFoodAllergies(foodAllergies.getText().toString());

        String type = "";

        if (ch1.isChecked())
            type += ch1.getText().toString();

        if (ch2.isChecked()) {
            if (type.length() == 0)
                type += ch2.getText().toString();
            else
                type += ", " + ch2.getText().toString();
        }

        if (ch3.isChecked()) {
            if (type.length() == 0)
                type += ch3.getText().toString();
            else
                type += ", " + ch3.getText().toString();
        }


        if (ch4.isChecked()) {
            if (type.length() == 0)
                type += ch4.getText().toString();
            else
                type += ", " + ch4.getText().toString();
        }

        patientObj.setServe(type);
        patientObj.setUser(Preference.getUserName());

        return patientObj;

    }

    private boolean isValidData() {

        if (validationManagerObj.isEmpty(firstName.getText().toString())) {
            //   Utility.showSnackbar(root, "Login Id can not be blank.");
            firstName.setError("Enter Patient First Name.");
            firstName.requestFocus();
            return false;
        } else firstName.setError(null);


        if (validationManagerObj.isEmpty(lastName.getText().toString())) {
            //   Utility.showSnackbar(root, "Login Id can not be blank.");
            lastName.setError("Enter Patient Last Name.");
            lastName.requestFocus();
            return false;
        } else lastName.setError(null);


        if (radioGroup.getCheckedRadioButtonId() == -1) {
            // no radio buttons are checked
            Utility.showSnackbar(root, "Enter Patient Gender.");
            radioGroup.requestFocus();
            return false;
        }


        if (validationManagerObj.isEmpty(bedNumber.getText().toString())) {
            //   Utility.showSnackbar(root, "Login Id can not be blank.");
            bedNumber.setError("Enter Patient Bed Number.");
            bedNumber.requestFocus();
            return false;
        } else bedNumber.setError(null);


        if (validationManagerObj.isEmpty(ward.getText().toString())) {
            //   Utility.showSnackbar(root, "Login Id can not be blank.");
            ward.setError("Enter Patient Ward.");
            ward.requestFocus();
            return false;
        } else ward.setError(null);


        return true;
    }

    private void setUserName() {

        adminName.setText("User : " + Preference.getUserName());
    }

    private void setHeaderName() {

        String dateAndTime = Utility.setTimeHeader();

        int weekId = Integer.parseInt(menuResponseData.getWeek_id());

        detail.setText("Week " + (++weekId) + ", " + dateAndTime);
    }


    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MyAction");

        notificationBroadCastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                //extract our message from intent
                String msg_for_me = intent.getStringExtra("notification");
                //log our message value
                Utility.showLog("TAG",msg_for_me);

                NotificationResponse notificationResponse = new Gson().fromJson(msg_for_me,NotificationResponse.class);


                if(notificationResponse.getChangedata()!=null){
                    if(notificationResponse.getChangedata().size()>0){

                        String position = "";


                        for(MenuResponseDataDay menuResponseDataDay :menuResponseData.getWeek_data()){

                            for(MenuResponseDataMeal menuResponseDataMeal :menuResponseDataDay.getDay_data()){

                                for(MenuResponseCategory menuResponseCategory :menuResponseDataMeal.getMeal_data()){

                                    if(menuResponseCategory.getCategory_id().equalsIgnoreCase(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getCategory_id())){
                                        menuResponseCategory.getCategory_data().clear();
                                        menuResponseCategory.getCategory_data().addAll(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getCategory_data());
                                        position = menuResponseCategory.getCategory_id();
                                    }
                                }
                            }
                        }

                        if(position.equalsIgnoreCase("")){


                            for(MenuResponseDataDay menuResponseDataDay :menuResponseData.getWeek_data()){

                                for(MenuResponseDataMeal menuResponseDataMeal :menuResponseDataDay.getDay_data()){

                                    if(menuResponseDataMeal.getMeal_id().equalsIgnoreCase(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_id())){
                                        menuResponseDataMeal.getMeal_data().addAll(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data());
                                    }
                                }
                            }
                        }

                    }
                }  else {
                    if(notificationResponse.getId()!=null){


                            for(MenuResponseDataDay menuResponseDataDay :menuResponseData.getWeek_data()){

                                for(MenuResponseDataMeal menuResponseDataMeal :menuResponseDataDay.getDay_data()){

                                    menuResponseDataMeal.setMeal_data(Utility.removeDataFromArrayList(menuResponseDataMeal.getMeal_data(),notificationResponse.getId()));

                                }
                            }

                    }
                }
            }
        };
        //registering our receiver
        this.registerReceiver(notificationBroadCastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        this.unregisterReceiver(notificationBroadCastReceiver);
    }


}