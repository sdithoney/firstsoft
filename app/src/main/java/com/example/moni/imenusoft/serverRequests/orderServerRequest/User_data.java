package com.example.moni.imenusoft.serverRequests.orderServerRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class User_data implements Serializable {
  @SerializedName("allergies")
  @Expose
  private String allergies;
  @SerializedName("Patient_data")
  @Expose
  private List<Patient_data> Patient_data;
  @SerializedName("bed_num")
  @Expose
  private String bed_num;
  @SerializedName("gender")
  @Expose
  private String gender;
  @SerializedName("patient_id")
  @Expose
  private String patient_id;
  @SerializedName("last_name")
  @Expose
  private String last_name;
  @SerializedName("ward")
  @Expose
  private String ward;
  @SerializedName("first_name")
  @Expose
  private String first_name;

  //serve

  @SerializedName("serve")
  @Expose
  private String serve;

  public User_data(){
  }

    public User_data(String allergies, List< com.example.moni.imenusoft.serverRequests.orderServerRequest.Patient_data> patient_data, String bed_num, String gender, String patient_id, String last_name, String ward, String first_name, String serve) {
        this.allergies = allergies;
        Patient_data = patient_data;
        this.bed_num = bed_num;
        this.gender = gender;
        this.patient_id = patient_id;
        this.last_name = last_name;
        this.ward = ward;
        this.first_name = first_name;
        this.serve = serve;
    }

    public String getAllergies() {
        return allergies;
    }

    public void setAllergies(String allergies) {
        this.allergies = allergies;
    }

    public List< com.example.moni.imenusoft.serverRequests.orderServerRequest.Patient_data> getPatient_data() {
        return Patient_data;
    }

    public void setPatient_data(List< com.example.moni.imenusoft.serverRequests.orderServerRequest.Patient_data> patient_data) {
        Patient_data = patient_data;
    }

    public String getBed_num() {
        return bed_num;
    }

    public void setBed_num(String bed_num) {
        this.bed_num = bed_num;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getPatient_id() {
        return patient_id;
    }

    public void setPatient_id(String patient_id) {
        this.patient_id = patient_id;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getWard() {
        return ward;
    }

    public void setWard(String ward) {
        this.ward = ward;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getServe() {
        return serve;
    }

    public void setServe(String serve) {
        this.serve = serve;
    }
}