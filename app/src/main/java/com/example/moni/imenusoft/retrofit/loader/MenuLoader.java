package com.example.moni.imenusoft.retrofit.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import  com.example.moni.imenusoft.retrofit.CommunicationLink;
import  com.example.moni.imenusoft.serverRequests.LoginRequest;
import  com.example.moni.imenusoft.serverRequests.LoginResponse;
import  com.example.moni.imenusoft.serverRequests.MenuRequest;
import  com.example.moni.imenusoft.serverRequests.MenuResponse;
import com.google.gson.GsonBuilder;

/**
 * Created by enuke on 7/11/17.
 */

public class MenuLoader extends AsyncTaskLoader<MenuResponse> {

    private Context mContext;
    private String accessToken;
    private int interestId;
    MenuRequest jsonObject;

    public MenuLoader(Context context, MenuRequest jsonObject) {
        super(context);
        mContext = context;
        this. accessToken=accessToken;
        this.interestId=interestId;
        this.jsonObject = jsonObject;
    }



    @Override
    public MenuResponse loadInBackground() {
        try {
            String json = new GsonBuilder().create().toJson(jsonObject);
            Log.i("request",""+json);
            MenuResponse responsePlan = CommunicationLink.actionMenuNew(jsonObject);
            return responsePlan;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
        protected void onStartLoading() {
        forceLoad();
    }

}
