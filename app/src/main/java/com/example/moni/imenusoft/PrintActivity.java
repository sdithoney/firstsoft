package com.example.moni.imenusoft;

import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.usb.UsbDevice;
import android.hardware.usb.UsbManager;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import com.example.moni.imenusoft.adapters.PrintAdapter;
import com.example.moni.imenusoft.serverRequests.orderServerRequest.Category_data;
import com.example.moni.imenusoft.serverRequests.orderServerRequest.Data;
import com.example.moni.imenusoft.serverRequests.orderServerRequest.Day_data;
import com.example.moni.imenusoft.serverRequests.orderServerRequest.Hospital_data;
import com.example.moni.imenusoft.serverRequests.orderServerRequest.Meal_data;
import com.example.moni.imenusoft.serverRequests.orderServerRequest.Order_data;
import com.example.moni.imenusoft.serverRequests.orderServerRequest.Order_details;
import com.example.moni.imenusoft.serverRequests.orderServerRequest.Patient_data;
import com.example.moni.imenusoft.serverRequests.orderServerRequest.User_data;
import com.example.moni.imenusoft.serverRequests.orderServerRequest.Week_data;
import com.example.moni.imenusoft.utils.Preference;
import com.example.moni.imenusoft.utils.Utility;
import com.wordpress.ebc81.esc_pos_lib.ESC_POS_EPSON_ANDROID;
import com.wordpress.ebc81.esc_pos_lib.USBPort;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

public class PrintActivity extends AppCompatActivity {

    private TextView mPrintHeader;
    private RelativeLayout mPreviewLayout;
    private TextView mHospitalName;
    private TextView mOrderNumber;
    private TextView mOrderDate;
    private TextView mUser;
    private TextView mPatientDetail;
    private TextView mMealHeader;
    private RecyclerView mOrderList;
    private LinearLayout mPrintLayout;
    private TextView mConnect;
    private TextView mPrint;
    private TextView mWeekId;
    private static final String ACTION_USB_PERMISSION = "com.android.example.USB_PERMISSION";
    private static final String TAG = "ESC_POS_SAMPLE";
    private PendingIntent mPermissionIntent;
    private UsbManager mUsbManager;
    private USBPort mUsbPort;
    private ESC_POS_EPSON_ANDROID mEscPos;
    int vendorId = 0, productId = 0;
    boolean visited = false;
    boolean searchDeviceCalled = false;
    private Map<String ,String > orderStatus;


    Data orderData;
    String patientId = "", orderNumberString = "";
    private RelativeLayout mRoot;
    private ArrayList<Day_data> menuArrayList;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_print);


        initVariables();
        initView();
        initMap();
        fillUI();
        setRecyclerView();
        initPrint();
        bindListeners();
    }



    private void initVariables(){

        if (getIntent().hasExtra("data")) {

            orderData = (Data) getIntent().getSerializableExtra("data");

            patientId = orderData.getOrder_details().get(0).getHospital_data().get(0).getUser_data().get(0).getPatient_id();
            orderNumberString = orderData.getOrder_num();
        }
    }

    private void initMap(){
        orderStatus = new HashMap<>();
        orderStatus.put("0","Created");
        orderStatus.put("1","Completed");
        orderStatus.put("2","Canceled");
    }
    private void initView() {

        mPrintHeader = findViewById(R.id.print_header);
        mPreviewLayout = findViewById(R.id.preview_layout);
        mHospitalName = findViewById(R.id.hospital_name);
        mOrderNumber = findViewById(R.id.order_number);
        mOrderDate = findViewById(R.id.orderDate);
        mUser = findViewById(R.id.user);
        mPatientDetail = findViewById(R.id.patientDetail);
        mMealHeader = findViewById(R.id.meal_header);
        mOrderList = findViewById(R.id.orderList);
        mPrintLayout = findViewById(R.id.print_layout);
        mConnect = findViewById(R.id.connect);
        mPrint = findViewById(R.id.print);
        mRoot = findViewById(R.id.root);
        mWeekId = findViewById(R.id.week_id);
    }

    private void bindListeners(){


        mConnect.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                OnClickButtonConnectDevices();
            }
        });

        mPrint.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(searchDeviceCalled)
                      print();
                else
                    Utility.showSnackbar(mRoot,"Print Error : Device Not Connected");

            }
        });
    }

    private void fillUI() {

        mHospitalName.setText(orderData.getOrder_details().get(0).getHospital_name());
        int weekId = Integer.parseInt(orderData.getOrder_details().get(0).getHospital_data().get(0).getUser_data().get(0).getPatient_data().get(0).getWeek_id());
        mWeekId.setText("Week "+ (++weekId));
        mOrderNumber.setText("ON :" + orderNumberString);
        mOrderDate.setText("Date : " + orderData.getOrder_details().get(0).getHospital_data().get(0).getUser_data().get(0).getPatient_data().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getOrder_date());
        mUser.setText("User : " + Preference.getUserName());
        String patient ="Name :" + getUserDetail().getFirst_name() + " " + getUserDetail().getLast_name() + "\n" +  getUserDetail().getGender().toUpperCase() + " " + "Bed No :" + getUserDetail().getBed_num() + " " + "Ward :" + getUserDetail().getWard() + "\n" + "Notes :" + getUserDetail().getAllergies() + "\n" + "Serve :" + getUserDetail().getServe();
        mPatientDetail.setText("" + patient);
    }

    private User_data getUserDetail() {

        return orderData.getOrder_details().get(0).getHospital_data().get(0).getUser_data().get(0);
    }

    private void setRecyclerView() {


        menuArrayList = new ArrayList();
        for (Order_details order_detailsObj : orderData.getOrder_details()) {
            for (Hospital_data hospital_data : order_detailsObj.getHospital_data()) {

                for (User_data user_data : hospital_data.getUser_data()) {

                    for (Patient_data patient_data : user_data.getPatient_data()) {

                        for (Week_data week_data : patient_data.getWeek_data()) {

                            for (Day_data day_data : week_data.getDay_data()) {

                                day_data.setDayValue(week_data.getDay_value());
                                day_data.setStatus(day_data.getMeal_data().get(0).getOrder_data().get(0).getCategory_data().get(0).getStatus());
                                menuArrayList.add(day_data);
                            }
                        }
                    }
                }
            }

        }

        mOrderList.setLayoutManager(new LinearLayoutManager(PrintActivity.this));
        mOrderList.setAdapter(new PrintAdapter(this,menuArrayList));

    }

    private void initPrint() {
        mUsbManager = (UsbManager) getSystemService(Context.USB_SERVICE);
        mPermissionIntent = PendingIntent.getBroadcast(this, 0, new Intent(ACTION_USB_PERMISSION), 0);
        IntentFilter filter = new IntentFilter(ACTION_USB_PERMISSION);
        registerReceiver(mUsbReceiver, filter);
        filter = new IntentFilter(UsbManager.ACTION_USB_DEVICE_DETACHED);
        registerReceiver(mUsbReceiver, filter);
        filter = new IntentFilter(UsbManager.ACTION_USB_DEVICE_ATTACHED);
        registerReceiver(mUsbReceiver, filter);
        mUsbPort = new USBPort(mUsbManager);
        mEscPos = new ESC_POS_EPSON_ANDROID(mUsbPort);

        OnClickButtonSearchDevices();
    }

    @Override
    public void onDestroy() {
        unregisterReceiver(mUsbReceiver);
        try {
            mUsbPort.close();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        super.onDestroy();
    }

    private final BroadcastReceiver mUsbReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if (ACTION_USB_PERMISSION.equals(action)) {
                synchronized (this) {
                    UsbDevice device = intent
                            .getParcelableExtra(UsbManager.EXTRA_DEVICE);
                    if (intent.getBooleanExtra(UsbManager.EXTRA_PERMISSION_GRANTED, false)) {
                        if (device != null) {
                            // call method to set up device communication
                            if (!visited) {
                                OnClickButtonConnectDevices();
                                visited = true;
                            }
                        }
                    } else {
                        Log.d(TAG, "permission denied for device " + device);
                    }
                }
            }
            if (UsbManager.ACTION_USB_DEVICE_ATTACHED.equals(action)) {

                if(!searchDeviceCalled)
                      OnClickButtonSearchDevices();

                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null) {
                    // call your method that cleans up and closes communication with the device
                    mUsbPort.SetUSBConnectionFlag(false);

                }
            }

            if (UsbManager.ACTION_USB_DEVICE_DETACHED.equals(action)) {

                searchDeviceCalled = false;
                UsbDevice device = intent.getParcelableExtra(UsbManager.EXTRA_DEVICE);
                if (device != null) {
                    // call your method that cleans up and closes communication with the device
                    mUsbPort.SetUSBConnectionFlag(false);

                }
            }
        }
    };

    public void OnClickButtonSearchDevices() {

        ArrayList<String> usb_devices_list = mUsbPort.get_usb_devices_list();
        final int size = usb_devices_list.size();
        String data = "";

        for (int i = 0; i < size; i++) {
            data += usb_devices_list.get(i) + ",";
        }


        if (data.length() > 0) {
            String dataArray[] = data.split(" ");

            if (dataArray != null) {

                if (dataArray.length > 0) {

                     Utility.showSnackbar(mRoot,"Printer Detected");

                    try {
                        vendorId = Integer.parseInt(dataArray[1].trim());

                    } catch (Exception ex){

                        vendorId =0;
                    }

                    try {
                        productId = Integer.parseInt(dataArray[3].trim());

                    } catch (Exception ex){

                        productId =0;
                    }

                    if(vendorId!=0 && productId!=0){
                        searchDeviceCalled = true;
                    }

                }
            }
        }
    }

    public void OnClickButtonConnectDevices() {


        UsbDevice foundDevice = mUsbPort.search_device(vendorId, productId);

        if (foundDevice == null) {
            Utility.showSnackbar(mRoot,"USB Device vendorId=" + vendorId + " productID=" + productId + " not found");

            return;
        } else {
            Utility.showSnackbar(mRoot,"Device Found");
        }
        try {
            if (!this.mUsbManager.hasPermission(foundDevice))
                Utility.showSnackbar(mRoot,"Need Authentication. Please Accept Permission");

            this.mUsbManager.requestPermission(foundDevice, mPermissionIntent);


            if (mUsbPort.connect_device(foundDevice)) {

                Utility.showSnackbar(mRoot,"Device connected");

            } else {

                Utility.showSnackbar(mRoot,"Device Not Connected");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void print(){

        mEscPos.init_printer();
        mEscPos.justification_center();
        mEscPos.select_fontA();
        mEscPos.print_linefeed();
        for(Order_details order_details :orderData.getOrder_details()){

            mEscPos.print_line(order_details.getHospital_name());


            for(Hospital_data hospital_data :order_details.getHospital_data()){


                for( User_data user_data :hospital_data.getUser_data()){



                    for(Patient_data patient_data :user_data.getPatient_data()){

                        int weekId = Integer.parseInt(patient_data.getWeek_id());

                        mEscPos.print_line("Week "+ ++weekId);

                        mEscPos.justification_left();

                        mEscPos.print_linefeed();
                        mEscPos.print_text("ON : ");
                        mEscPos.double_strike_on();
                        mEscPos.print_text(orderNumberString);
                        mEscPos.double_strike_off();
                        mEscPos.print_linefeed();
                        mEscPos.print_text("Date :"+orderData.getOrder_details().get(0).getHospital_data().get(0).getUser_data().get(0).getPatient_data().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getOrder_date());
                        mEscPos.print_linefeed();
                        mEscPos.print_text("User :"+ Preference.getUserName());
                        mEscPos.print_linefeed();
                        mEscPos.print_text("Name :"+ getUserDetail().getFirst_name()+ " "+getUserDetail().getLast_name());
                        mEscPos.print_linefeed();
                        mEscPos.print_text(getUserDetail().getGender().toUpperCase()+ " Bed No:"+getUserDetail().getBed_num()+" Ward:"+getUserDetail().getWard());
                        mEscPos.print_linefeed();
                        mEscPos.print_text("Note :"+getUserDetail().getAllergies());
                        mEscPos.print_linefeed();
                        mEscPos.print_text("Serve :"+getUserDetail().getServe());

                        for(Week_data week_data :patient_data.getWeek_data()){

                            for(Day_data day_data: week_data.getDay_data()){

                                mEscPos.print_and_feed_lines((byte) 1);
                                mEscPos.print_text("ON : ");
                                mEscPos.double_strike_on();
                                mEscPos.print_text(orderNumberString);
                                mEscPos.double_strike_off();
                                mEscPos.print_linefeed();
                                mEscPos.double_strike_on();
                                mEscPos.print_text(day_data.getMeal_value());//
                                mEscPos.double_strike_off();

                                mEscPos.print_text( "("+Utility.getYYYYMMDD(getDateOfDay(getCalanderDay(week_data.getDay_value())))+" "+week_data.getDay_value()+")");
                        //        mEscPos.print_text( "("+Utility.getDayMonthAndDate(getDateOfDay(getCalanderDay(week_data.getDay_value())))+" "+week_data.getDay_value()+")");
                                mEscPos.print_linefeed();
                                for(Meal_data meal_data :day_data.getMeal_data()){

                                    for(Order_data order_data :meal_data.getOrder_data()){

                                        mEscPos.underline_2dot_on();
                                        mEscPos.print_text(order_data.getCategory_value());
                                        mEscPos.underline_off();
                                        mEscPos.print_text(" ");

                                        for(Category_data category_data: order_data.getCategory_data()){

                                            mEscPos.print_text("*"+ category_data.getMenu_data().getMenu_name()+" ");
                                       }

                                    }

                                }
                            }
                        }
                    }
                }
            }
        }
        mEscPos.print_linefeed();
        mEscPos.justification_center();
        mEscPos.print_line("---");
        mEscPos.print_and_feed_lines((byte) 1);
        mEscPos.feedpapercut();

    }

    public int getCalanderDay(String dayString){

        int dayId = -1;
        switch (dayString){

            case "Monday":
                dayId = Calendar.MONDAY;
                break;

            case "Tuesday":
                dayId = Calendar.TUESDAY;
                break;

            case "Wednesday":
                dayId = Calendar.WEDNESDAY;
                break;

            case "Thursday":
                dayId = Calendar.THURSDAY;
                break;

            case "Friday":
                dayId = Calendar.FRIDAY;
                break;

            case "Saturday":
                dayId = Calendar.SATURDAY;
                break;

            case "Sunday":
                dayId = Calendar.SUNDAY;
                break;
        }

        return dayId;
    }

    public Calendar getDateOfDay(int day){

        Calendar calObj = Calendar.getInstance();

        if(calObj.get(Calendar.DAY_OF_WEEK)!=day){

            while(calObj.get(Calendar.DAY_OF_WEEK)!=day){
                calObj.add(Calendar.DATE,1);
            }

        }
        return calObj;
    }
}