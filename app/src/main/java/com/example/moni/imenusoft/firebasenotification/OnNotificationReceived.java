package com.example.moni.imenusoft.firebasenotification;

/**
 * Created by enuke on 24/10/17.
 */

public interface OnNotificationReceived {
    void onNotificationReceived(String string);
}
