package com.example.moni.imenusoft.serverRequests;

import com.google.gson.annotations.Expose;

/**
 * Created by enuke on 1/10/17.
 */

public class LoginRequest {

    @Expose
    String email;

    @Expose
    String password;

    @Expose
    String device_token;

    public LoginRequest(String email, String password, String device_token) {
        this.email = email;
        this.password = password;
        this.device_token = device_token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }
}
