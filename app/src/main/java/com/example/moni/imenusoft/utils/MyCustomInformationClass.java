package com.example.moni.imenusoft.utils;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.widget.LinearLayout;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;

/**
 * Created by enuke on 7/10/17.
 */

public class MyCustomInformationClass extends LinearLayout {

    Context context;
    private TextView all;
    private TextView no;
    private TextView others;
    private LinearLayout rootLayout;

    public MyCustomInformationClass(Context context) {
        super(context);
        this.context = context;
        init();

    }

    public MyCustomInformationClass(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();

    }

    public MyCustomInformationClass(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();

    }


    private void init() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.custom_bottom_information_layout, this);

        initView();
    }

    private void initView() {

        all = findViewById(R.id.all);
        no = findViewById(R.id.no);
        others = findViewById(R.id.others);
        rootLayout = findViewById(R.id.root_layout);
    }

    public void setColor(int color){


        GradientDrawable drawable = (GradientDrawable)rootLayout.getBackground();
        drawable.setStroke((int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1, getResources().getDisplayMetrics()), color);

        Drawable imgAll[] = all.getCompoundDrawables();

        applyThemeToDrawable(imgAll[0],color);


        Drawable imgNo[] = no.getCompoundDrawables();

     //   applyThemeToDrawable(imgNo[0],color);


        Drawable imgOthers[] = others.getCompoundDrawables();

    //    applyThemeToDrawable(imgOthers[0],color);

        GradientDrawable drawableNo = (GradientDrawable) imgOthers[0];
        drawableNo.setColor(color);
        drawableNo.setStroke(3, color);


        GradientDrawable drawableOthers = (GradientDrawable)imgNo[0];
        drawableOthers.setStroke(3, color);

    }

    public void applyThemeToDrawable(Drawable image, int color) {

        if (image != null) {
            PorterDuffColorFilter porterDuffColorFilter = new PorterDuffColorFilter(color,
                    PorterDuff.Mode.SRC_ATOP);

            image.setColorFilter(porterDuffColorFilter);
        }
    }


}
