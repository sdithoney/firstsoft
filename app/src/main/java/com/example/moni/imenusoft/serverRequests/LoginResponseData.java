package com.example.moni.imenusoft.serverRequests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by enuke on 1/10/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponseData implements Serializable {

    @SerializedName("user")
    @Expose
    private LoginResponseDataUser user;

    @SerializedName("hospital")
    @Expose
    private LoginResponseDataHospital hospital;

    public LoginResponseData() {
    }

    public LoginResponseDataUser getUser() {
        return user;
    }

    public void setUser(LoginResponseDataUser user) {
        this.user = user;
    }

    public LoginResponseDataHospital getHospital() {
        return hospital;
    }

    public void setHospital(LoginResponseDataHospital hospital) {
        this.hospital = hospital;
    }
}
