package com.example.moni.imenusoft.firebasenotification.notificationJson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class NotificationResponse{
  @SerializedName("visibility")
  @Expose

  private Boolean visibility;
  @SerializedName("action")
  @Expose
  private String action;

  @SerializedName("changedata")
  @Expose
  private List<Changedata> changedata;

  // id
  @SerializedName("id")
  @Expose
  private String id;

  @SerializedName("OrderData")
  @Expose
  private Changedata OrderData;


  public void setVisibility(Boolean visibility){
   this.visibility=visibility;
  }
  public Boolean getVisibility(){
   return visibility;
  }
  public void setAction(String action){
   this.action=action;
  }
  public String getAction(){
   return action;
  }
  public void setChangedata(List<Changedata> changedata){
   this.changedata=changedata;
  }
  public List<Changedata> getChangedata(){
   return changedata;
  }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Changedata getOrderData() {
        return OrderData;
    }

    public void setOrderData(Changedata orderData) {
        OrderData = orderData;
    }
}