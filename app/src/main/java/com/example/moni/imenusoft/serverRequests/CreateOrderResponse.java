package com.example.moni.imenusoft.serverRequests;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class CreateOrderResponse{
  @SerializedName("data")
  @Expose
  private Data data;
  @SerializedName("success")
  @Expose
  private Boolean success;
  public CreateOrderResponse(){
  }
  public CreateOrderResponse(Data data,Boolean success){
   this.data=data;
   this.success=success;
  }
  public void setData(Data data){
   this.data=data;
  }
  public Data getData(){
   return data;
  }
  public void setSuccess(Boolean success){
   this.success=success;
  }
  public Boolean getSuccess(){
   return success;
  }
}