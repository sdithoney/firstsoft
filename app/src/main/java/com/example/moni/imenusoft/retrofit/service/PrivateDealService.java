package com.example.moni.imenusoft.retrofit.service;



import  com.example.moni.imenusoft.serverRequests.CancelOrderResponse;
import  com.example.moni.imenusoft.serverRequests.CancelRequest;
import  com.example.moni.imenusoft.serverRequests.CreateOrderResponse;
import  com.example.moni.imenusoft.serverRequests.GetOrderRequest;
import  com.example.moni.imenusoft.serverRequests.LoginRequest;
import  com.example.moni.imenusoft.serverRequests.LoginResponse;
import  com.example.moni.imenusoft.serverRequests.MenuRequest;
import  com.example.moni.imenusoft.serverRequests.MenuResponse;
import  com.example.moni.imenusoft.serverRequests.OrderRequest;
import  com.example.moni.imenusoft.serverRequests.UpdateOrderRequest;
import  com.example.moni.imenusoft.serverRequests.UpdateOrderResponse;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.OrderDetailResponse;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;


/**
 * Created by enuke on 5/12/17.
 */

public interface PrivateDealService {

    /**
     * Method to get Country List
     * @return
     */
    /*@GET("api/Location/GetCountryList")
    Call<JSONObject> countries();*/

    /**
     * Method for Login
     * @param loginBody
     * @return
     */
/*    @POST("SignUp")
     Call<JSONObject> login(@Body JSONObject loginBody);*/

    @POST("api/user_login.php")
    Call<LoginResponse> login(@Body LoginRequest loginBody);

    @POST("api/get_order_details.php")
    Call<OrderDetailResponse> getOrder(@Body GetOrderRequest loginBody);

    @POST("api/get_menu.php")
    Call<MenuResponse> menulist(@Body MenuRequest loginBody);

    @POST("api/create_order.php")
    Call<CreateOrderResponse> createOrder(@Body OrderRequest loginBody);

    @POST("api/order_cancel.php")
    Call<CancelOrderResponse> cancelOrder(@Body CancelRequest loginBody);

    @POST("api/change_order_status.php")
    Call<UpdateOrderResponse> updateOrder(@Body UpdateOrderRequest loginBody);

    @POST("api/update_order.php")
    Call<UpdateOrderResponse> updateDetailOrder(@Body OrderRequest loginBody);

  }