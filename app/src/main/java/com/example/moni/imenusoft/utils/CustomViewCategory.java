package com.example.moni.imenusoft.utils;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.adapters.MenuAdapter;
import  com.example.moni.imenusoft.adapters.OnMenuClick;
import  com.example.moni.imenusoft.serverRequests.MenuResponseCategory;

/**
 * Created by enuke on 20/10/17.
 */

public class CustomViewCategory extends LinearLayout {

    Context context;

    private TextView foodCatagory;
    private RecyclerView menuRecycler;

    public OnMenuClick listener;
    MenuResponseCategory menuResponseCategoryObj;

    public CustomViewCategory(Context context) {
        super(context);
        this.context = context;

        init();
    }

    public CustomViewCategory(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        this.context = context;

        init();

    }

    public CustomViewCategory(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.context = context;

        init();

    }

    private void init() {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.order_list_row, this);

        initView();
    }


    private void initView() {

        foodCatagory = findViewById(R.id.food_catagory);
        menuRecycler = findViewById(R.id.menu_recycler);

        GridLayoutManager gridLayoutManagerObj = new GridLayoutManager(context,2);
        menuRecycler.setLayoutManager(gridLayoutManagerObj);

        menuRecycler.addOnItemTouchListener(new RecyclerItemClickListener(context, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

                Utility.showToast(context,menuResponseCategoryObj.getCategory_data().get(position).getMenu_name());

                if(listener!=null){

                    if(menuResponseCategoryObj.getCategory_data().get(position).isSelected()){
                        menuResponseCategoryObj.getCategory_data().get(position).setSelected(false);
                    } else {
                        menuResponseCategoryObj.getCategory_data().get(position).setSelected(true);
                    }

                    listener.OnMenuClick(menuResponseCategoryObj.getCategory_data().get(position));

                    if(menuRecycler.getAdapter()!=null){
                        menuRecycler.getAdapter().notifyItemChanged(position);
                    }
                }
            }
        }));
    }

    public void setData(MenuResponseCategory menuResponseCategoryObj, String color){

        foodCatagory.setTextColor(Utility.getColorCode(color));

        this.menuResponseCategoryObj = menuResponseCategoryObj;

        foodCatagory.setText(menuResponseCategoryObj.getCategory_value());
        menuRecycler.setAdapter(new MenuAdapter(context,menuResponseCategoryObj.getCategory_data(),color));
    }
}
