package com.example.moni.imenusoft.retrofit.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import  com.example.moni.imenusoft.retrofit.CommunicationLink;
import  com.example.moni.imenusoft.serverRequests.GetOrderRequest;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.OrderDetailResponse;
import com.google.gson.GsonBuilder;

/**
 * Created by enuke on 20/10/17.
 */

public class GetOrderLoader extends AsyncTaskLoader<OrderDetailResponse> {

    private Context mContext;
    private String accessToken;
    private int interestId;
    GetOrderRequest jsonObject;

    public GetOrderLoader(Context context, GetOrderRequest jsonObject) {
        super(context);
        mContext = context;
        this. accessToken=accessToken;
        this.interestId=interestId;
        this.jsonObject = jsonObject;
    }



    @Override
    public OrderDetailResponse loadInBackground() {
        try {
            String json = new GsonBuilder().create().toJson(jsonObject);
            Log.i("request",""+json);
            OrderDetailResponse responsePlan = CommunicationLink.actiongetOrderDetail(jsonObject);
            return responsePlan;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

}
