package com.example.moni.imenusoft.retrofit.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import  com.example.moni.imenusoft.retrofit.CommunicationLink;
import  com.example.moni.imenusoft.serverRequests.LoginRequest;
import  com.example.moni.imenusoft.serverRequests.LoginResponse;
import com.google.gson.GsonBuilder;

/**
 * Created by enuke on 7/11/17.
 */

public class LoginLoader extends AsyncTaskLoader<LoginResponse> {

    private Context mContext;
    private String accessToken;
    private int interestId;
    LoginRequest jsonObject;

    public LoginLoader(Context context, LoginRequest jsonObject) {
        super(context);
        mContext = context;
        this. accessToken=accessToken;
        this.interestId=interestId;
        this.jsonObject = jsonObject;
    }



    @Override
    public LoginResponse loadInBackground() {
        try {
            String json = new GsonBuilder().create().toJson(jsonObject);
            Log.i("request",""+json);
            LoginResponse responsePlan = CommunicationLink.actionLoginNew(jsonObject);
            return responsePlan;
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
        protected void onStartLoading() {
        forceLoad();
    }

}
