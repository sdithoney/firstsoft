package com.example.moni.imenusoft.serverRequests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by enuke on 10/10/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class MenuResponse implements Serializable {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("data")
    @Expose
    private ArrayList<MenuResponseData> data;

    @SerializedName("massage")
    @Expose
    private String massage;

    public MenuResponse(boolean success, ArrayList<MenuResponseData> data) {
        this.success = success;
        this.data = data;
    }

    public MenuResponse(boolean success, ArrayList<MenuResponseData> data, String massage) {
        this.success = success;
        this.data = data;
        this.massage = massage;
    }

    public MenuResponse() {
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public ArrayList<MenuResponseData> getData() {
        return data;
    }

    public void setData(ArrayList<MenuResponseData> data) {
        this.data = data;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }
}
