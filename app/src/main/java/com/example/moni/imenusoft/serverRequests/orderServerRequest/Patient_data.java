package com.example.moni.imenusoft.serverRequests.orderServerRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class Patient_data implements Serializable {
  @SerializedName("week_color")
  @Expose
  private String week_color;
  @SerializedName("week_id")
  @Expose
  private String week_id;
  @SerializedName("week_data")
  @Expose
  private List<Week_data> week_data;
  public Patient_data(){
  }
  public Patient_data(String week_color, String week_id, List<Week_data> week_data){
   this.week_color=week_color;
   this.week_id=week_id;
   this.week_data=week_data;
  }
  public void setWeek_color(String week_color){
   this.week_color=week_color;
  }
  public String getWeek_color(){
   return week_color;
  }
  public void setWeek_id(String week_id){
   this.week_id=week_id;
  }
  public String getWeek_id(){
   return week_id;
  }
  public void setWeek_data(List<Week_data> week_data){
   this.week_data=week_data;
  }
  public List<Week_data> getWeek_data(){
   return week_data;
  }
}