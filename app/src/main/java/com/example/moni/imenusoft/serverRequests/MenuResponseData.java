package com.example.moni.imenusoft.serverRequests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by enuke on 10/10/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class MenuResponseData implements Serializable {

  /*  {
        "week_id":"1",
            "week_color":"blue",
            "week_data":[]
    } */

    @SerializedName("week_id")
    @Expose
    private String week_id;

    @SerializedName("week_color")
    @Expose
    private String week_color;

    @SerializedName("week_data")
    @Expose
    private ArrayList<MenuResponseDataDay> week_data;

    public MenuResponseData(String week_id, String week_color, ArrayList<MenuResponseDataDay> week_data) {
        this.week_id = week_id;
        this.week_color = week_color;
        this.week_data = week_data;
    }

    public MenuResponseData() {
    }

    public String getWeek_id() {
        return week_id;
    }

    public void setWeek_id(String week_id) {
        this.week_id = week_id;
    }

    public String getWeek_color() {
        return week_color;
    }

    public void setWeek_color(String week_color) {
        this.week_color = week_color;
    }

    public ArrayList<MenuResponseDataDay> getWeek_data() {
        return week_data;
    }

    public void setWeek_data(ArrayList<MenuResponseDataDay> week_data) {
        this.week_data = week_data;
    }
}
