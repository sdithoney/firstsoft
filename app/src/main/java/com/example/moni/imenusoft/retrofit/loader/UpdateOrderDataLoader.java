package com.example.moni.imenusoft.retrofit.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import  com.example.moni.imenusoft.retrofit.CommunicationLink;
import  com.example.moni.imenusoft.serverRequests.OrderRequest;
import  com.example.moni.imenusoft.serverRequests.UpdateOrderRequest;
import  com.example.moni.imenusoft.serverRequests.UpdateOrderResponse;
import com.google.gson.GsonBuilder;

/**
 * Created by enuke on 23/10/17.
 */

public class UpdateOrderDataLoader extends AsyncTaskLoader<UpdateOrderResponse> {

    private Context mContext;
    private String accessToken;
    private int interestId;
    OrderRequest jsonObject;

    public UpdateOrderDataLoader(Context context, OrderRequest jsonObject) {

        super(context);
        mContext = context;
        this. accessToken=accessToken;
        this.interestId=interestId;
        this.jsonObject = jsonObject;
    }

    @Override
    public UpdateOrderResponse loadInBackground() {
        try {

            String json = new GsonBuilder().create().toJson(jsonObject);
            Log.i("request",""+json);
            UpdateOrderResponse responsePlan = CommunicationLink.updateDetailOrder(jsonObject);
            return responsePlan;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

}
