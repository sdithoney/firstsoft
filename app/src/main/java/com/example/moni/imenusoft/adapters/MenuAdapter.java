package com.example.moni.imenusoft.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.serverRequests.MenuResponseCategory;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataMenu;
import  com.example.moni.imenusoft.utils.Utility;

import java.util.ArrayList;

/**
 * Created by enuke on 10/10/17.
 */

public class MenuAdapter extends RecyclerView.Adapter<MenuAdapter.ViewHolder> {


    Context context;
    ArrayList<MenuResponseDataMenu> facilityList;
    String color;

    RecyclerView recyclerView;



    public MenuAdapter(Context context, ArrayList<MenuResponseDataMenu> facilityList, String color) {
        this.context = context;
        this.facilityList = facilityList;
        this.color = color;

    }


    @Override
    public MenuAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_item, parent, false);
        return new MenuAdapter.ViewHolder(view);


    }

    @Override
    public void onBindViewHolder(final MenuAdapter.ViewHolder holder, final int position) {

        holder.menu.setText(facilityList.get(position).getMenu_name());


        holder.menu.setCompoundDrawables(null,null,null,null);
        holder.menu.requestLayout();

        if(facilityList.get(position).isSelected()){

            holder.menu.setTextColor(Utility.getColorCode(color));
            holder.menu.setTypeface(  Typeface.DEFAULT_BOLD);

        } else {

            holder.menu.setTextColor(Color.BLACK);
            holder.menu.setTypeface( Typeface.DEFAULT);
        }
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
        this.recyclerView = recyclerView;
    }

    @Override
    public int getItemCount() {
        return facilityList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView menu;

        public ViewHolder(View itemView) {
            super(itemView);

            menu = itemView.findViewById(R.id.menu_item);

        }
    }
}
