package com.example.moni.imenusoft.adapters;

import android.content.Context;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.serverRequests.MenuResponseCategory;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataMenu;
import  com.example.moni.imenusoft.utils.CustomViewCategory;
import  com.example.moni.imenusoft.utils.RecyclerItemClickListener;
import  com.example.moni.imenusoft.utils.Utility;

import java.util.ArrayList;

/**
 * Created by enuke on 10/10/17.
 */

public class MenuCategoryAdapter extends RecyclerView.Adapter<MenuCategoryAdapter.ViewHolder> {


    Context context;
    ArrayList<MenuResponseCategory> facilityList;
    String color;
    public OnMenuClick listener;


    public MenuCategoryAdapter(Context context, ArrayList<MenuResponseCategory> facilityList, String color) {
        this.context = context;
        this.facilityList = facilityList;
        this.color = color;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.category_custom_row, parent, false);
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        holder.customViewCategoryObj.listener = new OnMenuClick() {
            @Override
            public void OnMenuClick(MenuResponseDataMenu menuObject) {

                if(listener!=null){
                    listener.OnMenuClick(menuObject);
                }

            }
        };


            holder.customViewCategoryObj.setData(facilityList.get(holder.getPosition()),color);


    }


    @Override
    public int getItemCount() {
        return facilityList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {


        CustomViewCategory customViewCategoryObj;

        public ViewHolder(View itemView) {
            super(itemView);

            customViewCategoryObj = itemView.findViewById(R.id.myCustomView);
        }
    }
}
