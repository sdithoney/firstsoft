package com.example.moni.imenusoft;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.serverRequests.LoginResponse;
import  com.example.moni.imenusoft.serverRequests.LoginResponseData;
import  com.example.moni.imenusoft.utils.Constants;
import  com.example.moni.imenusoft.utils.Preference;
import  com.example.moni.imenusoft.utils.Utility;
import com.google.gson.Gson;

/**
 * Created by enuke on 28/9/17.
 */

public class HospitalWelcome extends AppCompatActivity {


    private ImageView logo;
    private ImageView hospitalLogo;
    private TextView hospitalName;
    private RelativeLayout root;

    BackgroundTask backgroundObj;

    LoginResponseData dataObj = new LoginResponseData();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hospital_welcome_screen);
        initView();
    }

    private void initView() {
        logo = findViewById(R.id.logo);
        hospitalLogo = findViewById(R.id.hospital_logo);
        hospitalName = findViewById(R.id.hospital_name);
        root = findViewById(R.id.root);

        root.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(backgroundObj!=null)
                    backgroundObj.cancel(true);

                startActivity(new Intent(HospitalWelcome.this,OrderDashboard.class));
            }
        });

        fillUI();

        backgroundObj = new BackgroundTask();

    }

    @Override
    protected void onResume() {
        super.onResume();

        if(backgroundObj!= null)
        {
            backgroundObj = null;
            backgroundObj = new BackgroundTask();
        }
        backgroundObj.execute();
    }

    private void fillUI(){

        dataObj = new Gson().fromJson(Preference.getUserObjectJson(), LoginResponseData.class);

        if(dataObj!=null){
            if(dataObj.getHospital()!=null) {

                if (dataObj.getHospital().getHospital_name() != null)
                    hospitalName.setText(dataObj.getHospital().getHospital_name());

                if (dataObj.getHospital().getLogo_image() != null)

                    Utility.showImage(HospitalWelcome.this, Uri.parse(Constants.BASE_URL+dataObj.getHospital().getLogo_image()), hospitalLogo);

                Utility.showLog("img",Constants.BASE_URL+dataObj.getHospital().getLogo_image());
            }
        }
    }

    private class BackgroundTask extends AsyncTask {
        Intent intent;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //  intent = new Intent(SplashScreen.this,MainActivity.class);
        }

        @Override
        protected Object doInBackground(Object[] params) {
    /* Use this method to load background
    * data that your app needs. */
            // if (!Utility.isInternetConnected(SplashScreen.this)) {
            try {
                Log.i("thread time","ok");
                Thread.sleep(3000);
            } catch (Exception e) {
                e.printStackTrace();
                // }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);

            startActivity(new Intent(HospitalWelcome.this,OrderDashboard.class));
        }
    }

    @Override
    protected void onPause() {
        super.onPause();

        if(backgroundObj!=null){
            backgroundObj.cancel(true);
        }
    }
}
