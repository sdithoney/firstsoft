package com.example.moni.imenusoft.utils;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.Toast;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.HospitalWelcome;
import  com.example.moni.imenusoft.LoginActivity;
import  com.example.moni.imenusoft.OrderDashboard;
import  com.example.moni.imenusoft.serverRequests.LoginResponseData;
import  com.example.moni.imenusoft.serverRequests.MenuResponseCategory;
import com.facebook.common.executors.CallerThreadExecutor;
import com.facebook.common.references.CloseableReference;
import com.facebook.datasource.DataSource;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.facebook.imagepipeline.core.ImagePipeline;
import com.facebook.imagepipeline.datasource.BaseBitmapDataSubscriber;
import com.facebook.imagepipeline.image.CloseableImage;
import com.facebook.imagepipeline.request.ImageRequest;
import com.facebook.imagepipeline.request.ImageRequestBuilder;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;
import java.util.List;

/**
 * Created by enuke on 29/9/17.
 */

public class Utility {

    private Calendar cal;

    public static boolean isInternetConnected(Context mContext) {

        final ConnectivityManager connectivityManager = (ConnectivityManager) mContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        final NetworkInfo ni = connectivityManager.getActiveNetworkInfo();

        return ni != null && ni.isConnectedOrConnecting();
    }

    public static void showToast(Context mContext, String message) {

        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    public static void showSnackbar(View root, String message){
        Snackbar snackbar = Snackbar
                .make(root, message, Snackbar.LENGTH_LONG);

        snackbar.show();
    }


    public static void showLog(String tag, String message) {

        Log.i(tag,message);
    }

    public static void traverseToLogin(Context con){
        ((Activity)con).finish();
        Intent loinIntent  = new Intent(con, LoginActivity.class);
        loinIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
      //  loinIntent.putExtra("userData", object);
        con.startActivity(loinIntent);

    }

    public static void traverseToHomePage(Context con){

        ((Activity)con).finish();
        Intent homePageIntent  = new Intent(con, HospitalWelcome.class);
        homePageIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        con.startActivity(homePageIntent);
    }


    public static void traverseToNewOrder(Context con){
        ((Activity)con).finish();
        Intent loinIntent  = new Intent(con, OrderDashboard.class);
        loinIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
        //  loinIntent.putExtra("userData", object);
        con.startActivity(loinIntent);

    }

    public static void doActionLogout(Context con){

        Preference.clearPreferences();
        Utility.traverseToLogin(con);
    }

    public static void showImage(final Context con, Uri url, final ImageView img){

        ImageRequest imageRequest = ImageRequestBuilder
                .newBuilderWithSource(url)
                .setAutoRotateEnabled(true)
                .build();

        ImagePipeline imagePipeline = Fresco.getImagePipeline();
        final DataSource<CloseableReference<CloseableImage>>
                dataSource = imagePipeline.fetchDecodedImage(imageRequest, con);

        dataSource.subscribe(new BaseBitmapDataSubscriber() {

            @Override
            public void onNewResultImpl(@Nullable Bitmap bitmap) {
                if (dataSource.isFinished() && bitmap != null) {

                    if (bitmap != null) {
                        final Drawable drawable1 = new BitmapDrawable(con.getResources(), bitmap);

                        if (drawable1 != null)
                            ((Activity)con).runOnUiThread(new Runnable() {
                                                              @Override
                                                              public void run() {
                                                                  img.setImageDrawable(drawable1);

                                                              }
                                                          }
                            );
                    }
                    dataSource.close();
                }
            }

            @Override
            public void onFailureImpl(DataSource dataSource) {
                if (dataSource != null) {
                    ((Activity)con).runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            img.setImageResource(R.drawable.default_hospital);
                        }
                    });
                    dataSource.close();
                }
            }
        }, CallerThreadExecutor.getInstance());
    }

    public static int getColorCode(String colorName){

       int color = Color.TRANSPARENT;

       if(colorName.equalsIgnoreCase("red")){

           color = Color.parseColor("#EF4036");


       } else if(colorName.equalsIgnoreCase("blue")){

           color = Color.parseColor("#2E3092");

       } else if(colorName.equalsIgnoreCase("green")){

           color = Color.parseColor("#006738");


       } else if(colorName.equalsIgnoreCase("brown")){

           color = Color.parseColor("#754C28");

       } else if(colorName.equalsIgnoreCase("default")){

           color = Color.parseColor("#00ADEF");
       }

       return color;
    }

    public static String setTimeHeader(){


            Calendar cal = Calendar.getInstance();
            SimpleDateFormat sdf = new SimpleDateFormat("EEEE MMMM dd, yyyy. hh:mm a");
            String strDate = sdf.format(cal.getTime());
            return strDate;

    }

    public static String getDayMonthAndDate(Calendar cal){

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE MMMM dd");
        String strDate = sdf.format(cal.getTime());
        return strDate;

    }

    public static String getYYYYMMDD(Calendar cal){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        String strDate = sdf.format(cal.getTime());
        return strDate;

    }

    public static String getOrderDate(Calendar cal){

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy/MM/dd");
        String strDate = sdf.format(cal.getTime());
        return strDate;

    }


    public static void showHelpMessages(String title, String message, Context con, final OnDialogClick listener){
        AlertDialog alertDialog = new AlertDialog.Builder(con).create();
        alertDialog.setTitle(title);
        alertDialog.setMessage(message);
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "Ok",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();

                        if(listener!=null){
                            listener.onDialogOkClick();
                        }
                    }
                });
        alertDialog.getWindow().setGravity(Gravity.BOTTOM);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alertDialog.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        alertDialog.show();
        alertDialog.getWindow().setAttributes(lp);
    }

    public static ArrayList<MenuResponseCategory> removeDataFromArrayList(ArrayList<MenuResponseCategory> arrayList, String id){


        List<MenuResponseCategory> list = new ArrayList<>();
        list.addAll(arrayList);

// This is a clever way to create the iterator and call iterator.hasNext() like
// you would do in a while-loop. It would be the same as doing:
//     Iterator<String> iterator = list.iterator();
//     while (iterator.hasNext()) {
        for (Iterator<MenuResponseCategory> iterator = list.iterator(); iterator.hasNext();) {
            MenuResponseCategory menuResponseCategoryObj = iterator.next();
           /* if (string.isEmpty()) {
                // Remove the current element from the iterator and the list.
                iterator.remove();
            }*/

            if(menuResponseCategoryObj.getCategory_id().equalsIgnoreCase(id))
                iterator.remove();
        }

        return (ArrayList<MenuResponseCategory>) list;
    }

}
