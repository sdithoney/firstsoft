package com.example.moni.imenusoft.adapters;

import  com.example.moni.imenusoft.serverRequests.MenuResponseDataMenu;

/**
 * Created by enuke on 20/10/17.
 */

public interface OnMenuClick {
    public void OnMenuClick(MenuResponseDataMenu menuObject);
}
