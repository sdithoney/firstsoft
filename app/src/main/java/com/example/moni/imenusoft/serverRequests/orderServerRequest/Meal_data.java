package com.example.moni.imenusoft.serverRequests.orderServerRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class Meal_data implements Serializable {
  @SerializedName("order_data")
  @Expose
  private List<Order_data> order_data;
  @SerializedName("order_num")
  @Expose
  private String order_num;
  @SerializedName("order_id")
  @Expose
  private String order_id;

  @SerializedName("order_status")
  @Expose
  private String order_status;

    @SerializedName("order_date")
    @Expose
    private String order_date;


  public Meal_data(){
  }

    public Meal_data(List<Order_data> order_data, String order_num, String order_id, String order_status, String order_date) {
        this.order_data = order_data;
        this.order_num = order_num;
        this.order_id = order_id;
        this.order_status = order_status;
        this.order_date = order_date;
    }

    public void setOrder_data(List<Order_data> order_data){
   this.order_data=order_data;
  }
  public List<Order_data> getOrder_data(){
   return order_data;
  }
  public void setOrder_num(String order_num){
   this.order_num=order_num;
  }
  public String getOrder_num(){
   return order_num;
  }
  public void setOrder_id(String order_id){
   this.order_id=order_id;
  }
  public String getOrder_id(){
   return order_id;
  }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }

    public String getOrder_date() {
        return order_date;
    }

    public void setOrder_date(String order_date) {
        this.order_date = order_date;
    }
}