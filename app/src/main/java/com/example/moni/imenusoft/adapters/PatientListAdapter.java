package com.example.moni.imenusoft.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.User_data;

import java.util.ArrayList;

/**
 * Created by enuke on 12/10/17.
 */

public class PatientListAdapter extends RecyclerView.Adapter<PatientListAdapter.ViewHolder> {


    Context context;
    ArrayList<User_data> facilityList;



    public PatientListAdapter(Context context, ArrayList<User_data> facilityList) {
        this.context = context;
        this.facilityList = facilityList;

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.order_summary_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        int i = holder.getAdapterPosition();
        holder.name.setText(""+(++i)+". "+facilityList.get(holder.getAdapterPosition()).getFirst_name()+" "+
        facilityList.get(holder.getAdapterPosition()).getLast_name());

        holder.bed.setText(facilityList.get(holder.getAdapterPosition()).getBed_num());
        holder.ward.setText(facilityList.get(holder.getAdapterPosition()).getWard());
    }


    @Override
    public int getItemCount() {
        return facilityList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        TextView name, bed, ward;

        public ViewHolder(View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.name);
            bed = itemView.findViewById(R.id.bed);
            ward = itemView.findViewById(R.id.ward);

        }
    }
}
