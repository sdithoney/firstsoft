package com.example.moni.imenusoft.firebasenotification;

import android.content.Context;
import android.util.Log;


import org.json.JSONObject;


/**
 * Created by enuke on 10/27/16.
 */
public class NotificationServiceManager {

  //  private static final String TAG = "NotificationServiceManager";
    public Context con;
    String base64 = "";
   /* CategorychildTable categorychildTable;
    CategoryParentTable categoryParentTable;*/

    public NotificationServiceManager(Context con) {
        this.con = con;

    }

    public void onMessageReceived(final String type, final JSONObject jsonObject) {
      /*  int count = Preference.getIsRead();
        //Log.d("Shared Preference value", ""+count);
        Preference.setIsRead(++count);*/
        //Log.d("On Message Receive","true");

/*
        ApplicationController.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                for (OnMessageReceived onMessageReceiveListener : AppController
                        .getInstance().getUIListeners(
                                OnMessageReceived.class))
                    onMessageReceiveListener
                            .onMessageReceived(type, jsonObject);
            }
        });*/

      Log.d("Message","Received");
    }

   /* public void updatingMap() {
        if (Preference.getMapJson().length() != 0) {
            //Log.d("Json Before Update ",Utility.hashMapToJson(preferencedMapData));
            preferencedMapData = Utility.jsonToHashmap(Preference.getMapJson());
        } else {
            preferencedMapData = new HashMap<String, ProductResponseDataNested>();
        }

        if (preferencedMapData != null) {
            ArrayList<ProductResponseDataNested> productResponseDataNested = CategoryPageVariantTable.getInstance().getNestedWholeData();
            *//*for(ProductResponseDataNested productResponseDataNested1 : productResponseDataNested)
            {
                ArrayList<ProductResponseDataVariant> productResponseDataVariants = CategoryPageVariantTable.getInstance().getCommentDataWithoutId(productResponseDataNested1.getId());
                productResponseDataNested1.setResponseDataVariants(productResponseDataVariants);
            }*//*
            for (int i = 0; i < productResponseDataNested.size(); i++) {
                ArrayList<ProductResponseDataVariant> productResponseDataVariants = CategoryPageVariantTable.getInstance().getCommentDataWithoutIdAndAll(productResponseDataNested.get(i).getId());
                productResponseDataNested.get(i).setResponseDataVariants(productResponseDataVariants);
            }
            //Log.d("in guide size",""+productResponseDataNested.size());

            for (ProductResponseDataNested productObj : productResponseDataNested) {
                for (ProductResponseDataVariant productObjVarient : productObj.getResponseDataVariants()) {
                    if (!preferencedMapData.containsKey(productObjVarient.getId())) {
                        productObjVarient.setQuantit_y("0");
                        productObjVarient.setImages(VariantImageTable.getInstance().getImages(productObjVarient.getId()));
                        preferencedMapData.put(productObjVarient.getId(), productObj);
                    } else {
                        productObjVarient.setImages(VariantImageTable.getInstance().getImages(productObjVarient.getId()));
                        productObjVarient.setQuantit_y("" + getItemQuantity(productObjVarient.getId()));
                        preferencedMapData.put(productObjVarient.getId(), productObj);
                    }

                }
            }

            updateMap(preferencedMapData);
        }

    }*/

   /* private int getItemQuantity(String variantID) {
        int quantity = 0;

        Map<String, ItemInfoBean> data = Utility.cartJsonToHashmap(Preference.getCartMapJson());

        if (data != null)
            if (data.containsKey(variantID)) {
                quantity = (data.get(variantID)).getQuantity();
            }

        return quantity;
    }


   *//* private void updateMap(Map<String, ProductResponseDataNested> map) {
        Preference.setMapJson(Utility.hashMapToJson(map));
        map.size();
    }
*//*

    public void updateStoreService(String storeID, String type_ID, String notificationType, JSONObject notificationJson) throws JSONException {
        String storeData = Preference.getSplashInsertedStatus();
        storeResponseData = new GsonBuilder().create().fromJson(storeData, StoreResponseMasterData.class);
        String url = Constant.GET_NEW_DATA;
        JSONObject requestJson = new JSONObject();
        requestJson.put("store_id", storeID);
        requestJson.put("type", notificationType);
        requestJson.put("id", type_ID);
        requestJson.put("Last_updated_date", Preference.getSplashInsertedStatus().equalsIgnoreCase("") ? "" : storeResponseData.getUpdated_date());

        if (Utility.isInternetConnected(con))
            makeServerRequestForStoreService(requestJson.toString(), url, notificationType, notificationJson);
        else
            Utility.showToast(con, con.getResources().getString(R.string.network_error));

    }

    private void makeServerRequestForStoreService(final String jsonRequestString, String url, final String type, final JSONObject jsonObject) {
        JsonObjectRequest jsonRequest = new JsonObjectRequest(Request.Method.POST, url, jsonRequestString, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(JSONObject response) {
                Log.d("noti_res",response.toString()+"request"+jsonRequestString);
                try {
                    if (response != null) {
                        if (response.getBoolean("success")) {

                            if (type.equals("store")) {

                                if (response.getJSONObject("data").getJSONObject("storeDetails") != null) {
                                    //Log.d("Response", "value " + response.getJSONObject("data").getJSONObject("storeDetails").toString());
                                    StoreResponseData storeResponseDataObj = new Gson().fromJson(response.getJSONObject("data").getJSONObject("storeDetails").toString(), StoreResponseData.class);
                                    insertIntoStoreTable(storeResponseDataObj);
                                    String lastUpdatedDate = response.getJSONObject("data").getString("updated_date");
                                    updatingLastDate(lastUpdatedDate);
                                }
                            }

                            if (type.equalsIgnoreCase("time")) {
                                StoreResponseDataTime storeResponseDataTime = new Gson().fromJson(response.getJSONObject("data").getJSONObject("time").toString(), StoreResponseDataTime.class);
                                insertWeeklyInfo(storeResponseDataTime);
                                String lastUpdatedDate = response.getJSONObject("data").getString("updated_date");
                                updatingLastDate(lastUpdatedDate);
                            }

                            if (type.equals("product")) {
                                ProductResponseDataNested productResponseDataNestedObj = new Gson().fromJson(response.getJSONObject("data").getJSONObject("products").toString(), ProductResponseDataNested.class);
                                insertIntoVariantTable(productResponseDataNestedObj);

                                String lastUpdatedDate = response.getJSONObject("data").getString("updated_date");
                                updatingLastDate(lastUpdatedDate);


                            }

                            if (type.equals("category")) {
                                JSONArray categoryArray = response.getJSONObject("data").getJSONArray("categoryData");
                                ArrayList<CategoryResponseData> data = new ArrayList<>();
                                CategoryResponseData categoryResponseDataObj;


                                for (int i = 0; i < categoryArray.length(); i++) {
                                    categoryResponseDataObj = new Gson().fromJson(categoryArray.optJSONObject(i).toString(), CategoryResponseData.class);
                                    data.add(categoryResponseDataObj);
                                }

                                insertCategoryData(data);

                                String lastUpdatedDate = response.getJSONObject("data").getString("updated_date");
                                updatingLastDate(lastUpdatedDate);

                                Intent intent = new Intent(NotificationServiceController.CATEGORY_BROADCAST_ACTION);
                                intent.putExtra("delete_cat","map updated");
                                con.sendBroadcast(intent);
                            }

                            if (type.equals("user")) {
                                StoreResponseDataOwner storeResponseDataOwner = new Gson().fromJson(response.getJSONObject("data").getJSONObject("user").toString(), StoreResponseDataOwner.class);
                                insertIntoOwnerTable(storeResponseDataOwner);
                                String lastUpdatedDate = response.getJSONObject("data").getString("updated_date");
                                updatingLastDate(lastUpdatedDate);
                            }

                            onMessageReceived(type, jsonObject);

                        }
                    }
                } catch (JSONException ex) {
                    ex.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });*/

  //     AppController.getInstance().addToRequestQueue(jsonRequest);
  /*  }



    private void insertWeeklyInfo(StoreResponseDataTime storeResponseDataTime) {


        String time_id = storeResponseDataTime.getId();
        String openingTime = storeResponseDataTime.getOpen();
        String closingTime = storeResponseDataTime.getClosing();
        String weekday = storeResponseDataTime.getWeekday();
        String storeId = storeResponseDataTime.getStoreId();
        String deliveryStartTime = storeResponseDataTime.getDeliveryStart();
        String deliveryEndTime = storeResponseDataTime.getDeliveryEnd();

        long groceryTimes = Grocery_TimesTable.getInstance().add(time_id, openingTime, closingTime, weekday, storeId, deliveryStartTime, deliveryEndTime);


    }

    private void insertIntoStoreTable(StoreResponseData storeResponseData) {
        String id = storeResponseData.getId();
        String name = storeResponseData.getName();
        String ownerId = storeResponseData.getOwner_id();
        String theme = storeResponseData.getTheme();
        String nameLogo = storeResponseData.getName_logo();
        String minOrder = storeResponseData.getMin_order();
        String deliveryCharge = storeResponseData.getDelivery_charge();
        String freeDeliveryAmount = storeResponseData.getFree_delivery_amount();
        String deliveryTime = storeResponseData.getDelivery_time();
        String deliveryType = storeResponseData.getDelivery_type();
        String longitude = storeResponseData.getLongitude() == null ? "0.0" : storeResponseData.getLongitude();
        String latitude = storeResponseData.getLatitude() == null ? "0.0" : storeResponseData.getLatitude();
        String taxPerOrder = storeResponseData.getTax_per_order();
        String street = storeResponseData.getStreet();
        String floor = storeResponseData.getFloor();
        String company = storeResponseData.getCompany();
        String countryId = storeResponseData.getCountry_id();
        String stateId = storeResponseData.getState_id();
        String cityId = storeResponseData.getCity_id();
        String createdAt = storeResponseData.getCreated_at();
        String updatedAt = storeResponseData.getUpdated_at();
        String deals = storeResponseData.getDeals();
        String featured = storeResponseData.getFeatured();
        String uniqueId = storeResponseData.getUnique_id();
        String imagePath = storeResponseData.getImage_path();
        String countryName = storeResponseData.getCountry_name();
        String stateName = storeResponseData.getState_name();
        String cityName = storeResponseData.getCity_name();
        String couponCode = storeResponseData.getCoupon_code();
        String couponValue = storeResponseData.getCoupon_value();
        String description = storeResponseData.getDescription();
        String orderType = storeResponseData.getOrderType();
        String aboutUs = storeResponseData.getAboutus();

        // updateOwnerProfile( Id,  name,  ownerId, theme,nameLogo, minimumOrder, deliveryCharge,  freeDeliveryAmount,  deliveryTime,  deliveryTimeType,  longitude, latitude, taxPerOrder,  floor,  street, company,  countryId, stateId, cityId ,createdAt, updatedAt, deals, featured, uniqueId, imagePath, countryName, StateName, cityName,couponCode, couponValue,description,orderType,aboutUs);

        long groceryDetails = Grocery_StoreDetailsTable.getInstance().updateOwnerProfile(id, name, ownerId, theme, nameLogo, minOrder, deliveryCharge, freeDeliveryAmount, deliveryTime, deliveryType, longitude, latitude, taxPerOrder, floor, street, company, countryId, stateId, cityId, createdAt, updatedAt, deals, featured, uniqueId, imagePath, countryName, stateName, cityName, couponCode, couponValue, description, orderType, aboutUs);

        //Log.d("inserted id",""+groceryDetails);

        if (groceryDetails == 1) {

        }
        getBase64FromURL(storeResponseData.getName_logo());

    }


    private void insertIntoOwnerTable(StoreResponseDataOwner storeResponseDataOwner) {

        String oid = storeResponseDataOwner.getId();
        String roleId = storeResponseDataOwner.getRoleId();
        String profileImage = storeResponseDataOwner.getProfileImage();
        String email = storeResponseDataOwner.getEmail();
        String firstName = storeResponseDataOwner.getFirstName();
        String lastName = storeResponseDataOwner.getLastName();
        String contactNumber = storeResponseDataOwner.getContactNumber();
        String storeId = storeResponseDataOwner.getStoreId();
        String cityIdOwner = storeResponseDataOwner.getCityId();
        String statusOwner = storeResponseDataOwner.getStatus();
        String createdAtOwner = storeResponseDataOwner.getCreatedAt();
        String updatedAtOwner = storeResponseDataOwner.getUpdatedAt();
        String deletedAtOwner = storeResponseDataOwner.getDeletedAt();
        String token = storeResponseDataOwner.getToken();
        String loginType = storeResponseDataOwner.getLoginType();
        String countrycode = storeResponseDataOwner.getCountrycode();
        String newsletter = storeResponseDataOwner.getNewsletter();

        long groceryOwner = Grocery_StoreOwnerTable.getInstance().add(oid, roleId, profileImage, firstName, lastName, email, contactNumber, storeId, cityIdOwner, statusOwner, createdAtOwner, updatedAtOwner, deletedAtOwner, token, loginType, countrycode, newsletter);


    }


    private void getBase64FromURL(String url) {
        if (url != null)

            if (url.length() > 0) {

                Picasso.with(con).load(url).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        ByteArrayOutputStream baos = new ByteArrayOutputStream();
                        bitmap.compress(Bitmap.CompressFormat.PNG, 100, baos);
                        byte[] b = baos.toByteArray();
                        base64 = Base64.encodeToString(b, Base64.DEFAULT);
                        Preference.setLogoImagePath(base64);
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });
            }
    }

    private void insertImages(ArrayList<ProductResponseVarImages> data) {
        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                ProductResponseVarImages entrydata = new ProductResponseVarImages();
                String id = data.get(i).getId();
                String variantId = data.get(i).getTarget_id();
                String image_path = data.get(i).getImage_path();

                entrydata.setId(id);
                entrydata.setTarget_id(variantId);
                entrydata.setImage_path(image_path);

                long insert = VariantImageTable.getInstance().add(entrydata);
                 Log.d("cat_child_data", "" + insert);
            }
        }
    }

    private void insertIntoVariantTable(ProductResponseDataNested productResponseDataNestedObj) {
        // ArrayList<ProductResponseDataNested> productResponseDataNesteds = value.getProductResponseDataNesteds();

        String parentId = "";
        String mainProductId = productResponseDataNestedObj.getId();
        String name = productResponseDataNestedObj.getName();
        String categoryID = productResponseDataNestedObj.getCategory_id();
        String storeId = Constant.STORE_ID;
        String status = productResponseDataNestedObj.getStatus();
        if (productResponseDataNestedObj.getResponseDataVariants().size() > 0) {
            for (ProductResponseDataVariant productResponseDataVariant : productResponseDataNestedObj.getResponseDataVariants()) {
                String variantId = productResponseDataVariant.getId();
                String topRated = "";
                if (productResponseDataVariant.getTop_rated().equalsIgnoreCase("1"))
                    topRated = "Yes";
                else
                    topRated = productResponseDataVariant.getTop_rated();
                String quantity = productResponseDataVariant.getQuantit_y();
                String ainstock = productResponseDataVariant.getAlways_in_stock();
                String desc = productResponseDataVariant.getDescription();
                String units = productResponseDataVariant.getUnits();
                String uom = productResponseDataVariant.getUom();
                String dim_x = productResponseDataVariant.getDimensions_x();
                String dim_y = productResponseDataVariant.getDimensions_y();
                String dim_z = productResponseDataVariant.getDimensions_z();
                String price = productResponseDataVariant.getPrice();
                String special_price = productResponseDataVariant.getSpecial_price();
                String cost = productResponseDataVariant.getCost();
                String imageArray = "";
                String bannerImage = "";
                String itemInStock = productResponseDataVariant.getItemInStock();
                StringBuilder stringBuilder = new StringBuilder();
                if (productResponseDataVariant.getId() != null) {

                    insertImages(productResponseDataVariant.getImages());
                    for (int image = 0; image < productResponseDataVariant.getImages().size(); image++) {
                        stringBuilder.append(productResponseDataVariant.getImages().get(image).getImage_path());
                        if (!(image == productResponseDataVariant.getImages().size() - 1))
                            stringBuilder.append(",");
                    }
                    imageArray = stringBuilder.toString();
                } else
                    imageArray = "";

                if (productResponseDataVariant.getBanner().size() > 0)
                    bannerImage = productResponseDataVariant.getBanner().get(0).getImage_path();
                else
                    bannerImage = "";

                CategoryPageVariantBean categoryPageVariantBean = new CategoryPageVariantBean(parentId, mainProductId, name, desc, categoryID, storeId, status,
                        variantId, topRated, quantity, ainstock, units, uom, dim_x, dim_y, dim_z, price, special_price, cost, imageArray, bannerImage, itemInStock);
                CategoryPageVariantTable.getInstance().add(categoryPageVariantBean);

                updateCartVarientQuantity(getItemInfoBean(name, mainProductId, desc, variantId,
                        price, special_price, units, uom, productResponseDataVariant.getImages().get(0).getImage_path(),
                        itemInStock));

            }
        }

    }

    private ItemInfoBean getItemInfoBean(String name, String mainProductId, String desc, String variantId, String price,
                                         String special_price, String units, String uom, String image_path, String itemInStock) {
        ItemInfoBean itemInfoBean = new ItemInfoBean();
        itemInfoBean.setProductName(name);
        itemInfoBean.setProductId(mainProductId);
        itemInfoBean.setProductDesc(desc);
        itemInfoBean.setVariantId(variantId);
        itemInfoBean.setPrice(price);
        itemInfoBean.setSpecial_price(special_price);
        itemInfoBean.setProductUnits(units);
        itemInfoBean.setProuctsUom(uom);
        itemInfoBean.setPrimaryImageURL(image_path);
        itemInfoBean.setItemInStock(Integer.parseInt(itemInStock));
        itemInfoBean.setQuantity(0);

        return itemInfoBean;
    }


    private void updateCartVarientQuantity(ItemInfoBean mItemInfoBean) {
        Map<String, ItemInfoBean> cartMap = Utility.cartJsonToHashmap(Preference.getCartMapJson());

        if (cartMap != null) {
            if (cartMap.size() > 0) {
                Log.i("cart map in size",""+cartMap.size());
                if (cartMap.containsKey(mItemInfoBean.getVariantId())) {
                    ItemInfoBean itemInfoBeanObj = cartMap.get(mItemInfoBean.getVariantId());
                    int quantity = itemInfoBeanObj.getQuantity();

                    if (quantity > mItemInfoBean.getItemInStock()) {

                        mItemInfoBean.setQuantity(mItemInfoBean.getItemInStock());
                    } else {

                        if (quantity != 0) {
                            mItemInfoBean.setQuantity(quantity);
                        }
                    }

                    if(mItemInfoBean.getQuantity()==0)
                        cartMap.remove(mItemInfoBean.getVariantId());

                    else
                        cartMap.put(mItemInfoBean.getVariantId(),mItemInfoBean);

                    Preference.setCartMapJson(Utility.hashMapToCartJson(cartMap));
                }
            }
        }
    }


    *//*private void updateMapVarientQuantity(String varientID,String itemInStock)
    {
        Map<String, ProductResponseDataNested> mainMap =  Utility.jsonToHashmap(Preference.getMapJson());

        if(mainMap!=null)
        {
            if(mainMap.size()>0)
            {
                if(mainMap.containsKey(varientID))
                {
                    ProductResponseDataNested productResponseDataNestedObj = mainMap.get(varientID);

                    for(ProductResponseDataVariant productResponseDataVariantObj: productResponseDataNestedObj.getResponseDataVariants())
                    {
                        if(productResponseDataVariantObj.getId().equals(varientID))
                        {
                            int quantity = Integer.parseInt(productResponseDataVariantObj.getQuantit_y());
                            if(quantity>Integer.parseInt(itemInStock))
                            {
                                productResponseDataVariantObj.setQuantit_y(itemInStock);
                                mainMap.put(varientID,productResponseDataNestedObj);
                                Preference.setMapJson(Utility.hashMapToJson(mainMap));
                                Preference.setCartCount(Utility.getTotalQuantityCount());
                            }
                        }
                    }
                }
            }
        }
    }


*//*
    *//**
     * store category parent and child data in database table
     *
     * @param data
     *//*
    public void insertCategoryData(ArrayList<CategoryResponseData> data) {

        if (data.size() > 0) {
            for (int i = 0; i < data.size(); i++) {
                CategoryParentDataBean parentData = new CategoryParentDataBean();
                String id = data.get(i).getId();
                String parent_id = data.get(i).getParent_id();
                String name = data.get(i).getName();
                String status = data.get(i).getStatus();
                String priority = data.get(i).getPriority();

                parentData.setId(id);
                parentData.setParent_id(parent_id);
                parentData.setName(name);
                parentData.setStatus(status);
                parentData.setPriority(priority);
                long insert = CategoryParentTable.getInstance().add(parentData);
                //Log.d("cat_parent_data",""+insert);
                inCategoryChildData(data.get(i).getChildren());
            }
        }
    }

    *//**
     * insert child data of category in childCategoryTable
     *
     * @param data
     *//*
    public void inCategoryChildData(ArrayList<CategoryResponseDataChildren> data) {
        if (data != null)
            if (data.size() > 0) {
                for (int i = 0; i < data.size(); i++) {
                    CategoryChildDataBean entrydata = new CategoryChildDataBean();
                    String id = data.get(i).getId();
                    String parent_id = data.get(i).getParent_id();
                    String name = data.get(i).getName();
                    String status = data.get(i).getStatus();
                    String store_id = Constant.STORE_ID;
                    String image = data.get(i).getImage();
                    String priority = data.get(i).getPriority();
                    entrydata.setId(id);
                    entrydata.setParent_id(parent_id);
                    entrydata.setName(name);
                    entrydata.setStatus(status);
                    entrydata.setStore_id(store_id);
                    entrydata.setImage(image);
                    entrydata.setPriority(priority);
                    long insert = CategorychildTable.getInstance().add(entrydata);
                    //Log.d("cat_child_data", "" + insert);
                }
            }
    }


    public void updatingLastDate(String lastDate) {
        storeResponseData.setUpdated_date(lastDate);
        String jsonMake = new GsonBuilder().create().toJson(storeResponseData);
        Preference.setSplashInsertedStatus(jsonMake);
    }



    *//** first app user *//*
    public static final int AID_APP = 10000;

    *//** offset for uid ranges for each user *//*
    public static final int AID_USER = 100000;

    private static String getForegroundApp() {
        File[] files = new File("/proc").listFiles();
        int lowestOomScore = Integer.MAX_VALUE;
        String foregroundProcess = null;

        for (File file : files) {
            if (!file.isDirectory()) {
                continue;
            }

            int pid;
            try {
                pid = Integer.parseInt(file.getName());
            } catch (NumberFormatException e) {
                continue;
            }

            try {
                String cgroup = read(String.format("/proc/%d/cgroup", pid));

                String[] lines = cgroup.split("\n");

                if (lines.length != 2) {
                    continue;
                }

                String cpuSubsystem = lines[0];
                String cpuaccctSubsystem = lines[1];

                if (!cpuaccctSubsystem.endsWith(Integer.toString(pid))) {
                    // not an application process
                    continue;
                }

                if (cpuSubsystem.endsWith("bg_non_interactive")) {
                    // background policy
                    continue;
                }

                String cmdline = read(String.format("/proc/%d/cmdline", pid));

                if (cmdline.contains("com.android.systemui")) {
                    continue;
                }

                int uid = Integer.parseInt(
                        cpuaccctSubsystem.split(":")[2].split("/")[1].replace("uid_", ""));
                if (uid >= 1000 && uid <= 1038) {
                    // system process
                    continue;
                }

                int appId = uid - AID_APP;
                int userId = 0;
                // loop until we get the correct user id.
                // 100000 is the offset for each user.
                while (appId > AID_USER) {
                    appId -= AID_USER;
                    userId++;
                }

                if (appId < 0) {
                    continue;
                }

                // u{user_id}_a{app_id} is used on API 17+ for multiple user account support.
                // String uidName = String.format("u%d_a%d", userId, appId);

                File oomScoreAdj = new File(String.format("/proc/%d/oom_score_adj", pid));
                if (oomScoreAdj.canRead()) {
                    int oomAdj = Integer.parseInt(read(oomScoreAdj.getAbsolutePath()));
                    if (oomAdj != 0) {
                        continue;
                    }
                }

                int oomscore = Integer.parseInt(read(String.format("/proc/%d/oom_score", pid)));
                if (oomscore < lowestOomScore) {
                    lowestOomScore = oomscore;
                    foregroundProcess = cmdline;
                }

            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return foregroundProcess;
    }

    private static String read(String path) throws IOException {
        StringBuilder output = new StringBuilder();
        BufferedReader reader = new BufferedReader(new FileReader(path));
        output.append(reader.readLine());
        for (String line = reader.readLine(); line != null; line = reader.readLine()) {
            output.append('\n').append(line);
        }
        reader.close();
        return output.toString();
    }
    public boolean isAppIsInForground(){

        String appName= getForegroundApp();
        String packageName= con.getApplicationContext().getPackageName();

        System.out.println("apps : packageName "+ packageName.trim());


        if(appName!=null){
            System.out.println("apps :"+ getForegroundApp().trim());
            if(packageName.trim().equals(appName.trim())){
                return true;
            }
        } else {
            System.out.println("apps :"+ getForegroundApp()); }


        return false;
    }*/
}


