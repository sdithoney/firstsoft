package com.example.moni.imenusoft.serverRequests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by enuke on 10/10/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class MenuResponseDataMeal implements Serializable {


    @SerializedName("meal_id")
    @Expose
    private String meal_id;

    @SerializedName("meal_value")
    @Expose
    private String meal_value;

    @SerializedName("meal_data")
    @Expose
    private ArrayList<MenuResponseCategory> meal_data;

    public MenuResponseDataMeal() {
    }

    public MenuResponseDataMeal(String meal_id, String meal_value, ArrayList<MenuResponseCategory> meal_data) {
        this.meal_id = meal_id;
        this.meal_value = meal_value;
        this.meal_data = meal_data;
    }

    public String getMeal_id() {
        return meal_id;
    }

    public void setMeal_id(String meal_id) {
        this.meal_id = meal_id;
    }

    public String getMeal_value() {
        return meal_value;
    }

    public void setMeal_value(String meal_value) {
        this.meal_value = meal_value;
    }

    public ArrayList<MenuResponseCategory> getMeal_data() {
        return meal_data;
    }

    public void setMeal_data(ArrayList<MenuResponseCategory> meal_data) {
        this.meal_data = meal_data;
    }
}

