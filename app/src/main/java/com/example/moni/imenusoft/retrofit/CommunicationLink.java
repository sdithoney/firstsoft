package com.example.moni.imenusoft.retrofit;

import  com.example.moni.imenusoft.retrofit.service.PrivateDealService;
import  com.example.moni.imenusoft.serverRequests.CancelOrderResponse;
import  com.example.moni.imenusoft.serverRequests.CancelRequest;
import  com.example.moni.imenusoft.serverRequests.CreateOrderResponse;
import  com.example.moni.imenusoft.serverRequests.GetOrderRequest;
import  com.example.moni.imenusoft.serverRequests.LoginRequest;
import  com.example.moni.imenusoft.serverRequests.LoginResponse;
import  com.example.moni.imenusoft.serverRequests.MenuRequest;
import  com.example.moni.imenusoft.serverRequests.MenuResponse;
import  com.example.moni.imenusoft.serverRequests.OrderRequest;
import  com.example.moni.imenusoft.serverRequests.UpdateOrderRequest;
import  com.example.moni.imenusoft.serverRequests.UpdateOrderResponse;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.OrderDetailResponse;

import java.io.IOException;

import retrofit2.Call;


public class CommunicationLink {

    public static <T> T getResponse(Call call) throws IOException {
        return (T) call.execute().body();
    }

    public static <T> T getErrorResponse(Call call) throws IOException {
        return (T) call.execute().errorBody();
    }

   /* public static JSONObject actionLogin(JSONObject loginBody) throws Exception {
        PrivateDealService service = CommunicationApi.getPrivateDealService();
        JSONObject responseLogin = getResponse(service.login(loginBody));
        return responseLogin;
    }
*/


/*    public static ImageUploadResponse actionImageUpload(  MultipartBody.Part body) throws Exception {
        PrivateDealService service = CommunicationApi.getPrivateDealService();
        String lang=Preference.getLangCode(AppController.getInstance());
        ImageUploadResponse responseInterest = getResponse(service.uploadImage(lang,body,Integer.parseInt(Preference.getUserId())));
        return responseInterest;
    }*/

    public static LoginResponse actionLoginNew(LoginRequest loginBody) throws Exception {
        PrivateDealService service = CommunicationApi.getPrivateDealService();
     //   String lang=Preference.getLangCode(AppController.getInstance());
     //   Log.i("lang",lang);
        LoginResponse responseLogin = getResponse(service.login(loginBody));
        return responseLogin;
    }

    public static OrderDetailResponse actiongetOrderDetail(GetOrderRequest loginBody) throws Exception {
        PrivateDealService service = CommunicationApi.getPrivateDealService();
        //   String lang=Preference.getLangCode(AppController.getInstance());
        //   Log.i("lang",lang);
        OrderDetailResponse responseLogin = getResponse(service.getOrder(loginBody));
        return responseLogin;
    }

    public static CancelOrderResponse actionCancelOrder(CancelRequest loginBody) throws Exception {
        PrivateDealService service = CommunicationApi.getPrivateDealService();
        //   String lang=Preference.getLangCode(AppController.getInstance());
        //   Log.i("lang",lang);
        CancelOrderResponse responseLogin = getResponse(service.cancelOrder(loginBody));
        return responseLogin;
    }


    public static UpdateOrderResponse actionUpdateOrder(UpdateOrderRequest loginBody) throws Exception {
        PrivateDealService service = CommunicationApi.getPrivateDealService();
        //   String lang=Preference.getLangCode(AppController.getInstance());
        //   Log.i("lang",lang);
        UpdateOrderResponse responseLogin = getResponse(service.updateOrder(loginBody));
        return responseLogin;
    }



    public static MenuResponse actionMenuNew(MenuRequest loginBody) throws Exception {
        PrivateDealService service = CommunicationApi.getPrivateDealService();
        //   String lang=Preference.getLangCode(AppController.getInstance());
        //   Log.i("lang",lang);
        MenuResponse responseLogin = getResponse(service.menulist(loginBody));
        return responseLogin;
    }

    public static CreateOrderResponse orderNew(OrderRequest loginBody) throws Exception {
        PrivateDealService service = CommunicationApi.getPrivateDealService();
        //   String lang=Preference.getLangCode(AppController.getInstance());
        //   Log.i("lang",lang);
        CreateOrderResponse responseLogin = getResponse(service.createOrder(loginBody));
        return responseLogin;
    }



    public static UpdateOrderResponse updateDetailOrder(OrderRequest loginBody) throws Exception {
        PrivateDealService service = CommunicationApi.getPrivateDealService();
        //   String lang=Preference.getLangCode(AppController.getInstance());
        //   Log.i("lang",lang);
        UpdateOrderResponse responseLogin = getResponse(service.updateDetailOrder(loginBody));
        return responseLogin;
    }


}