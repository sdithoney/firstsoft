package com.example.moni.imenusoft.firebasenotification.notificationJson;
import com.example.moni.imenusoft.serverRequests.MenuResponseCategory;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class Day_data{
  @SerializedName("meal_value")
  @Expose
  private String meal_value;
  @SerializedName("meal_id")
  @Expose
  private String meal_id;
  @SerializedName("meal_data")
  @Expose
  private List<MenuResponseCategory> meal_data;
  public void setMeal_value(String meal_value){
   this.meal_value=meal_value;
  }
  public String getMeal_value(){
   return meal_value;
  }
  public void setMeal_id(String meal_id){
   this.meal_id=meal_id;
  }
  public String getMeal_id(){
   return meal_id;
  }
  public void setMeal_data(List<MenuResponseCategory> meal_data){
   this.meal_data=meal_data;
  }
  public List<MenuResponseCategory> getMeal_data(){
   return meal_data;
  }
}