package com.example.moni.imenusoft.serverRequests.orderServerRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class OrderDetailResponse implements Serializable {
  @SerializedName("data")
  @Expose
  private List<Data> data;
  @SerializedName("success")
  @Expose
  private Boolean success;
  public OrderDetailResponse(){
  }
  public OrderDetailResponse(List<Data> data, Boolean success){
   this.data=data;
   this.success=success;
  }
  public void setData(List<Data> data){
   this.data=data;
  }
  public List<Data> getData(){
   return data;
  }
  public void setSuccess(Boolean success){
   this.success=success;
  }
  public Boolean getSuccess(){
   return success;
  }
}