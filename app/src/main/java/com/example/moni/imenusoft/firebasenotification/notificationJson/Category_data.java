package com.example.moni.imenusoft.firebasenotification.notificationJson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class Category_data{
  @SerializedName("category_id")
  @Expose
  private String category_id;
  @SerializedName("user_id")
  @Expose
  private String user_id;
  @SerializedName("week_id")
  @Expose
  private String week_id;
  @SerializedName("menu_name")
  @Expose
  private String menu_name;
  @SerializedName("id")
  @Expose
  private String id;
  @SerializedName("hospital_id")
  @Expose
  private String hospital_id;
  @SerializedName("day_id")
  @Expose
  private String day_id;
  @SerializedName("meal_id")
  @Expose
  private String meal_id;
  @SerializedName("symbol_id")
  @Expose
  private String symbol_id;
  public void setCategory_id(String category_id){
   this.category_id=category_id;
  }
  public String getCategory_id(){
   return category_id;
  }
  public void setUser_id(String user_id){
   this.user_id=user_id;
  }
  public String getUser_id(){
   return user_id;
  }
  public void setWeek_id(String week_id){
   this.week_id=week_id;
  }
  public String getWeek_id(){
   return week_id;
  }
  public void setMenu_name(String menu_name){
   this.menu_name=menu_name;
  }
  public String getMenu_name(){
   return menu_name;
  }
  public void setId(String id){
   this.id=id;
  }
  public String getId(){
   return id;
  }
  public void setHospital_id(String hospital_id){
   this.hospital_id=hospital_id;
  }
  public String getHospital_id(){
   return hospital_id;
  }
  public void setDay_id(String day_id){
   this.day_id=day_id;
  }
  public String getDay_id(){
   return day_id;
  }
  public void setMeal_id(String meal_id){
   this.meal_id=meal_id;
  }
  public String getMeal_id(){
   return meal_id;
  }
  public void setSymbol_id(String symbol_id){
   this.symbol_id=symbol_id;
  }
  public String getSymbol_id(){
   return symbol_id;
  }
}