package com.example.moni.imenusoft;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.retrofit.loader.LoginLoader;
import  com.example.moni.imenusoft.serverRequests.LoginRequest;
import  com.example.moni.imenusoft.serverRequests.LoginResponse;
import  com.example.moni.imenusoft.utils.Preference;
import  com.example.moni.imenusoft.utils.Utility;
import  com.example.moni.imenusoft.utils.ValidationManager;
import com.google.gson.Gson;

/**
 * Created by enuke on 28/9/17.
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {


    private ImageView logo;
    private TextView labelText;
    private EditText userId;
    private TextView passwordLabel;
    private TextView signIn;
    private EditText password;
    private ProgressDialog progressBar;
    LoginRequest loginRequest;
    private RelativeLayout root;

    ValidationManager validationManager;

    Snackbar snackbar;
    private TextView bottomView;
    private TextView info;

    @Override

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        try {
            initView();
        } catch (Exception e) {
            e.printStackTrace();
            //   //Utility.showHelpMessages("Debug Message",e.getMessage(),this);
        }
        validationManager = new ValidationManager();
    }




    private void initView() throws Exception {

        progressBar = new ProgressDialog(LoginActivity.this);
        logo = findViewById(R.id.logo);
        labelText = findViewById(R.id.label_text);
        userId = findViewById(R.id.user_id);
        passwordLabel = findViewById(R.id.password_label);
        password = findViewById(R.id.password);
        signIn = findViewById(R.id.signIn);

        signIn.setOnClickListener(this);

        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int actionId, KeyEvent keyEvent) {

                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    if (Utility.isInternetConnected(LoginActivity.this)) {

                        if (isValidData(userId.getText().toString().trim(), password.getText().toString().trim())) {

                            progressBar.setCancelable(false);
                            progressBar.setTitle("Loading");
                            progressBar.setMessage("Please wait...");
                            progressBar.show();
                            try {
                                getServerRequest(userId.getText().toString().trim(), password.getText().toString().trim());

                            } catch (Exception ex) {
                                //Utility.showHelpMessages("Debug Message",ex.getMessage(), LoginActivity.this);
                            }

                        }
                    } else
                        Utility.showToast(LoginActivity.this, "No internet Connectivity");
                }
                return false;
            }
        });

        root = findViewById(R.id.root);
        bottomView = findViewById(R.id.bottomView);

        actionKeyBoardUpOrDown();
        info = (TextView) findViewById(R.id.info);
    }

    @Override
    public void onClick(View view) {


    }

    private void getServerRequest(String email, String password) {
        loginRequest = new LoginRequest(email, password, Preference.getDeviceToken());

        getSupportLoaderManager().initLoader(0, null, responseLoginCallback);

    }

    LoaderManager.LoaderCallbacks<LoginResponse> responseLoginCallback = new LoaderManager.LoaderCallbacks<LoginResponse>() {
        @Override
        public Loader<LoginResponse> onCreateLoader(int id, Bundle args) {
            return new LoginLoader(LoginActivity.this, loginRequest);
        }

        @Override
        public void onLoadFinished(Loader<LoginResponse> loader, LoginResponse data) {
            //    hideProgressDialog();

            try {


                //Utility.showHelpMessages("Debug Detail",new Gson().toJson(data).toString(), LoginActivity.this);

                progressBar.dismiss();
                Utility.showSnackbar(root, data.getMassage());


                if (data != null) {
                    Log.d("json obj", "value :" + data.toString());
                    if (data.isSuccess()) {

                        Preference.setId(data.getData().getUser().getId());
                        Preference.setUserId(data.getData().getUser().getEmail());
                        Preference.setIsLogin(true);
                        Preference.setHospitalId(data.getData().getHospital().getId());
                        Preference.setUserName(data.getData().getUser().getFirst_name() + " " + data.getData().getUser().getLast_name());

                        Preference.setUserObjectJson(new Gson().toJson(data.getData()).toString());

                        Utility.traverseToHomePage(LoginActivity.this);

                    } else {

                    }
                }
            } catch (Exception ex) {
                //Utility.showHelpMessages("Debug Detail",ex.getMessage(), LoginActivity.this);

            }
            getSupportLoaderManager().destroyLoader(0);
        }

        @Override
        public void onLoaderReset(Loader<LoginResponse> loader) {
            progressBar.dismiss();
        }
    };


    private boolean isValidData(String id, String userPassword) {

        if (validationManager.isEmpty(id)) {
            //   Utility.showSnackbar(root, "Login Id can not be blank.");
            userId.setError("Login Id can not be blank.");
            userId.requestFocus();
            return false;
        } else userId.setError(null);

        if (!validationManager.isValidMail(id)) {
            //   Utility.showSnackbar(root, "Enter a valid Login Id");
            userId.setError("Enter a valid Login Id");
            userId.requestFocus();
            return false;
        } else userId.setError(null);

        if (validationManager.isEmpty(userPassword)) {
            //    Utility.showSnackbar(root, "Password can not be blank.");
            password.setError("Password can not be blank.");
            password.requestFocus();
            return false;
        } else password.setError(null);

        if (!validationManager.isValidPassword(userPassword)) {
            //  Utility.showSnackbar(root, "Enter a valid password");
            password.setError("Enter a valid password");
            password.requestFocus();
            return false;
        } else password.setError(null);

        return true;
    }


    private void actionKeyBoardUpOrDown() {

        root.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                int heightDiff = root.getRootView().getHeight() - root.getHeight();
                if (heightDiff > dpToPx(LoginActivity.this, 200)) {
                    // if more than 200 dp, it's probably a keyboard...
                    bottomView.setVisibility(View.GONE);
                    info.setVisibility(View.GONE);
                } else {
                    bottomView.setVisibility(View.VISIBLE);
                    info.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    public static float dpToPx(Context context, float valueInDp) {
        DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics);
    }
}
