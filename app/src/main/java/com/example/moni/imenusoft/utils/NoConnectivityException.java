package com.example.moni.imenusoft.utils;

import java.io.IOException;

/**
 * Created by enuke on 7/18/17.
 */

public class NoConnectivityException extends IOException {

    @Override
    public String getMessage() {
     //   Toast.makeText(AppController.getInstance(),AppController.getInstance().getResources().getString(R.string.no_internet),Toast.LENGTH_SHORT).show();
        return "No Internet";
    }
}
