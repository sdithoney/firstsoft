package com.example.moni.imenusoft.serverRequests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by enuke on 1/10/17.
 */


@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponseDataUser implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("role_id")
    @Expose
    private String role_id;

    @SerializedName("profile_image")
    @Expose
    private String profile_image;

    @SerializedName("first_name")
    @Expose
    private String first_name;

    @SerializedName("last_name")
    @Expose
    private String last_name;

    @SerializedName("email")
    @Expose
    private String email;

    @SerializedName("contact_number")
    @Expose
    private String contact_number;

    @SerializedName("verified")
    @Expose
    private String verified;

    @SerializedName("hospital_id")
    @Expose
    private String hospital_id;

    @SerializedName("device_id")
    @Expose
    private String device_id;

    @SerializedName("device_token")
    @Expose
    private String device_token;

    @SerializedName("device_type")
    @Expose
    private String device_type;

    @SerializedName("api_access_key")
    @Expose
    private String api_access_key;

    @SerializedName("app_version")
    @Expose
    private String app_version;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("remember_token")
    @Expose
    private String remember_token;

    @SerializedName("token")
    @Expose
    private String token;

    @SerializedName("created_at")
    @Expose
    private String created_at;

    @SerializedName("updated_at")
    @Expose
    private String updated_at;

    @SerializedName("deleted_at")
    @Expose
    private String deleted_at;

    @SerializedName("login_type")
    @Expose
    private String login_type;

    public LoginResponseDataUser() {
        this.id = id;
    }

    public LoginResponseDataUser(String id, String role_id, String profile_image, String first_name, String last_name, String email, String contact_number, String verified, String hospital_id, String device_id, String device_token, String device_type, String api_access_key, String app_version, String status, String remember_token, String token, String created_at, String updated_at, String deleted_at, String login_type) {
        this.id = id;
        this.role_id = role_id;
        this.profile_image = profile_image;
        this.first_name = first_name;
        this.last_name = last_name;
        this.email = email;
        this.contact_number = contact_number;
        this.verified = verified;
        this.hospital_id = hospital_id;
        this.device_id = device_id;
        this.device_token = device_token;
        this.device_type = device_type;
        this.api_access_key = api_access_key;
        this.app_version = app_version;
        this.status = status;
        this.remember_token = remember_token;
        this.token = token;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.deleted_at = deleted_at;
        this.login_type = login_type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getRole_id() {
        return role_id;
    }

    public void setRole_id(String role_id) {
        this.role_id = role_id;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getContact_number() {
        return contact_number;
    }

    public void setContact_number(String contact_number) {
        this.contact_number = contact_number;
    }

    public String getVerified() {
        return verified;
    }

    public void setVerified(String verified) {
        this.verified = verified;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getApi_access_key() {
        return api_access_key;
    }

    public void setApi_access_key(String api_access_key) {
        this.api_access_key = api_access_key;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRemember_token() {
        return remember_token;
    }

    public void setRemember_token(String remember_token) {
        this.remember_token = remember_token;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getDeleted_at() {
        return deleted_at;
    }

    public void setDeleted_at(String deleted_at) {
        this.deleted_at = deleted_at;
    }

    public String getLogin_type() {
        return login_type;
    }

    public void setLogin_type(String login_type) {
        this.login_type = login_type;
    }
}
