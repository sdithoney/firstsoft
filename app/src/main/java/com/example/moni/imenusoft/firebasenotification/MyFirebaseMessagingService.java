package com.example.moni.imenusoft.firebasenotification;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;


import com.example.moni.imenusoft.MainActivity;
import com.example.moni.imenusoft.R;
import com.example.moni.imenusoft.firebasenotification.notificationJson.NotificationResponse;
import com.example.moni.imenusoft.utils.Utility;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.google.gson.Gson;


/**
 * Created by enuke on 19/9/16.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    String messageDisplay = "";
    String orderNumber = "";
    String orderId = "";
    String status = "";
    String type ="";
    String imageUrl="";
    String isTopRated="";

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        //Displaying data in log
        //It is optional

        Utility.showLog("TAG",remoteMessage.getData().get("tag"));


        if(!remoteMessage.getData().get("tag").startsWith("[")||remoteMessage.getData().get("tag").length()>3) {


            NotificationResponse notificationResponseObj =  new Gson().fromJson(remoteMessage.getData().get("tag"), NotificationResponse.class);

            if(notificationResponseObj.getVisibility()){

                sendNotification(remoteMessage.getData().get("body"));

                Intent myNotification = new Intent("android.intent.action.MyAction").putExtra("notification", remoteMessage.getData().get("tag"));

                // Send Notification to Application
                this.sendBroadcast(myNotification);
                this.stopSelf();
            } else {

                Intent myNotification = new Intent("android.intent.action.MyAction").putExtra("notification", remoteMessage.getData().get("tag"));

                // Send Notification to Application
                this.sendBroadcast(myNotification);
                this.stopSelf();
            }

        } else{
            sendNotification(remoteMessage.getData().get("body"));

        }

    }

    //This method is only generating push notification
    //It is same as we did in earlier posts

    private void sendNotification(String messageBody) {
        PendingIntent pendingIntent;
        Intent intent1 ;

        intent1 = new Intent(this, MainActivity.class);

        intent1.putExtra("order_number", orderNumber);
        intent1.putExtra("order_id", orderId);
        intent1.putExtra("order_status", status);
        intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        pendingIntent = PendingIntent.getActivity(this, 0, intent1,
                PendingIntent.FLAG_ONE_SHOT);


        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);


        NotificationCompat.Builder notificationBuilder  = new NotificationCompat.Builder(this)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(getString(R.string.app_name))
                .setStyle(new NotificationCompat.BigTextStyle().bigText(messageBody))
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setVibrate(new long[] { 1000, 1000, 1000, 1000, 1000 });

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0, notificationBuilder.build());

    }
}
