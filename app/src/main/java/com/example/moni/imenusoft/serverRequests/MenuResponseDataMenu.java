package com.example.moni.imenusoft.serverRequests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by enuke on 10/10/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class MenuResponseDataMenu implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("menu_name")
    @Expose
    private String menu_name;

    @SerializedName("user_id")
    @Expose
    private String user_id;

    @SerializedName("hospital_id")
    @Expose
    private String hospital_id;

    @SerializedName("week_id")
    @Expose
    private String week_id;

    @SerializedName("day_id")
    @Expose
    private String day_id;

    @SerializedName("meal_id")
    @Expose
    private String meal_id;

    @SerializedName("category_id")
    @Expose
    private String category_id;

    @SerializedName("symbol_id")
    @Expose
    private String symbol_id;

    private boolean isSelected = false;

    public MenuResponseDataMenu(String id, String menu_name, String user_id, String hospital_id, String week_id, String day_id, String meal_id, String category_id, String symbol_id) {
        this.id = id;
        this.menu_name = menu_name;
        this.user_id = user_id;
        this.hospital_id = hospital_id;
        this.week_id = week_id;
        this.day_id = day_id;
        this.meal_id = meal_id;
        this.category_id = category_id;
        this.symbol_id = symbol_id;
    }

    public MenuResponseDataMenu() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getMenu_name() {
        return menu_name;
    }

    public void setMenu_name(String menu_name) {
        this.menu_name = menu_name;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getWeek_id() {
        return week_id;
    }

    public void setWeek_id(String week_id) {
        this.week_id = week_id;
    }

    public String getDay_id() {
        return day_id;
    }

    public void setDay_id(String day_id) {
        this.day_id = day_id;
    }

    public String getMeal_id() {
        return meal_id;
    }

    public void setMeal_id(String meal_id) {
        this.meal_id = meal_id;
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getSymbol_id() {
        return symbol_id;
    }

    public void setSymbol_id(String symbol_id) {
        this.symbol_id = symbol_id;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(boolean selected) {
        isSelected = selected;
    }
}
