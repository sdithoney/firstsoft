package com.example.moni.imenusoft.serverRequests;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class Patient{
  @SerializedName("allergies")
  @Expose
  private String allergies;
  @SerializedName("bed_num")
  @Expose
  private String bed_num;
  @SerializedName("gender")
  @Expose
  private String gender;
  @SerializedName("last_name")
  @Expose
  private String last_name;
  @SerializedName("serve")
  @Expose
  private String serve;
  @SerializedName("ward")
  @Expose
  private String ward;
  @SerializedName("first_name")
  @Expose
  private String first_name;
  public void setAllergies(String allergies){
   this.allergies=allergies;
  }
  public String getAllergies(){
   return allergies;
  }
  public void setBed_num(String bed_num){
   this.bed_num=bed_num;
  }
  public String getBed_num(){
   return bed_num;
  }
  public void setGender(String gender){
   this.gender=gender;
  }
  public String getGender(){
   return gender;
  }
  public void setLast_name(String last_name){
   this.last_name=last_name;
  }
  public String getLast_name(){
   return last_name;
  }
  public void setServe(String serve){
   this.serve=serve;
  }
  public String getServe(){
   return serve;
  }
  public void setWard(String ward){
   this.ward=ward;
  }
  public String getWard(){
   return ward;
  }
  public void setFirst_name(String first_name){
   this.first_name=first_name;
  }
  public String getFirst_name(){
   return first_name;
  }
}