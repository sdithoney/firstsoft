package com.example.moni.imenusoft.serverRequests;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class UpdateOrderRequest{
  @SerializedName("order_status")
  @Expose
  private String order_status;
  @SerializedName("user_id")
  @Expose
  private String user_id;
  @SerializedName("order_num")
  @Expose
  private String order_num;
  @SerializedName("hospital_id")
  @Expose
  private String hospital_id;
  public UpdateOrderRequest(){
  }
  public UpdateOrderRequest(String order_status, String user_id, String order_num, String hospital_id){
   this.order_status=order_status;
   this.user_id=user_id;
   this.order_num=order_num;
   this.hospital_id=hospital_id;
  }
  public void setOrder_status(String order_status){
   this.order_status=order_status;
  }
  public String getOrder_status(){
   return order_status;
  }
  public void setUser_id(String user_id){
   this.user_id=user_id;
  }
  public String getUser_id(){
   return user_id;
  }
  public void setOrder_num(String order_num){
   this.order_num=order_num;
  }
  public String getOrder_num(){
   return order_num;
  }
  public void setHospital_id(String hospital_id){
   this.hospital_id=hospital_id;
  }
  public String getHospital_id(){
   return hospital_id;
  }
}