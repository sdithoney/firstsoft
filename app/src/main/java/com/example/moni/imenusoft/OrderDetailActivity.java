package com.example.moni.imenusoft;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.adapters.MealAdapter;
import  com.example.moni.imenusoft.firebasenotification.notificationJson.NotificationResponse;
import  com.example.moni.imenusoft.retrofit.loader.UpdateOrderDataLoader;
import  com.example.moni.imenusoft.serverRequests.OrderRequest;
import  com.example.moni.imenusoft.serverRequests.UpdateOrderResponse;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Category_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Day_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Hospital_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Meal_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Order_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Order_details;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Patient_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.User_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Week_data;
import  com.example.moni.imenusoft.utils.OnDialogClick;
import  com.example.moni.imenusoft.utils.Preference;
import  com.example.moni.imenusoft.utils.RecyclerItemClickListener;
import  com.example.moni.imenusoft.utils.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by enuke on 28/10/17.
 */

public class OrderDetailActivity extends AppCompatActivity {

    private RelativeLayout root;
    private TextView orderNumber;
    private TextView orderDate;
    private TextView orderStatus;
    private Spinner chooseOrderStatus;
    private TextView showOrderMenu;
    private RecyclerView MenuResponseList;
    MealAdapter adapter;
    ArrayList<Day_data> menuArrayList;
    ArrayList<String> status = new ArrayList<>();
    OrderRequest orderRequest;
    ImageView done, cancel;
    Data orderData;
    private ProgressDialog progressBar;
    String patientId = "", orderNumberString = "";
    private RelativeLayout mRoot;
    private TextView mOrderNumber;
    private TextView mOrderDate;
    private TextView mOrderStatus;
    private Spinner mChooseOrderStatus;
    private TextView mShowOrderMenu;
    private RecyclerView mMenuResponseList;
    private TextView mTextViewUpdateStatus;
    private BroadcastReceiver notificationBroadCastReceiver;
    boolean isDataChanged = false;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        try{

            setContentView(R.layout.activity_order_detail);

            progressBar = new ProgressDialog(OrderDetailActivity.this);
            status.add("Update Status");
            // status.add("Created");
            status.add("Completed");
            status.add("Canceled");

            if (getIntent().hasExtra("data")) {
                orderData = (Data) getIntent().getSerializableExtra("data");

                patientId = orderData.getOrder_details().get(0).getHospital_data().get(0).getUser_data().get(0).getPatient_id();
                orderNumberString = orderData.getOrder_num();
            }


            menuArrayList = new ArrayList<>();
            initView();
            fillView();

        } catch (Exception ex) {
            Utility.showHelpMessages("Crash", ex.getMessage(), this, new OnDialogClick() {
                @Override
                public void onDialogOkClick() {

                }

                @Override
                public void onDialogCancleClick() {

                }
            });
        }

    }

    private void fillView() {

        orderNumber.setText("Order Number -" + orderData.getOrder_num());
        orderStatus.setText("Status : " + orderData.getOrder_details().get(0).getHospital_data().get(0).getUser_data().get(0).getPatient_data().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getOrder_status());
        orderDate.setText("Order Date : " + orderData.getOrder_details().get(0).getHospital_data().get(0).getUser_data().get(0).getPatient_data().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getOrder_date());


        for (Order_details order_detailsObj : orderData.getOrder_details()) {
            for (Hospital_data hospital_data : order_detailsObj.getHospital_data()) {

                for (User_data user_data : hospital_data.getUser_data()) {

                    for (Patient_data patient_data : user_data.getPatient_data()) {

                        for (Week_data week_data : patient_data.getWeek_data()) {

                            for (Day_data day_data : week_data.getDay_data()) {

                                day_data.setDayValue(week_data.getDay_value());
                                day_data.setStatus(day_data.getMeal_data().get(0).getOrder_data().get(0).getCategory_data().get(0).getStatus());
                                menuArrayList.add(day_data);
                            }
                        }
                    }
                }
            }

        }

        if (adapter != null) {
            adapter.notifyDataSetChanged();
        }
    }

    private void initView() {

        root = findViewById(R.id.root);
        orderNumber = findViewById(R.id.orderNumber);
        orderDate = findViewById(R.id.orderDate);
        orderStatus = findViewById(R.id.orderStatus);
        chooseOrderStatus = findViewById(R.id.chooseOrderStatus);
        showOrderMenu = findViewById(R.id.showOrderMenu);
        MenuResponseList = findViewById(R.id.MenuResponseList);

        done = findViewById(R.id.done);
        cancel = findViewById(R.id.cancel);

        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utility.isInternetConnected(OrderDetailActivity.this)) {

                    if(chooseOrderStatus.getSelectedItemPosition()==1||chooseOrderStatus.getSelectedItemPosition()==0){

                        if(isOrderCompletable()){
                            getOrderDetail();

                        } else {

                            Utility.showSnackbar(root,"Order will only complete when all Menus are served.!!");
                        }
                    }  else {
                        getOrderDetail();

                    }


                } else {
                    Utility.showSnackbar(root, "No Internet Connectivity.");
                }
            }
        });

        MenuResponseList.addOnItemTouchListener(new RecyclerItemClickListener(OrderDetailActivity.this, new RecyclerItemClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {

             //  if(menuArrayList.get(position).getStatus().equalsIgnoreCase("0")){

                   showOrderStatus(position);

            //   } else{
            //       Utility.showSnackbar(root,"This Meal is either served or Canceled.");
            //   }
            }
        }));

        setRecylerView();
        setOrderStatus();
        mRoot = findViewById(R.id.root);
        mOrderNumber = findViewById(R.id.orderNumber);
        mOrderDate = findViewById(R.id.orderDate);
        mOrderStatus = findViewById(R.id.orderStatus);
        mChooseOrderStatus = findViewById(R.id.chooseOrderStatus);
        mShowOrderMenu = findViewById(R.id.showOrderMenu);
        mMenuResponseList = findViewById(R.id.MenuResponseList);
        mTextViewUpdateStatus = findViewById(R.id.textViewUpdateStatus);

        mTextViewUpdateStatus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Utility.isInternetConnected(OrderDetailActivity.this)) {

                    if(chooseOrderStatus.getSelectedItemPosition()==1||chooseOrderStatus.getSelectedItemPosition()==0){

                        if(isOrderCompletable()){
                            getOrderDetail();

                        } else {

                            Utility.showSnackbar(root,"Order will only complete when all Menus are served.!!");
                        }
                    }  else {
                        getOrderDetail();

                    }


                } else {
                    Utility.showSnackbar(root, "No Internet Connectivity.");
                }
            }
        });

        chooseOrderStatus.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
               // changeSubMenu(i);

                if(adapter!=null){
                    adapter.getOrderStatus(""+i);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    private void setRecylerView() {

        MenuResponseList.setLayoutManager(new LinearLayoutManager(this));


        adapter = new MealAdapter(this, menuArrayList);
        MenuResponseList.setAdapter(adapter);
    }

    private void setOrderStatus() {

        ArrayAdapter<String> statusAdapter = new ArrayAdapter<String>(OrderDetailActivity.this, R.layout.spinner_list_item, status);
        chooseOrderStatus.setAdapter(statusAdapter);
    }

    private void showOrderStatus(final int position) {

        final CharSequence[] items = {
                "Served", "Canceled","Default"
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.MyAlertDialogStyle);
        builder.setTitle("Update meal status to");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {


                if (adapter!=null){

                    if(items[item].toString().equalsIgnoreCase("Served")){

                     //   menuArrayList.get(position).setStatus("1");
                        adapter.getOrderStatus("1",position);

                    } else  if(items[item].toString().equalsIgnoreCase("Canceled")){

                     //   menuArrayList.get(position).setStatus("2");
                        adapter.getOrderStatus("2",position);


                    }else  if(items[item].toString().equalsIgnoreCase("Default")){

                     //   menuArrayList.get(position).setStatus("0");
                        adapter.getOrderStatus("0",position);


                    }
                }



            }
        });

        AlertDialog alert = builder.create();
        alert.getWindow().setGravity(Gravity.BOTTOM);

        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        lp.copyFrom(alert.getWindow().getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        alert.show();
        alert.getWindow().setAttributes(lp);
        alert.show();

    }

    private void getOrderDetail() {


        //String order_date, List<Order_data> order_data, String user_id, Patient patient, String patient_id, String hospital_id, String order_num

        if (orderNumber.length() > 0) {

            progressBar.setTitle("Loading..");
            progressBar.setMessage("Please Wait, Updating Order");
            progressBar.setCancelable(false);
            progressBar.show();

            if(chooseOrderStatus.getSelectedItemPosition()!=0)
            orderRequest = new OrderRequest(Utility.getOrderDate(Calendar.getInstance()),
                    getOrderData(), Preference.getId(), patientId, Preference.getHospitalId(), orderNumberString, chooseOrderStatus.getSelectedItem().toString());

            else{

                orderRequest = new OrderRequest(Utility.getOrderDate(Calendar.getInstance()),
                        getOrderData(), Preference.getId(), patientId, Preference.getHospitalId(), orderNumberString, "");

            }

            if (orderRequest.getOrder_data().size() > 0) {

                getSupportLoaderManager().initLoader(7, null, responseUpdateCallback);

            } else {

                progressBar.dismiss();
                Utility.showSnackbar(root, "Please select any product to Generate an order");
            }

        }
    }

    private ArrayList< com.example.moni.imenusoft.serverRequests.Order_data> getOrderData() {
        ArrayList< com.example.moni.imenusoft.serverRequests.Order_data> orderDataList = new ArrayList<>();

        if (menuArrayList != null) {
            if (menuArrayList.size() > 0) {

                for(Day_data day_data : menuArrayList){

                    for(Meal_data meal_data :day_data.getMeal_data()){

                        for(Order_data order_data : meal_data.getOrder_data()){

                            for (Category_data category_data : order_data.getCategory_data()) {

                                 com.example.moni.imenusoft.serverRequests.Order_data orderDataItemObj = new  com.example.moni.imenusoft.serverRequests.Order_data();

                                orderDataItemObj.setWeek_id(category_data.getMenu_data().getWeek_id());
                                orderDataItemObj.setDay_id(category_data.getMenu_data().getDay_id());
                                orderDataItemObj.setMeal_id(category_data.getMenu_data().getMeal_id());

                                orderDataItemObj.setCategory_id(category_data.getMenu_data().getCategory_id());
                                orderDataItemObj.setMenu_id(category_data.getMenu_data().getId());
                                orderDataItemObj.setQty("1");
                                orderDataItemObj.setStatus(category_data.getStatus());
                                orderDataList.add(orderDataItemObj);

                            }

                        }
                    }
                }

            }
        }

        return orderDataList;
    }


    private LoaderManager.LoaderCallbacks<UpdateOrderResponse> responseUpdateCallback = new LoaderManager.LoaderCallbacks<UpdateOrderResponse>() {
        @Override
        public Loader<UpdateOrderResponse> onCreateLoader(int id, Bundle args) {
            return new UpdateOrderDataLoader(OrderDetailActivity.this, orderRequest);
        }

        @Override
        public void onLoadFinished(Loader<UpdateOrderResponse> loader, UpdateOrderResponse data) {

            progressBar.dismiss();


            if (data != null) {
                if (data.getSuccess()) {

                    isDataChanged = true;

                    Utility.showHelpMessages("Order Updated", "Your Order " + orderNumberString + " is updated", OrderDetailActivity.this, new OnDialogClick() {
                        @Override
                        public void onDialogOkClick() {

                            setBackData();
                            //   Utility.traverseToNewOrder(OrderList.this);
                        }

                        @Override
                        public void onDialogCancleClick() {

                        }
                    });


                } else {

                }

            } else {
                Utility.showSnackbar(root, "There might be some technical problem. Please have patience and try Again !! ");
            }

            getSupportLoaderManager().destroyLoader(7);
        }

        @Override
        public void onLoaderReset(Loader<UpdateOrderResponse> loader) {

        }
    };

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MyAction");

        notificationBroadCastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                //extract our message from intent
                String msg_for_me = intent.getStringExtra("notification");
                //log our message value

                NotificationResponse notificationResponse = new Gson().fromJson(msg_for_me,NotificationResponse.class);

                Log.d("Notification",msg_for_me);


         //       notificationResponse.getOrderData().getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getCategory_data().get(0).

            }
        };
        //registering our receiver
        this.registerReceiver(notificationBroadCastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        this.unregisterReceiver(notificationBroadCastReceiver);
    }


    private boolean isOrderCompletable(){

     /*   int flag = 0;
        for(Day_data category_data :menuArrayList){

            if(category_data.getStatus().equals("0")){
                flag = 1;
                break;
            }
        }
*/
        return true;
    }

    private void changeSubMenu(int pos){

        if(pos!=0){

            for(Day_data category_data :menuArrayList){


                if(category_data.getStatus().equalsIgnoreCase("0"))
                   category_data.setStatus(""+pos);
            }

            if(adapter!=null){
                adapter.notifyDataSetChanged();
            }
        }

    }


    @Override
    public void onBackPressed() {

        setBackData();
    }

     private void setBackData(){

        Intent resultIntent = new Intent();
// TODO Add extras or a data URI to this intent as appropriate.
        resultIntent.putExtra("data", isDataChanged);
        setResult(RESULT_OK, resultIntent);
        finish();
    }
}
