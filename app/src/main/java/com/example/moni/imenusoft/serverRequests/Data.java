package com.example.moni.imenusoft.serverRequests;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */


public class Data{
    
  @SerializedName("date")
  @Expose
  private String date;
  @SerializedName("order_date")
  @Expose
  private String order_date;
  @SerializedName("order_status")
  @Expose
  private String order_status;
  @SerializedName("user_id")
  @Expose
  private String user_id;
  @SerializedName("patient_id")
  @Expose
  private String patient_id;
  @SerializedName("update_at")
  @Expose
  private String update_at;
  @SerializedName("created_at")
  @Expose
  private String created_at;
  @SerializedName("order_num")
  @Expose
  private String order_num;
  @SerializedName("hospital_id")
  @Expose
  private String hospital_id;
  
  public Data(){
  }
  
  public Data(String date, String order_date, String order_status, String user_id, String patient_id, String update_at, String created_at, String order_num, String hospital_id){
   this.date=date;
   this.order_date=order_date;
   this.order_status=order_status;
   this.user_id=user_id;
   this.patient_id=patient_id;
   this.update_at=update_at;
   this.created_at=created_at;
   this.order_num=order_num;
   this.hospital_id=hospital_id;
  }
  public void setDate(String date){
   this.date=date;
  }
  public String getDate(){
   return date;
  }
  public void setOrder_date(String order_date){
   this.order_date=order_date;
  }
  public String getOrder_date(){
   return order_date;
  }
  public void setOrder_status(String order_status){
   this.order_status=order_status;
  }
  public String getOrder_status(){
   return order_status;
  }
  public void setUser_id(String user_id){
   this.user_id=user_id;
  }
  public String getUser_id(){
   return user_id;
  }
  public void setPatient_id(String patient_id){
   this.patient_id=patient_id;
  }
  public String getPatient_id(){
   return patient_id;
  }
  public void setUpdate_at(String update_at){
   this.update_at=update_at;
  }
  public String getUpdate_at(){
   return update_at;
  }
  public void setCreated_at(String created_at){
   this.created_at=created_at;
  }
  public String getCreated_at(){
   return created_at;
  }
  public void setOrder_num(String order_num){
   this.order_num=order_num;
  }
  public String getOrder_num(){
   return order_num;
  }
  public void setHospital_id(String hospital_id){
   this.hospital_id=hospital_id;
  }
  public String getHospital_id(){
   return hospital_id;
  }
}