package com.example.moni.imenusoft;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.firebasenotification.notificationJson.NotificationResponse;
import  com.example.moni.imenusoft.serverRequests.MenuResponse;
import  com.example.moni.imenusoft.serverRequests.MenuResponseCategory;
import  com.example.moni.imenusoft.serverRequests.MenuResponseData;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataDay;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataMeal;
import com.example.moni.imenusoft.utils.Utility;
import com.google.gson.Gson;

/**
 * Created by enuke on 30/9/17.
 */

public class Menu extends AppCompatActivity implements View.OnClickListener {

    private TextView week1;
    private TextView week2;
    private TextView week3;
    private TextView week4;

    MenuResponse menuObject;

    int activeid = -1;
    private LinearLayout root;
    private BroadcastReceiver notificationBroadCastReceiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_week);
        initView();

        menuObject = (MenuResponse) getIntent().getSerializableExtra("data");
    }

    private void initView() {


        week1 = findViewById(R.id.week1);
        week2 = findViewById(R.id.week2);
        week3 = findViewById(R.id.week3);
        week4 = findViewById(R.id.week4);

        week1.setOnClickListener(this);
        week2.setOnClickListener(this);
        week3.setOnClickListener(this);
        week4.setOnClickListener(this);
        root = findViewById(R.id.root);

        GradientDrawable drawable = (GradientDrawable) week1.getBackground();
        drawable.setColor(Color.WHITE);

        GradientDrawable drawable1 = (GradientDrawable) week2.getBackground();
        drawable1.setColor(Color.WHITE);

        GradientDrawable drawable2 = (GradientDrawable) week3.getBackground();
        drawable2.setColor(Color.WHITE);

        GradientDrawable drawable3 = (GradientDrawable) week4.getBackground();
        drawable3.setColor(Color.WHITE);
    }

    @Override
    public void onClick(View view) {

        int id = view.getId();

        switch (id) {

            case R.id.week1:

                activeid = 0;
                traverseToPatient();
                break;

            case R.id.week2:

                activeid = 1;
                traverseToPatient();
                break;

            case R.id.week3:

                activeid = 2;
                traverseToPatient();
                break;

            case R.id.week4:

                activeid = 3;
                traverseToPatient();
                break;
        }
    }

    private void traverseToPatient() {

        if(menuObject!=null)
        for(MenuResponseData weekType : menuObject.getData()){

            if(weekType.getWeek_id().equalsIgnoreCase(String.valueOf(activeid))){

                Intent intent =  new Intent(Menu.this, PatientDetail.class);
                intent.putExtra("data",weekType);
                startActivity(intent);
            }
            else{
                Utility.showSnackbar(root,"Data Not avaliable");
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MyAction");

        notificationBroadCastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                //extract our message from intent
                String msg_for_me = intent.getStringExtra("notification");
                //log our message value

                NotificationResponse notificationResponse = new Gson().fromJson(msg_for_me,NotificationResponse.class);






                if(notificationResponse.getChangedata()!=null){
                    if(notificationResponse.getChangedata().size()>0){

                        String position = "";
                        for( MenuResponseData menuResponseDataObj :menuObject.getData()){

                            for(MenuResponseDataDay menuResponseDataDay :menuResponseDataObj.getWeek_data()){

                                for(MenuResponseDataMeal menuResponseDataMeal :menuResponseDataDay.getDay_data()){

                                    for(MenuResponseCategory menuResponseCategory :menuResponseDataMeal.getMeal_data()){

                                        if(menuResponseCategory.getCategory_id().equalsIgnoreCase(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getCategory_id())){
                                            menuResponseCategory.getCategory_data().clear();
                                            menuResponseCategory.getCategory_data().addAll(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getCategory_data());
                                            position = menuResponseCategory.getCategory_id();
                                        }
                                    }
                                }
                            }
                        }

                        if(position.equalsIgnoreCase("")){

                            for( MenuResponseData menuResponseDataObj :menuObject.getData()){

                                for(MenuResponseDataDay menuResponseDataDay :menuResponseDataObj.getWeek_data()){

                                    for(MenuResponseDataMeal menuResponseDataMeal :menuResponseDataDay.getDay_data()){

                                        if(menuResponseDataMeal.getMeal_id().equalsIgnoreCase(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_id())){
                                            menuResponseDataMeal.getMeal_data().addAll(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data());
                                        }
                                    }
                                }
                            }

                        }

                        Utility.showSnackbar(root,position);

                    }
                } else {
                    if(notificationResponse.getId()!=null){

                        for( MenuResponseData menuResponseDataObj :menuObject.getData()){

                            for(MenuResponseDataDay menuResponseDataDay :menuResponseDataObj.getWeek_data()){

                                for(MenuResponseDataMeal menuResponseDataMeal :menuResponseDataDay.getDay_data()){

                                    menuResponseDataMeal.setMeal_data(Utility.removeDataFromArrayList(menuResponseDataMeal.getMeal_data(),notificationResponse.getId()));

                                }
                            }
                        }
                    }
                }

            }
        };
        //registering our receiver
        this.registerReceiver(notificationBroadCastReceiver, intentFilter);
    }

    @Override
    protected void onPause() {
        super.onPause();

        this.unregisterReceiver(notificationBroadCastReceiver);
    }
}
