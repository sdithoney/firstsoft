package com.example.moni.imenusoft.serverRequests;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class OrderRequest{
  @SerializedName("order_date")
  @Expose
  private String order_date;
  @SerializedName("order_data")
  @Expose
  private List<Order_data> order_data;
  @SerializedName("user_id")
  @Expose
  private String user_id;
  @SerializedName("patient")
  @Expose
  private Patient patient;
  @SerializedName("patient_id")
  @Expose
  private String patient_id;
  @SerializedName("hospital_id")
  @Expose
  private String hospital_id;

  @SerializedName("order_num")
  @Expose
  private String order_num;

    @SerializedName("order_status")
    @Expose
    private String order_status;


  public OrderRequest(String order_date, List<Order_data> order_data, String user_id, Patient patient, String hospital_id) {
        this.order_date = order_date;
        this.order_data = order_data;
        this.user_id = user_id;
        this.patient = patient;
        this.hospital_id = hospital_id;
   }

    public OrderRequest(String order_date, List<Order_data> order_data, String user_id, Patient patient, String patient_id, String hospital_id, String order_num) {
        this.order_date = order_date;
        this.order_data = order_data;
        this.user_id = user_id;
        this.patient = patient;
        this.patient_id = patient_id;
        this.hospital_id = hospital_id;
        this.order_num = order_num;
    }

    public OrderRequest(String order_date, List<Order_data> order_data, String user_id, String patient_id, String hospital_id, String order_num, String order_status) {
        this.order_date = order_date;
        this.order_data = order_data;
        this.user_id = user_id;
        this.patient_id = patient_id;
        this.hospital_id = hospital_id;
        this.order_num = order_num;
        this.order_status = order_status;
    }

    public OrderRequest() {
    }


    public void setOrder_date(String order_date){
   this.order_date=order_date;
  }
  public String getOrder_date(){
   return order_date;
  }
  public void setOrder_data(List<Order_data> order_data){
   this.order_data=order_data;
  }
  public List<Order_data> getOrder_data(){
   return order_data;
  }
  public void setUser_id(String user_id){
   this.user_id=user_id;
  }
  public String getUser_id(){
   return user_id;
  }
  public void setPatient(Patient patient){
   this.patient=patient;
  }
  public Patient getPatient(){
   return patient;
  }
  public void setPatient_id(String patient_id){
   this.patient_id=patient_id;
  }
  public String getPatient_id(){
   return patient_id;
  }
  public void setHospital_id(String hospital_id){
   this.hospital_id=hospital_id;
  }
  public String getHospital_id(){
   return hospital_id;
  }

    public String getOrder_num() {
        return order_num;
    }

    public void setOrder_num(String order_num) {
        this.order_num = order_num;
    }

    public String getOrder_status() {
        return order_status;
    }

    public void setOrder_status(String order_status) {
        this.order_status = order_status;
    }
}