package com.example.moni.imenusoft.serverRequests.orderServerRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Awesome Pojo Generator
 * */
public class Category_data implements Serializable {
    
  @SerializedName("menu_data")
  @Expose
  private Menu_data menu_data;
  @SerializedName("menu_name")
  @Expose
  private String menu_name;
  @SerializedName("menu_id")
  @Expose
  private String menu_id;

    @SerializedName("status")
    @Expose
    private String status;


    private String day;
    private String category;
    private String meal;

  public Category_data(){
  }


    public Category_data(Menu_data menu_data, String menu_name, String menu_id, String status) {
        this.menu_data = menu_data;
        this.menu_name = menu_name;
        this.menu_id = menu_id;
        this.status = status;
    }

    public void setMenu_data(Menu_data menu_data){
   this.menu_data=menu_data;
  }
  public Menu_data getMenu_data(){
   return menu_data;
  }
  public void setMenu_name(String menu_name){
   this.menu_name=menu_name;
  }
  public String getMenu_name(){
   return menu_name;
  }
  public void setMenu_id(String menu_id){
   this.menu_id=menu_id;
  }
  public String getMenu_id(){
   return menu_id;
  }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getMeal() {
        return meal;
    }

    public void setMeal(String meal) {
        this.meal = meal;
    }
}