package com.example.moni.imenusoft.serverRequests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by enuke on 10/10/17.
 */


@JsonIgnoreProperties(ignoreUnknown = true)

public class MenuResponseCategory implements Serializable {

    /**
     *
     *     {
     "category_id":"1",
     "category_value":"Hot coffy",
     "category_data":[]
     }
     */

    @SerializedName("category_id")
    @Expose
    private String category_id;

    @SerializedName("category_value")
    @Expose
    private String category_value;

    @SerializedName("category_data")
    @Expose
    private ArrayList<MenuResponseDataMenu> category_data;

    public MenuResponseCategory(String category_id, String category_value, ArrayList<MenuResponseDataMenu> category_data) {
        this.category_id = category_id;
        this.category_value = category_value;
        this.category_data = category_data;
    }

    public MenuResponseCategory() {
    }

    public String getCategory_id() {
        return category_id;
    }

    public void setCategory_id(String category_id) {
        this.category_id = category_id;
    }

    public String getCategory_value() {
        return category_value;
    }

    public void setCategory_value(String category_value) {
        this.category_value = category_value;
    }

    public ArrayList<MenuResponseDataMenu> getCategory_data() {
        return category_data;
    }

    public void setCategory_data(ArrayList<MenuResponseDataMenu> category_data) {
        this.category_data = category_data;
    }
}
