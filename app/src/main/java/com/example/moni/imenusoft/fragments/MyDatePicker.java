package com.example.moni.imenusoft.fragments;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

/**
 * Created by enuke on 11/10/17.
 */

public  class MyDatePicker extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    public onCalanderClick listener;


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dialog = new DatePickerDialog(getActivity(), this, year, month, day);
        dialog.getDatePicker().setMinDate(c.getTimeInMillis());
        c.add(Calendar.DAY_OF_MONTH,6);
        dialog.getDatePicker().setMaxDate(c.getTimeInMillis());
        return  dialog;
    }

 /*   public void onDateSet(DatePicker view, int year, int month, int day) {
        // btnDate.setText(ConverterDate.ConvertDate(year, month + 1, day));




    }
*/
    @Override
    public void onDateSet(android.widget.DatePicker datePicker, int year, int month, int day) {

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR,year);
        cal.set(Calendar.MONTH,month);
        cal.set(Calendar.DAY_OF_MONTH,day);

        if(listener!=null){
            listener.onClick(cal);
        }
    }
}