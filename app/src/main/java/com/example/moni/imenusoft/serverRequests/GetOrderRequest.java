package com.example.moni.imenusoft.serverRequests;

import com.google.gson.annotations.Expose;

/**
 * Created by enuke on 20/10/17.
 */

public class GetOrderRequest {

    @Expose
    String user_id;

    @Expose
    String hospital_id;

    @Expose
    String list_type;

    public GetOrderRequest(String user_id, String hospital_id, String list_type) {
        this.user_id = user_id;
        this.hospital_id = hospital_id;
        this.list_type = list_type;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getHospital_id() {
        return hospital_id;
    }

    public void setHospital_id(String hospital_id) {
        this.hospital_id = hospital_id;
    }

    public String getList_type() {
        return list_type;
    }

    public void setList_type(String list_type) {
        this.list_type = list_type;
    }
}
