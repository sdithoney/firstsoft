package com.example.moni.imenusoft.utils;

import android.text.TextUtils;

/**
 * Created by enuke on 29/9/17.
 */

public class ValidationManager {

    public boolean isEmpty(String value){

       return value.length()==0;
    }

    public  boolean isValidMail(String email){

        return isValidEmail(email);
    }

    public boolean isValidPhoneNumber(String phoneNumber){

        return  (phoneNumber.length()>=10);
    }

    public boolean isValidPassword(String password){

        return  (password.length()>=4);
    }

    public boolean isNull(Object obj){

           return obj==null;
    }

    private  boolean isValidEmail(CharSequence target) {

        return !TextUtils.isEmpty(target) && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }

}
