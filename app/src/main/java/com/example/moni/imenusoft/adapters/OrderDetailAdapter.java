package com.example.moni.imenusoft.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataMenu;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Category_data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by enuke on 12/10/17.
 */

public class OrderDetailAdapter extends RecyclerView.Adapter<OrderDetailAdapter.ViewHolder> {


    Context context;
    ArrayList<Category_data> facilityList;
    private Map<String ,String > orderStatus;


    public OrderDetailAdapter(Context context, ArrayList<Category_data> facilityList) {
        this.context = context;
        this.facilityList = facilityList;
        orderStatus = new HashMap<>();

        orderStatus.put("0","Created");
        orderStatus.put("1","Completed");
        orderStatus.put("2","Canceled");

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_order_detail_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Category_data category_data = facilityList.get(holder.getPosition());

        holder.mMenuValue.setText(category_data.getMenu_name());
        holder.mMenuCategory.setText("Category : "+category_data.getCategory());
        holder.mCurrentStatus.setText("Current Status : "+orderStatus.get(category_data.getStatus()));
        holder.mMenuDay.setText("Order By :"+category_data.getDay());
        holder.mMealType.setText("For Meal : "+category_data.getMeal());

    }


    @Override
    public int getItemCount() {
        return facilityList.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mMenuValue;
        private TextView mMenuCategory;
        private TextView mMenuDay;
        private TextView mCurrentStatus;
        private TextView mMealType;


        public ViewHolder(View convertView) {
            super(convertView);

            mMenuValue = convertView.findViewById(R.id.menuValue);
            mMenuCategory = convertView.findViewById(R.id.menuCategory);
            mMenuDay = convertView.findViewById(R.id.menuDay);
            mCurrentStatus = convertView.findViewById(R.id.currentStatus);
            mMealType = convertView.findViewById(R.id.mealType);
        }
    }

    public Map<String, String> getOrderStatus() {
        return orderStatus;
    }
}
