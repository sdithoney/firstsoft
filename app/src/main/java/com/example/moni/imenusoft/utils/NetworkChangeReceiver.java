package com.example.moni.imenusoft.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by enuke on 29/9/17.
 */

public class NetworkChangeReceiver  extends BroadcastReceiver {

    private final EventBus eventBus = EventBus.getDefault();

    @Override
    public void onReceive(final Context context, Intent intent) {
        Log.d("Change in network", "true");
        if(isConnected(context))
            EventBus.getDefault().post(new NetworkStateChanged(true));
        else EventBus.getDefault().post(new NetworkStateChanged(false));
    }
    public boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);

        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = activeNetwork != null &&
                activeNetwork.isConnected();
        return isConnected;
    }
}