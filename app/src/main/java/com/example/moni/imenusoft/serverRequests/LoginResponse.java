package com.example.moni.imenusoft.serverRequests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by enuke on 1/10/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponse implements Serializable {

    @SerializedName("success")
    @Expose
    private boolean success;

    @SerializedName("data")
    @Expose
    private LoginResponseData data;

    @SerializedName("massage")
    @Expose
    private String massage;

    public LoginResponse() {
    }

    public LoginResponse(boolean success, LoginResponseData data, String massage) {
        this.success = success;
        this.data = data;
        this.massage = massage;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public LoginResponseData getData() {
        return data;
    }

    public void setData(LoginResponseData data) {
        this.data = data;
    }

    public String getMassage() {
        return massage;
    }

    public void setMassage(String massage) {
        this.massage = massage;
    }
}
