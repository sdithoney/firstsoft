package com.example.moni.imenusoft.adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Category_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Day_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Meal_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Order_data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by enuke on 1/11/17.
 */

public class MealAdapter extends RecyclerView.Adapter<MealAdapter.ViewHolder> {


    Context context;
    ArrayList<Day_data> facilityList;
    private Map<String ,String > orderStatus;


    public MealAdapter(Context context, ArrayList<Day_data> facilityList) {
        this.context = context;
        this.facilityList = facilityList;
        orderStatus = new HashMap<>();

        orderStatus.put("0","Created");
        orderStatus.put("1","Completed");
        orderStatus.put("2","Canceled");

    }


    @Override
    public MealAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.adapter_order_detail_meal, parent, false);
        return new MealAdapter.ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final MealAdapter.ViewHolder holder, final int position) {

        Day_data day_data = facilityList.get(holder.getPosition());

        holder.mMealHeader.setText(day_data.getMeal_value());

        ArrayList<Category_data> menuData = new ArrayList<>();

        for(Meal_data meal_data :day_data.getMeal_data()){
            for(Order_data order_data :meal_data.getOrder_data()){
                for(Category_data category_data :order_data.getCategory_data()){

                    category_data.setDay(day_data.getDayValue());
                    category_data.setMeal(day_data.getMeal_value());
                    category_data.setCategory(order_data.getCategory_value());
                    menuData.add(category_data);
                }
            }
        }
      OrderDetailAdapter adapter = new OrderDetailAdapter(context,menuData);
      holder.menuList.setLayoutManager(new LinearLayoutManager(context));
      holder.menuList.setAdapter(adapter);

    }


    @Override
    public int getItemCount() {
        return facilityList.size();
    }




    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mMealHeader;
        private RecyclerView menuList;



        public ViewHolder(View convertView) {
            super(convertView);

            mMealHeader = convertView.findViewById(R.id.mealValue);
            menuList = convertView.findViewById(R.id.menuList);

        }
    }

    public Map<String, String> getOrderStatus() {
        return orderStatus;
    }

    public void getOrderStatus(String data){

        for (Day_data day_data : facilityList) {
            day_data.setStatus(data);
            for (Meal_data meal_data : day_data.getMeal_data()) {
                for (Order_data order_data : meal_data.getOrder_data()) {
                    for (Category_data category_data : order_data.getCategory_data()) {
                        if(category_data.getStatus().equalsIgnoreCase("0"))
                            category_data.setStatus(data);
                    }
                }
            }
        }

        notifyDataSetChanged();
    }

    public void getOrderStatus(String data, int i){

            facilityList.get(i).setStatus(data);

            for (Meal_data meal_data : facilityList.get(i).getMeal_data()) {

                for (Order_data order_data : meal_data.getOrder_data()) {
                    for (Category_data category_data : order_data.getCategory_data()) {
                        category_data.setStatus(data);
                    }
                }
            }

        notifyDataSetChanged();
    }
}
