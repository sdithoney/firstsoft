package com.example.moni.imenusoft.retrofit;



import  com.example.moni.imenusoft.data.ApplicationController;
import  com.example.moni.imenusoft.retrofit.service.PrivateDealService;
import  com.example.moni.imenusoft.utils.Constants;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.converter.jackson.JacksonConverterFactory;


public class CommunicationApi {

    private static final String PROTOCOL_CHARSET = "utf-8";
    private static final String PROTOCOL_CONTENT_TYPE =
            String.format("application/json; charset=%s", PROTOCOL_CHARSET);
   // private static final String API_BASE_URL = "https://test.saferpay.com/";
    public static PrivateDealService service;
    public static PrivateDealService service1;
    public static PrivateDealService service2;

    public static PrivateDealService getPrivateDealService() {
        if (service == null) {
            service = CommunicationApi.restAdapterGen(Constants.BASE_URL);
        }
        return service;
    }




    public static PrivateDealService restAdapterGen(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getDefaultOkHttpClient())
                .build();
        PrivateDealService service = retrofit.create(PrivateDealService.class);
        return service;
    }

    public static PrivateDealService restAdapterGenTwo(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getDefaultOkHttpClient())
                .build();
        PrivateDealService service = retrofit.create(PrivateDealService.class);
        return service;
    }

    public static PrivateDealService restAdapterGenThree(String baseUrl) {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getDefaultOkHttpClient())
                .build();
        PrivateDealService service = retrofit.create(PrivateDealService.class);
        return service;
    }

    private static OkHttpClient getDefaultOkHttpClient() {
        HttpLoggingInterceptor httpLoggingInterceptor = new HttpLoggingInterceptor();
        httpLoggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        Interceptor headerInterceptor = new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request()
                        .newBuilder()
                        .addHeader("Accept", "application/json")
                        .addHeader("Content-Type", "application/json")
                        .cacheControl(CacheControl.FORCE_NETWORK)

                        .build();
                return chain.proceed(request);
            }
        };
        OkHttpClient.Builder builder = new OkHttpClient.Builder();
        builder.connectTimeout(1, TimeUnit.MINUTES)
                .writeTimeout(1, TimeUnit.MINUTES)
                .readTimeout(3, TimeUnit.MINUTES);
        builder.interceptors().add(httpLoggingInterceptor);
        builder.interceptors().add(headerInterceptor);
        builder.interceptors().add(new ConnectivityInterceptor(ApplicationController.getInstance()));
        OkHttpClient client = builder.build();
        return client;
    }
}