package com.example.moni.imenusoft.serverRequests;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class UpdateOrderResponse{

  @SerializedName("data")
  @Expose
  private String data;
  @SerializedName("success")
  @Expose
  private Boolean success;
  @SerializedName("massage")
  @Expose
  private String massage;

    public UpdateOrderResponse(String data, Boolean success) {
        this.data = data;
        this.success = success;
    }

    public UpdateOrderResponse(String data, Boolean success, String massage) {
        this.data = data;
        this.success = success;
        this.massage = massage;
    }


  public void setData(String data){
   this.data=data;
  }
  public String getData(){
   return data;
  }

  public void setSuccess(Boolean success){
   this.success=success;
  }
  public Boolean getSuccess(){
   return success;
  }

  public void setMassage(String massage){
   this.massage=massage;
  }
  public String getMassage(){
   return massage;
  }
}