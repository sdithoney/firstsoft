package com.example.moni.imenusoft.serverRequests;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by enuke on 1/10/17.
 */

@JsonIgnoreProperties(ignoreUnknown = true)
public class LoginResponseDataHospital implements Serializable {

    @SerializedName("id")
    @Expose
    private String id;

    @SerializedName("hospital_name")
    @Expose
    private String hospital_name;

    @SerializedName("address")
    @Expose
    private String address;

    @SerializedName("city")
    @Expose
    private String city;

    @SerializedName("post_code")
    @Expose
    private String post_code;

    @SerializedName("logo_image")
    @Expose
    private String logo_image;

    public LoginResponseDataHospital() {
    }

    public LoginResponseDataHospital(String id, String hospital_name, String address, String city, String post_code, String logo_image) {
        this.id = id;
        this.hospital_name = hospital_name;
        this.address = address;
        this.city = city;
        this.post_code = post_code;
        this.logo_image = logo_image;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getHospital_name() {
        return hospital_name;
    }

    public void setHospital_name(String hospital_name) {
        this.hospital_name = hospital_name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPost_code() {
        return post_code;
    }

    public void setPost_code(String post_code) {
        this.post_code = post_code;
    }

    public String getLogo_image() {
        return logo_image;
    }

    public void setLogo_image(String logo_image) {
        this.logo_image = logo_image;
    }
}
