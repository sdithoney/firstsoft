package com.example.moni.imenusoft;

import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.adapters.PatientListAdapter;
import  com.example.moni.imenusoft.pojo.Patient;
import  com.example.moni.imenusoft.retrofit.loader.CancelOrderLoader;
import  com.example.moni.imenusoft.retrofit.loader.GetOrderLoader;
import  com.example.moni.imenusoft.retrofit.loader.UpdateOrderLoader;
import  com.example.moni.imenusoft.serverRequests.CancelOrderResponse;
import  com.example.moni.imenusoft.serverRequests.CancelRequest;
import  com.example.moni.imenusoft.serverRequests.GetOrderRequest;
import  com.example.moni.imenusoft.serverRequests.MenuResponse;
import  com.example.moni.imenusoft.serverRequests.MenuResponseData;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataMenu;
import  com.example.moni.imenusoft.serverRequests.UpdateOrderRequest;
import  com.example.moni.imenusoft.serverRequests.UpdateOrderResponse;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Category_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Day_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Hospital_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Meal_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.OrderDetailResponse;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Order_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Order_details;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Patient_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.User_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Week_data;
import  com.example.moni.imenusoft.utils.Preference;
import  com.example.moni.imenusoft.utils.RecyclerItemClickListener;
import  com.example.moni.imenusoft.utils.Utility;
import com.google.gson.Gson;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by enuke on 12/10/17.
 */

public class PatientList extends AppCompatActivity {


    public ArrayList<Data> orderResponse;
    PatientListAdapter adapter;
    int color;

    ArrayList<User_data> userList = new ArrayList<>();

    String themeColor = "";
    private RelativeLayout root;
    private TextView userName;
    private TextView detail;
    private View viewUpper;
    private LinearLayout headerLayout;
    private View viewLower;
    private RecyclerView patientList;
    private TextView newOrder;
    private LinearLayout optionLayout;
    private TextView PatientName;
    private TextView ammendOrder;
    private TextView updateOrder;
    private ImageView close;
    private MenuResponse menuResponseObj;
    private MenuResponseData menuResponseDataObj;
    private TextView printOrder;

    User_data patientObj;
    Patient mPatientObj;
    CancelRequest cancelRequestObj;
    UpdateOrderRequest updateOrderRequestObj;

    ProgressDialog progressDialog;
    int currentPos = -1;
    String currentStatus="";
    GetOrderRequest getOrderRequestObj;
    private ProgressDialog progressBar;




    private android.support.v4.app.LoaderManager.LoaderCallbacks<CancelOrderResponse> cancelRequestCallback = new android.support.v4.app.LoaderManager.LoaderCallbacks<CancelOrderResponse>() {
        @Override
        public android.support.v4.content.Loader<CancelOrderResponse> onCreateLoader(int id, Bundle args) {
            return new CancelOrderLoader(PatientList.this,cancelRequestObj);
        }

        @Override
        public void onLoadFinished(android.support.v4.content.Loader<CancelOrderResponse> loader, CancelOrderResponse data) {

            progressDialog.dismiss();
            Utility.showLog("Data",""+ new Gson().toJson(data).toString());

            if(data.getSuccess()){
                Utility.showSnackbar(root,data.getData().getData());

                if(optionLayout.getVisibility()== View.VISIBLE){
                    optionLayout.setVisibility(View.GONE);
                }

                removeOrders();
            } else {
                Utility.showSnackbar(root,data.getMassage());
            }

            getSupportLoaderManager().destroyLoader(1001);
        }

        @Override
        public void onLoaderReset(android.support.v4.content.Loader<CancelOrderResponse> loader) {

        }
    };
    private LoaderManager.LoaderCallbacks<UpdateOrderResponse> updateOrderCallback = new LoaderManager.LoaderCallbacks<UpdateOrderResponse>() {
        @Override
        public Loader<UpdateOrderResponse> onCreateLoader(int id, Bundle args) {

            return new UpdateOrderLoader(PatientList.this,updateOrderRequestObj);
        }

        @Override
        public void onLoadFinished(Loader<UpdateOrderResponse> loader, UpdateOrderResponse data) {


            if(data.getSuccess()){
                optionLayout.setVisibility(View.GONE);
                Utility.showSnackbar(root,data.getData());

                removeOrders();

            } else {
                Utility.showSnackbar(root,data.getMassage());
            }

            progressDialog.dismiss();
            getSupportLoaderManager().destroyLoader(234);
        }

        @Override
        public void onLoaderReset(Loader<UpdateOrderResponse> loader) {

        }
    };


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.order_summary);

        progressBar = new ProgressDialog(PatientList.this);

        progressDialog = new ProgressDialog(PatientList.this);

        if (getIntent().hasExtra("data_color"))
            themeColor = getIntent().getStringExtra("data_color");


        if (getIntent().hasExtra("data"))
            orderResponse = (ArrayList<Data>) getIntent().getSerializableExtra("data");

        if (getIntent().hasExtra("data_menu"))
            menuResponseObj = (MenuResponse) getIntent().getSerializableExtra("data_menu");


        initView();
        setRecycler();

    }


    private void setRecycler() {

        patientList.setLayoutManager(new LinearLayoutManager(PatientList.this));



        if(orderResponse.size()>0)
          for(Data dataObj: orderResponse){

            for(Order_details order_details: dataObj.getOrder_details()){

                for(Hospital_data hospital_data : order_details.getHospital_data()){

                    userList.addAll(hospital_data.getUser_data());
                }
            }

        }

        adapter = new PatientListAdapter(PatientList.this, userList);

        patientList.setAdapter(adapter);


        patientList.addOnItemTouchListener(
                new RecyclerItemClickListener(PatientList.this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override
                    public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        showExtraMenu(position);
                        Utility.showLog("Data",""+ new Gson().toJson(userList.get(position)));
                        patientObj = userList.get(position);
                        currentPos = position;
                    }
                })
        );
    }

    private void showExtraMenu(int position) {

        optionLayout.setVisibility(View.VISIBLE);

        if (userList != null) {
            if (userList.size() > 0) {

                PatientName.setText("Edit Order : " + userList.get(position).getFirst_name() + " " + userList.get(position).getLast_name());
            }
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Utility.traverseToNewOrder(PatientList.this);
    }


    private void showOrderStatus() {

        final CharSequence[] items = {
                "Created", "Served" , "Deleted"
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Update your order to");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int item) {
                // Do something with the selection
                //  mDoneButton.setText(items[item]);

                if(Utility.isInternetConnected(PatientList.this)){

                    currentStatus = items[item].toString();

                    if(items[item].toString().equalsIgnoreCase("Created")){

                        Utility.showSnackbar(root,"Order is already created");

                    } else {

                        makeOrderUpdate(items[item].toString());
                    }

                } else{
                    Utility.showSnackbar(root,"No Internet COnnection");

                    optionLayout.setVisibility(View.GONE);
                    currentStatus = "";
                }

            }
        });
        AlertDialog alert = builder.create();
        alert.show();

    }

    private void makeOrderUpdate(String orderStatus) {

        //String order_status,String user_id,String order_num,String hospital_id
        if(Utility.isInternetConnected(PatientList.this)){

            progressDialog.setTitle("Loading");
            progressDialog.setMessage("Updating order status");
            progressDialog.setCancelable(false);
            progressDialog.show();
            updateOrderRequestObj = new UpdateOrderRequest(orderStatus,Preference.getId(),orderResponse.get(currentPos).getOrder_num(),Preference.getHospitalId());

            getSupportLoaderManager().initLoader(234,null,updateOrderCallback);

        } else {

            Utility.showSnackbar(root,"No Internet Connectivity");
        }
    }

    private void initView() {

        root = findViewById(R.id.root);
        userName = findViewById(R.id.user_name);
        detail = findViewById(R.id.detail);
        viewUpper = findViewById(R.id.view_upper);
        headerLayout = findViewById(R.id.header_layout);
        viewLower = findViewById(R.id.view_lower);
        patientList = findViewById(R.id.patientList);
        newOrder = findViewById(R.id.newOrder);
        optionLayout = findViewById(R.id.optionLayout);
        PatientName = findViewById(R.id.PatientName);
        ammendOrder = findViewById(R.id.ammendOrder);
        updateOrder = findViewById(R.id.updateOrder);
        close = findViewById(R.id.close);
        printOrder = findViewById(R.id.printOrder);

        optionLayout.setVisibility(View.GONE);

        bindListeners();
        setValues();
        setThemeColors();

    }

    private void setThemeColors() {

        newOrder.setBackgroundColor(Utility.getColorCode(themeColor));
        ammendOrder.setBackgroundColor(Utility.getColorCode(themeColor));
        updateOrder.setBackgroundColor(Utility.getColorCode(themeColor));
    }

    private void bindListeners(){

        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(optionLayout.getVisibility()== View.VISIBLE){
                    optionLayout.setVisibility(View.GONE);
                }
            }
        });

        newOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Utility.traverseToNewOrder(PatientList.this);
            }
        });

        updateOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
              //  showOrderStatus();

                optionLayout.setVisibility(View.GONE);
                Intent orderIntent = new Intent(PatientList.this,OrderDetailActivity.class);
                orderIntent.putExtra("data",orderResponse.get(currentPos));
                startActivityForResult(orderIntent,2000);

            }
        });

    /*    ammendLunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Call to cancel order
                makeCancelOrder(orderResponse.get(currentPos).getOrder_num());

            }
        });*/

        ammendOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Call to OrderList

                Intent intent = new Intent(PatientList.this, OrderList.class);

                intent.putExtra("data", getMenuResponseDataObj());
                intent.putExtra("user_data", getmPatientObj(patientObj));
                intent.putExtra("selected_menu_array", (Serializable) getMenu());
                intent.putExtra("order_number", orderResponse.get(currentPos).getOrder_num());

                startActivity(intent);
            }
        });

        printOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                optionLayout.setVisibility(View.GONE);
                Intent printIntent = new Intent(PatientList.this,PrintActivity.class);
                printIntent.putExtra("data",orderResponse.get(currentPos));

                startActivity(printIntent);
            }
        });
    }

    private void setValues(){

        userName.setText("User :" + Preference.getUserName());
        detail.setText(Utility.setTimeHeader());

    }

    private Patient getmPatientObj(User_data data){

        Patient patientObj = new Patient();

        patientObj.setFirstname(data.getFirst_name());

        patientObj.setLastname(data.getLast_name());

        patientObj.setBedNo(data.getBed_num());

        patientObj.setWardNo(data.getWard());

        patientObj.setFoodAllergies(data.getAllergies());

        patientObj.setGender(data.getGender());

        // need this from server
        patientObj.setServe(data.getServe());

        // also need this from server
        patientObj.setUser(Preference.getUserName());

        patientObj.setId(data.getPatient_id());

        return patientObj;
    }

    private MenuResponseData getMenuResponseDataObj(){

        MenuResponseData menuResponseData = null;

        for(MenuResponseData mObj : menuResponseObj.getData()) {
            if (patientObj.getPatient_data().get(0).getWeek_id().equalsIgnoreCase(mObj.getWeek_id())){
                menuResponseData = mObj;
            }
        }

        return menuResponseData;
    }

    private void removeOrders(){


        userList.remove(currentPos);
        orderResponse.remove(currentPos);

        if(adapter!=null){

                adapter.notifyDataSetChanged();
        }
    }

    private Map<String, MenuResponseDataMenu> getMenu(){

        Map<String, MenuResponseDataMenu> map = new HashMap<>();

        for(Patient_data patient_data :patientObj.getPatient_data()){

            for(Week_data week_data :patient_data.getWeek_data()){

                for(Day_data day_data :week_data.getDay_data()){

                    for(Meal_data meal_data :day_data.getMeal_data()) {

                        for(Order_data order_data : meal_data.getOrder_data()){

                            for(Category_data category_data : order_data.getCategory_data()){

                             MenuResponseDataMenu menuObj = new MenuResponseDataMenu();

                             menuObj.setId(category_data.getMenu_data().getId());
                             menuObj.setSelected(true);
                             menuObj.setCategory_id(category_data.getMenu_data().getCategory_id());
                             menuObj.setDay_id(category_data.getMenu_data().getDay_id());
                             menuObj.setHospital_id(category_data.getMenu_data().getHospital_id());
                             menuObj.setMeal_id(category_data.getMenu_data().getMeal_id());
                             menuObj.setMenu_name(category_data.getMenu_data().getMenu_name());
                             menuObj.setSymbol_id(category_data.getMenu_data().getSymbol_id());
                             menuObj.setUser_id(category_data.getMenu_data().getUser_id());
                             menuObj.setWeek_id(category_data.getMenu_data().getWeek_id());

                             map.put(menuObj.getId(),menuObj);
                            }
                        }
                    }
                }
            }
        }


        return map;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK){

            if(requestCode ==2000){


                if(data!=null){

                    if(data.hasExtra("data")){

                        boolean isOrderChanged = data.getBooleanExtra("data",false);

                        if(isOrderChanged){

                            if (Utility.isInternetConnected(PatientList.this)) {
                                progressBar.setCancelable(false);
                                progressBar.setTitle("Loading");
                                progressBar.setMessage("Please wait...");
                                progressBar.show();
                                getOrderDetail();
                            } else {
                                Utility.showToast(PatientList.this, "No internet Connectivity");
                            }

                        }
                    }
                }



            }

        }
    }

    private void getOrderDetail() {

        getOrderRequestObj = new GetOrderRequest(Preference.getId(), Preference.getHospitalId(), "hospital");

        getSupportLoaderManager().initLoader(1, null, responseGetOrderCallback);
    }

    private LoaderManager.LoaderCallbacks<OrderDetailResponse> responseGetOrderCallback = new LoaderManager.LoaderCallbacks<OrderDetailResponse>() {
        @Override
        public Loader<OrderDetailResponse> onCreateLoader(int id, Bundle args) {

            return new GetOrderLoader(PatientList.this,getOrderRequestObj);
        }

        @Override
        public void onLoadFinished(Loader<OrderDetailResponse> loader, OrderDetailResponse data) {

            progressBar.dismiss();
            getSupportLoaderManager().destroyLoader(1);

            if(data!=null) {
                if (data.getSuccess()) {

                    if (data.getData()!=null){

                        orderResponse.clear();
                        orderResponse = (ArrayList<Data>) data.getData();

                        userList.clear();
                        if(orderResponse.size()>0)
                            for(Data dataObj: orderResponse){

                                for(Order_details order_details: dataObj.getOrder_details()){

                                    for(Hospital_data hospital_data : order_details.getHospital_data()){

                                        userList.addAll(hospital_data.getUser_data());
                                    }
                                }

                            }

                            if(adapter!=null){
                                adapter.notifyDataSetChanged();
                            }

                    } else {
                        Utility.showSnackbar(root,"No Order Found. Create some order(s)");
                    }
                } else {
                    Utility.showSnackbar(root, "Ops !! something went wrong, We are trying to fix");
                }
            } else{
                Utility.showSnackbar(root, "Some thing went wrong. Try again!!");

            }


        }

        @Override
        public void onLoaderReset(Loader<OrderDetailResponse> loader) {

        }
    };

}
