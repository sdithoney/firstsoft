package com.example.moni.imenusoft.firebasenotification.notificationJson;
import com.example.moni.imenusoft.serverRequests.MenuResponseDataMenu;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class Meal_data{
  @SerializedName("category_data")
  @Expose
  private List<MenuResponseDataMenu> category_data;
  @SerializedName("category_id")
  @Expose
  private String category_id;
  @SerializedName("category_value")
  @Expose
  private String category_value;
  public void setCategory_data(List<MenuResponseDataMenu> category_data){
   this.category_data=category_data;
  }
  public List<MenuResponseDataMenu> getCategory_data(){
   return category_data;
  }
  public void setCategory_id(String category_id){
   this.category_id=category_id;
  }
  public String getCategory_id(){
   return category_id;
  }
  public void setCategory_value(String category_value){
   this.category_value=category_value;
  }
  public Object getCategory_value(){
   return category_value;
  }
}