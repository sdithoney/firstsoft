package com.example.moni.imenusoft.serverRequests.orderServerRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class Order_data implements Serializable {
  @SerializedName("category_data")
  @Expose
  private List<Category_data> category_data;
  @SerializedName("category_id")
  @Expose
  private String category_id;
  @SerializedName("category_value")
  @Expose
  private String category_value;
  public Order_data(){
  }
  public Order_data(List<Category_data> category_data, String category_id, String category_value){
   this.category_data=category_data;
   this.category_id=category_id;
   this.category_value=category_value;
  }
  public void setCategory_data(List<Category_data> category_data){
   this.category_data=category_data;
  }
  public List<Category_data> getCategory_data(){
   return category_data;
  }
  public void setCategory_id(String category_id){
   this.category_id=category_id;
  }
  public String getCategory_id(){
   return category_id;
  }
  public void setCategory_value(String category_value){
   this.category_value=category_value;
  }
  public String getCategory_value(){
   return category_value;
  }
}