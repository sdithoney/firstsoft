package com.example.moni.imenusoft.serverRequests.orderServerRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class Hospital_data implements Serializable {
  @SerializedName("user_id")
  @Expose
  private String user_id;
  @SerializedName("User_data")
  @Expose
  private List<User_data> User_data;
  @SerializedName("email")
  @Expose
  private String email;
  public Hospital_data(){
  }
  public Hospital_data(String user_id, List<User_data> User_data, String email){
   this.user_id=user_id;
   this.User_data=User_data;
   this.email=email;
  }
  public void setUser_id(String user_id){
   this.user_id=user_id;
  }
  public String getUser_id(){
   return user_id;
  }
  public void setUser_data(List<User_data> User_data){
   this.User_data=User_data;
  }
  public List<User_data> getUser_data(){
   return User_data;
  }
  public void setEmail(String email){
   this.email=email;
  }
  public String getEmail(){
   return email;
  }
}