package com.example.moni.imenusoft.firebasenotification;

import android.content.Context;
import android.util.Log;


import org.json.JSONObject;


/**
 * Created by enuke on 10/27/16.
 */
public class NotificationServiceController {


    NotificationServiceManager notificationServiceManagerObj;
    Context con;


    public static final String CATEGORY_BROADCAST_ACTION = "com.enuke.groceryapp.firebasenotification.CATEGORY";

    public NotificationServiceController(Context con) {
        this.con = con;
        notificationServiceManagerObj = new NotificationServiceManager(this.con);
    }

    public void notificationController(String store_ID, String type_ID, String type, String action, JSONObject notificationJson) {
        Log.d("true","true 0 "+ notificationJson.toString());

        // Case 1 . Store edit
       /* if (action.equalsIgnoreCase("edit") || action.equalsIgnoreCase("add")) {
            try {
                notificationServiceManagerObj.updateStoreService(store_ID, type_ID, type, notificationJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }*/

      /*  if (type.equalsIgnoreCase("image") && action.equalsIgnoreCase("delete")) {
            //String variantId = VariantImageTable.getInstance().variantIdFetch(id);
            VariantImageTable.getInstance().deleteImage(type_ID);
            onMessageReceived(type, notificationJson);

        }

        if (type.equalsIgnoreCase("variant") && action.equalsIgnoreCase("delete")) {
            CategoryPageVariantTable.getInstance().deleteVariant(type_ID);

            updateCartVarientQuantity(type_ID,"0","delete");

            onMessageReceived(type, notificationJson);

        }


        if (type.equalsIgnoreCase("product") && action.equalsIgnoreCase("delete")) {
            ArrayList<String> gettingProductIds = CategoryPageVariantTable.getInstance().gettingVariantIds(type_ID);
            CategoryPageVariantTable.getInstance().deleteProduct(type_ID);

            //Log.drintln("before"+preferencedMapData.toString());

                if (gettingProductIds.size() > 0) {
                    for (String variantId : gettingProductIds) {

                            //Log.drintln("after" + preferencedMapData.toString());
                        updateCartVarientQuantity(type_ID,"0","delete");
                        }

                }

            onMessageReceived(type, notificationJson);

        }


        if (type.equalsIgnoreCase("category") && action.equalsIgnoreCase("delete")) {
            ArrayList<String> gettingProductIds = CategoryPageVariantTable.getInstance().gettingVariantIdsBasedOnCategory(type_ID);
            CategoryPageVariantTable.getInstance().deleteCategory(type_ID);
            CategorychildTable.getInstance().deleteCategory(type_ID);


            //Log.drintln("before"+preferencedMapData.toString());

                if (gettingProductIds.size() > 0) {
                    for (String variantId : gettingProductIds) {

                            updateCartVarientQuantity(variantId,"0","delete");
                            //Log.drintln("after" + preferencedMapData.toString());
                    }
                }

            Intent intent = new Intent(CATEGORY_BROADCAST_ACTION);
            intent.putExtra("delete_cat","map updated");
            con.sendBroadcast(intent);
           // onMessageReceived(type, notificationJson);

        }


        if(type.equalsIgnoreCase("user")&&action.equalsIgnoreCase("inactive"))
        {
            if(notificationServiceManagerObj.isAppIsInForground()) {
                Utility.deActivateUser(con);
            } else{
                Preference.clearPreferencesLogin();
                Preference.setCartMapJson("");
                Preference.clearPreferences();
            }
        }



        if (type.equalsIgnoreCase("parent category") && action.equalsIgnoreCase("delete")) {
            CategoryParentTable.getInstance().deleteParentCategory(type_ID);
            CategorychildTable.getInstance().deleteParentCategoryChild(type_ID);

            onMessageReceived(type, notificationJson);

        }

        if (type.equalsIgnoreCase("time") && action.equalsIgnoreCase("delete")) {
            Grocery_TimesTable.getInstance().deleteTimeRow(type_ID);

            onMessageReceived(type, notificationJson);

        }

        if (type.equalsIgnoreCase("user") && action.equalsIgnoreCase("delete")) {
            Grocery_StoreOwnerTable.getInstance().deleteOwner(type_ID);

            onMessageReceived(type, notificationJson);

        }

        if (type.equalsIgnoreCase("order") && action.equalsIgnoreCase("order_status_change")) {

            try {
                if(notificationJson.has("ordercount"))
                Preference.setOrderCount(notificationJson.getInt("ordercount"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            onMessageReceived(type, notificationJson);

        }

        if(type.equalsIgnoreCase("veriant_qty")&& action.equalsIgnoreCase("veriant_qty_change"))
        {
            try {
                JSONArray data = notificationJson.getJSONArray("data");
                String id = "";
                String itemQuantity = "";

                for(int i = 0;i<data.length();i++)
                {
                    id = ""+data.getJSONObject(i).getString("veriant_id");
                    itemQuantity =""+data.getJSONObject(i).getString("veriant_qty");

                    CategoryPageVariantTable.getInstance().updateItemInStock(itemQuantity,id);

                    if(notificationJson.getString("on_order").equals("cancelled")||!notificationJson.getString("user_id").equalsIgnoreCase(Preference.getUserIdCommon()) ) {
                        updateCartVarientQuantity(id, itemQuantity,"veriant_qty_change");
                    }

                    onMessageReceived(type,notificationJson);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void updateCartVarientQuantity(String varientID, String itemInStock, String action) {
        Map<String, ItemInfoBean> cartMap = Utility.cartJsonToHashmap(Preference.getCartMapJson());
        if (cartMap != null) {
            if (cartMap.size() > 0) {

                if (cartMap.containsKey(varientID)) {

                    ItemInfoBean itemInfoBeanObj = cartMap.get(varientID);
                    int quantity = itemInfoBeanObj.getQuantity();

                    if (quantity > Integer.parseInt(itemInStock)) {
                        itemInfoBeanObj.setQuantity(Integer.parseInt(itemInStock));
                        cartMap.put(varientID, itemInfoBeanObj);
                        Preference.setCartMapJson(Utility.hashMapToCartJson(cartMap));

                        if(action.equals("delete")) {

                            cartMap.remove(varientID);

                        }
                    }


                }
            }

        }
    }


    public void onMessageReceived(final String type, final JSONObject notificationJson) {
      *//*  int count = Preference.getIsRead();
        //Log.d("Shared Preference value", ""+count);
        Preference.setIsRead(++count);*//*
        //Log.d("On Message Receive","true");


        AppController.getInstance().runOnUiThread(new Runnable() {
            @Override
            public void run() {

                for (OnMessageReceived onMessageReceiveListener : AppController
                        .getInstance().getUIListeners(
                                OnMessageReceived.class))
                    onMessageReceiveListener
                            .onMessageReceived(type, notificationJson);
            }
        });
    }
*/
}
}
