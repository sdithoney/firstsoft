package com.example.moni.imenusoft.utils;

import android.content.Context;
import android.content.SharedPreferences;

import  com.example.moni.imenusoft.data.ApplicationController;


/**
 * Created by manoj on 11/30/15.
 */
public class Preference {
    final static String PREFS_NAME = "iMenu";
    final static String PREFS_NAME_NOTIFICATION = "iMenuNotification";




    public static String getDeviceToken()
    {
        SharedPreferences pref = ApplicationController.getInstance().getApplicationContext().getSharedPreferences(PREFS_NAME_NOTIFICATION, Context.MODE_PRIVATE);
        String isVisited = pref.getString(Constants.NOTIFICATION,"");
        return isVisited;
    }

    public static void setDeviceToken(String value)
    {
        SharedPreferences pref = ApplicationController.getInstance().getSharedPreferences(PREFS_NAME_NOTIFICATION, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.NOTIFICATION, value);
        editor.commit();
    }


    /***
     * Save Integer value to SharedPreferences
     *
     * @param key   key of the data
     * @param value Integer value that you want to save
     */
    public static void putInt(String key, int value) {
        final SharedPreferences appData = ApplicationController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.putInt(key, value);
        editor.commit();

    }

    public static String getUserName()
    {
        SharedPreferences pref = ApplicationController.getInstance().getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String isVisited = pref.getString(Constants.USER_NAME,"");
        return isVisited;
    }

    public static void setUserName(String value)
    {
        SharedPreferences pref = ApplicationController.getInstance().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.USER_NAME, value);
        editor.commit();
    }



    public static String getId()
    {
        SharedPreferences pref = ApplicationController.getInstance().getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String isVisited = pref.getString(Constants.ID,"");
        return isVisited;
    }

    public static void setId(String value)
    {
        SharedPreferences pref = ApplicationController.getInstance().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.ID, value);
        editor.commit();
    }

    public static String getHospitalId()
    {
        SharedPreferences pref = ApplicationController.getInstance().getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String isVisited = pref.getString(Constants.HOSPITAL_ID,"");
        return isVisited;
    }

    public static void setHospitalId(String value)
    {
        SharedPreferences pref = ApplicationController.getInstance().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.HOSPITAL_ID, value);
        editor.commit();
    }

    public static String getUserId()
    {
        SharedPreferences pref = ApplicationController.getInstance().getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String isVisited = pref.getString(Constants.USER_ID,"");
        return isVisited;
    }

    public static void setUserId(String value)
    {
        SharedPreferences pref = ApplicationController.getInstance().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.USER_ID, value);
        editor.commit();
    }


    public static boolean  isLogin()
    {
        SharedPreferences pref = ApplicationController.getInstance().getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        boolean isVisited = pref.getBoolean(Constants.IS_LOGIN,false);
        return isVisited;
    }

    public static void setIsLogin(boolean value)
    {
        SharedPreferences pref = ApplicationController.getInstance().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putBoolean(Constants.IS_LOGIN, value);
        editor.commit();
    }

    public static String getUserObjectJson()
    {
        SharedPreferences pref = ApplicationController.getInstance().getApplicationContext().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        String isVisited = pref.getString(Constants.USER_OBJECT_JSON,"");
        return isVisited;
    }

    public static void setUserObjectJson(String value)
    {
        SharedPreferences pref = ApplicationController.getInstance().getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString(Constants.USER_OBJECT_JSON, value);
        editor.commit();
    }

    public static void clearPreferences() {
        final SharedPreferences appData = ApplicationController.getInstance().getSharedPreferences(
                PREFS_NAME, 0);
        SharedPreferences.Editor editor = appData.edit();
        editor.clear();
        editor.commit();

    }

}
