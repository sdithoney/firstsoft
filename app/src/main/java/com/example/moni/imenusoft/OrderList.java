package com.example.moni.imenusoft;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.Loader;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.adapters.MenuCategoryAdapter;
import  com.example.moni.imenusoft.adapters.OnMenuClick;
import  com.example.moni.imenusoft.firebasenotification.notificationJson.NotificationResponse;
import  com.example.moni.imenusoft.fragments.MyDatePicker;
import  com.example.moni.imenusoft.fragments.onCalanderClick;
import  com.example.moni.imenusoft.pojo.Patient;
import  com.example.moni.imenusoft.retrofit.loader.OrderGenerateLoader;
import  com.example.moni.imenusoft.retrofit.loader.UpdateOrderDataLoader;
import  com.example.moni.imenusoft.serverRequests.CreateOrderResponse;
import  com.example.moni.imenusoft.serverRequests.MenuResponseCategory;
import  com.example.moni.imenusoft.serverRequests.MenuResponseData;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataDay;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataMeal;
import  com.example.moni.imenusoft.serverRequests.MenuResponseDataMenu;
import  com.example.moni.imenusoft.serverRequests.OrderRequest;
import  com.example.moni.imenusoft.serverRequests.Order_data;
import  com.example.moni.imenusoft.serverRequests.UpdateOrderResponse;
import  com.example.moni.imenusoft.utils.MyCustomInformationClass;
import  com.example.moni.imenusoft.utils.OnDialogClick;
import  com.example.moni.imenusoft.utils.Preference;
import  com.example.moni.imenusoft.utils.Utility;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by enuke on 7/10/17.
 */

public class OrderList extends AppCompatActivity  {

    private TextView weekHeader;
    private TextView orderDesc;
    private TextView orderDescLine2;
    private TextView patientDesc;
    private RecyclerView orderRecycler;
    private MyCustomInformationClass info;
    private Button breakfast;
    private Button lunch;
    private Button dinner;

    MenuResponseData menuResponseData;
    Patient patientDetail;
    private RelativeLayout root;
    String patientId = "";
    private Button order;
    String mealType = "Breakfast";
    String dateForOrder = Utility.getDayMonthAndDate(Calendar.getInstance());
    ArrayList<MenuResponseCategory> menuResponseCategories = new ArrayList<>();
    OrderRequest orderRequest;

    ProgressDialog progressBar;
    MenuCategoryAdapter adapter;

    Map<String,MenuResponseDataMenu> mapOrder;
    String orderNumber="";

    BroadcastReceiver notificationBroadCastReceiver;



    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_order_list);


        progressBar = new ProgressDialog(OrderList.this);

        if (getIntent().hasExtra("data")) {
            menuResponseData = (MenuResponseData) getIntent().getSerializableExtra("data");
        }

        if (getIntent().hasExtra("user_data")) {
            patientDetail = (Patient) getIntent().getSerializableExtra("user_data");
        }

        if (getIntent().hasExtra("selected_menu_array")) {
            mapOrder = (Map<String, MenuResponseDataMenu>) getIntent().getSerializableExtra("selected_menu_array");
        }

        if (getIntent().hasExtra("order_number")) {
            orderNumber =  getIntent().getStringExtra("order_number");

           dateForOrder = Utility.getDayMonthAndDate(getDateOfDay(getCalanderDay(getDay())));

        }


        initMap();


        initView();
        setRecycler();

        breakfast.setBackgroundColor(Utility.getColorCode(menuResponseData.getWeek_color()));
        lunch.setBackgroundColor(Utility.getColorCode(menuResponseData.getWeek_color()));
        dinner.setBackgroundColor(Utility.getColorCode(menuResponseData.getWeek_color()));
        order.setBackgroundColor(Utility.getColorCode(menuResponseData.getWeek_color()));

        breakfast.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mealType = "Breakfast";

                if(orderNumber.length()==0)
                    dateForOrder = Utility.getDayMonthAndDate(Calendar.getInstance());
                else
                    dateForOrder = Utility.getDayMonthAndDate(getDateOfDay(getCalanderDay(getDayByDayId(getDateByMap("1")))));

                setHeader();
                setMealTypeData();
                setSelector(view.getId());
            }
        });


        lunch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mealType = "Lunch";

                if(orderNumber.length()==0)
                   dateForOrder = Utility.getDayMonthAndDate(Calendar.getInstance());
                else
                    dateForOrder = Utility.getDayMonthAndDate(getDateOfDay(getCalanderDay(getDayByDayId(getDateByMap("2")))));

                setHeader();
                setMealTypeData();
                setSelector(view.getId());


            }
        });

        dinner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                mealType = "Dinner";

                if(orderNumber.length()==0)
                    dateForOrder = Utility.getDayMonthAndDate(Calendar.getInstance());
                else
                    dateForOrder = Utility.getDayMonthAndDate(getDateOfDay(getCalanderDay(getDayByDayId(getDateByMap("3")))));

                setHeader();
                setMealTypeData();
                setSelector(view.getId());

            }
        });


        breakfast.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {

                MyDatePicker newFragment = new MyDatePicker();
                newFragment.show(getSupportFragmentManager(), "datePicker");
                newFragment.listener = new onCalanderClick() {
                    @Override
                    public void onClick(Calendar cal) {

                        dateForOrder = Utility.getDayMonthAndDate(cal);
                        mealType = "BreakFast";
                        setHeader();
                        setMealTypeData();
                        setSelector(view.getId());

                    }
                };


                return false;
            }
        });

        lunch.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {

                MyDatePicker newFragment = new MyDatePicker();
                newFragment.listener = new onCalanderClick() {
                    @Override
                    public void onClick(Calendar cal) {

                        dateForOrder = Utility.getDayMonthAndDate(cal);
                        mealType = "Lunch";
                        setHeader();
                        setMealTypeData();
                        setSelector(view.getId());

                    }
                };
                newFragment.show(getSupportFragmentManager(), "datePicker");


                return false;
            }
        });

        dinner.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(final View view) {

                MyDatePicker newFragment = new MyDatePicker();
                newFragment.listener = new onCalanderClick() {
                    @Override
                    public void onClick(Calendar cal) {

                        dateForOrder = Utility.getDayMonthAndDate(cal);
                        mealType = "Dinner";
                        setHeader();
                        setMealTypeData();
                        setSelector(view.getId());

                    }
                };
                newFragment.show(getSupportFragmentManager(), "datePicker");


                return false;
            }
        });

        breakfast.setTextColor(Color.WHITE);
        lunch.setTextColor(Color.WHITE);
        dinner.setTextColor(Color.WHITE);
        order.setTextColor(Color.WHITE);


        GradientDrawable drawable = (GradientDrawable) weekHeader.getBackground();
        drawable.setColor(Utility.getColorCode(menuResponseData.getWeek_color()));
        weekHeader.setTextColor(Color.WHITE);
        int weekNo = Integer.parseInt(menuResponseData.getWeek_id());
        weekHeader.setText("Week "+ ++weekNo);

        info.setColor(Utility.getColorCode(menuResponseData.getWeek_color()));


        setMealTypeData();

        if(orderNumber.length()>0){
            setSelector(getMealValue());

        } else{
            setSelector(R.id.breakfast);

        }

    }

    private int getMealValue() {
        int id = -1;

        if(mealType.equalsIgnoreCase("Breakfast"))
            id = R.id.breakfast;

        else if(mealType.equalsIgnoreCase("Lunch"))
            id = R.id.lunch;

        else if(mealType.equalsIgnoreCase("Dinner"))
            id = R.id.dinner;

            return id;
    }


    private void initView() {

        weekHeader = findViewById(R.id.week_header);
        orderDesc = findViewById(R.id.order_desc);
        orderDescLine2 = findViewById(R.id.order_desc_line_2);
        patientDesc = findViewById(R.id.patient_desc);
        orderRecycler = findViewById(R.id.order_recycler);
        info = findViewById(R.id.info);
        breakfast = findViewById(R.id.breakfast);
        lunch = findViewById(R.id.lunch);
        dinner = findViewById(R.id.dinner);
        root = findViewById(R.id.root);
        order = findViewById(R.id.order);

        order.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (Utility.isInternetConnected(OrderList.this)) {

                    getOrderDetail();

                } else {
                    Utility.showSnackbar(root, "No Internet Connectivity.");
                }
            }
        });

        if(orderNumber.length()>0){

            order.setText("Update Order");

        } else {
            order.setText("Generate Order");
        }
        setHeader();
    }

    private void setHeader() {

        orderDesc.setText("Order taken on :" + Utility.setTimeHeader());

        String color = Integer.toHexString(Utility.getColorCode(menuResponseData.getWeek_color()));
        color = "#" + color.substring(2);
        //   orderDescLine2.setText(Html.fromHtml("<font color = #"+color+" "+meatType+ " for "+ dateForOrder+"</font>"),TextView.BufferType.SPANNABLE);

        String data = "<font color = " + color + "> <b>" + mealType + " for " + dateForOrder + "</b></font>";
        data += " User : " + Preference.getUserName();
        orderDescLine2.setText(Html.fromHtml(data), TextView.BufferType.SPANNABLE);

        patientDesc.setText(
                "Patient :" + patientDetail.getFirstname() + " " + patientDetail.getLastname() +
                        " BedNo :" + patientDetail.getBedNo() + " Ward :" + patientDetail.getWardNo() + " Additional Notes :" + patientDetail.getFoodAllergies() + ", "
                        + patientDetail.getServe()
        );

    }

    private void setMealTypeData() {

        menuResponseCategories.clear();

        setMapDataToArrayList();

        for (MenuResponseDataDay dayData : menuResponseData.getWeek_data()) {


            if (dayData.getDay_value().equalsIgnoreCase(dateForOrder.split(" ")[0].toString().trim())) {

                for (MenuResponseDataMeal mealObj : dayData.getDay_data()) {

                    if (mealObj.getMeal_value().equalsIgnoreCase(mealType)) {

                        if(mealObj.getMeal_data().size()>0) {
                            menuResponseCategories.addAll(mealObj.getMeal_data());
                        }
                    }
                }
            }
        }

        if (adapter != null)
            adapter.notifyDataSetChanged();

    }

    private void setRecycler() {
        orderRecycler.setLayoutManager(new LinearLayoutManager(OrderList.this));


        adapter = new MenuCategoryAdapter(OrderList.this, menuResponseCategories, menuResponseData.getWeek_color());

        adapter.listener = new OnMenuClick() {
            @Override
            public void OnMenuClick(MenuResponseDataMenu menuObject) {

                if(menuObject.isSelected())
                    mapOrder.put(menuObject.getId(),menuObject);
                else
                    if(mapOrder.containsKey(menuObject.getId())){
                         mapOrder.remove(menuObject.getId());
                    }
            }
        };

        orderRecycler.setAdapter(adapter);
        orderRecycler.setHasFixedSize(true);

    }

    private void getOrderDetail() {



        //String order_date, List<Order_data> order_data, String user_id, Patient patient, String patient_id, String hospital_id, String order_num

        if(orderNumber.length()>0){

            progressBar.setTitle("Loading..");
            progressBar.setMessage("Please Wait, Updating Order");
            progressBar.setCancelable(false);
            progressBar.show();
            orderRequest = new OrderRequest(Utility.getOrderDate(Calendar.getInstance()),
                    getOrderData(), Preference.getId(),getPatient(patientDetail),patientDetail.getId(),Preference.getHospitalId(),orderNumber);

            if(orderRequest.getOrder_data().size()>0){

                getSupportLoaderManager().initLoader(7, null, responseUpdateCallback);

            } else{

                progressBar.dismiss();
                Utility.showSnackbar(root,"Please select any product to Generate an order");
            }

        }

        //String order_date, List<Order_data> order_data, String user_id, Patient patient,  String hospital_id

        else {

            progressBar.setTitle("Loading..");
            progressBar.setMessage("Please Wait, Generating Order");
            progressBar.setCancelable(false);
            progressBar.show();

            orderRequest = new OrderRequest(Utility.getOrderDate(Calendar.getInstance()),
                    getOrderData(), Preference.getId(),getPatient(patientDetail),Preference.getHospitalId());

            if(orderRequest.getOrder_data().size()>0){

                getSupportLoaderManager().initLoader(0, null, responseCreateOrderCallback);

            } else{

                progressBar.dismiss();
                Utility.showSnackbar(root,"Please select any product to Generate an order");
            }

        }
    }

    private  com.example.moni.imenusoft.serverRequests.Patient getPatient(Patient mPatient) {

         com.example.moni.imenusoft.serverRequests.Patient serverPatientObj = new  com.example.moni.imenusoft.serverRequests.Patient();

        serverPatientObj.setFirst_name(mPatient.getFirstname());
        serverPatientObj.setLast_name(mPatient.getLastname());
        serverPatientObj.setBed_num(mPatient.getBedNo());
        serverPatientObj.setWard(mPatient.getWardNo());
        serverPatientObj.setGender(mPatient.getGender());
        serverPatientObj.setAllergies(mPatient.getFoodAllergies());
        serverPatientObj.setServe(mPatient.getServe());

        return serverPatientObj;
    }



    private ArrayList<Order_data> getOrderData() {
        ArrayList<Order_data> orderDataList = new ArrayList<>();

        if(mapOrder!=null) {
            if (mapOrder.size() > 0) {

                for (Map.Entry<String, MenuResponseDataMenu> entry : mapOrder.entrySet()) {

                    Order_data orderDataItemObj = new Order_data();

                    orderDataItemObj.setWeek_id(entry.getValue().getWeek_id());
                    orderDataItemObj.setDay_id(entry.getValue().getDay_id());
                    orderDataItemObj.setMeal_id(entry.getValue().getMeal_id());

                    orderDataItemObj.setCategory_id(entry.getValue().getCategory_id());
                    orderDataItemObj.setMenu_id(entry.getValue().getId());
                    orderDataItemObj.setQty("1");

                    orderDataList.add(orderDataItemObj);

                }
            }
        }

        return orderDataList;
    }


    private void initMap (){

        if(mapOrder== null)
            mapOrder = new HashMap<>();

    }

    private void setMapDataToArrayList(){

        if(mapOrder!=null)
        {
            if(mapOrder.size()>0){

                for (Map.Entry<String, MenuResponseDataMenu> entry : mapOrder.entrySet())
                {
                    for (MenuResponseDataDay menuResponseDataDayObj : menuResponseData.getWeek_data()) {

                        for (MenuResponseDataMeal menuResponseDataMealObj : menuResponseDataDayObj.getDay_data()) {

                            for (MenuResponseCategory menuResponseCategoryObj : menuResponseDataMealObj.getMeal_data()) {

                                for (MenuResponseDataMenu menuResponseDataMenuObj : menuResponseCategoryObj.getCategory_data()) {

                                   if(entry.getValue().getId().equalsIgnoreCase(menuResponseDataMenuObj.getId())){

                                       menuResponseDataMenuObj.setSelected(true);
                                   }
                                }
                            }
                        }
                    }

                }
            }
        }



    }


    @Override
    protected void onPause() {
        super.onPause();

        if (progressBar != null)
            progressBar.dismiss();

        this.unregisterReceiver(notificationBroadCastReceiver);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Utility.traverseToNewOrder(OrderList.this);
    }

    private void setSelector(int id){

        switch (id){

            case R.id.breakfast:

                breakfast.setAlpha(1.0f);
                lunch.setAlpha(0.6f);
                dinner.setAlpha(0.6f);

                break;

            case R.id.lunch:

                lunch.setAlpha(1.0f);
                breakfast.setAlpha(0.6f);
                dinner.setAlpha(0.6f);
                break;

            case R.id.dinner:

                dinner.setAlpha(1.0f);
                breakfast.setAlpha(0.6f);
                lunch.setAlpha(0.6f);
                break;

        }
    }

 /*   @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(resultCode==RESULT_OK){
            if(requestCode == 5000){

                if(data!=null){

                    if(data.getSerializableExtra("data_menu_list")!=null){

                    }

                    if(data.getSerializableExtra("data_patient")!=null){

                    }

                }

            }
        }
    }
*/
    private LoaderManager.LoaderCallbacks<CreateOrderResponse> responseCreateOrderCallback = new LoaderManager.LoaderCallbacks<CreateOrderResponse>() {
        @Override
        public Loader<CreateOrderResponse> onCreateLoader(int id, Bundle args) {
            return new OrderGenerateLoader(OrderList.this, orderRequest);
        }

        @Override
        public void onLoadFinished(Loader<CreateOrderResponse> loader, CreateOrderResponse data) {

            progressBar.dismiss();

            if(data!=null) {
                if (data.getSuccess()) {

                    Utility.showHelpMessages("Order Created", "Your Order "+data.getData().getOrder_num()+" is created. Please go to Order Dashboard to create another order or View your Order History.", OrderList.this, new OnDialogClick() {
                        @Override
                        public void onDialogOkClick() {

                            Utility.traverseToNewOrder(OrderList.this);
                        }

                        @Override
                        public void onDialogCancleClick() {

                        }
                    });
                  /*  Intent intent = new Intent(OrderList.this, PatientList.class);
                    intent.putExtra("data", (Serializable) data.getData());
                    intent.putExtra("data_color",menuResponseData.getWeek_color());
                    startActivityForResult(intent,5000);*/
                } else {
                    Utility.showSnackbar(root, "Ops !! something went wrong, We are trying to fix");
                }
            } else{
                Utility.showSnackbar(root, "Some thing went wrong. Try again!!");

            }

            getSupportLoaderManager().destroyLoader(0);
        }

        @Override
        public void onLoaderReset(Loader<CreateOrderResponse> loader) {

        }
    };


    private LoaderManager.LoaderCallbacks<UpdateOrderResponse> responseUpdateCallback = new LoaderManager.LoaderCallbacks<UpdateOrderResponse>() {
        @Override
        public Loader<UpdateOrderResponse> onCreateLoader(int id, Bundle args) {
            return new UpdateOrderDataLoader(OrderList.this,orderRequest);
        }

        @Override
        public void onLoadFinished(Loader<UpdateOrderResponse> loader, UpdateOrderResponse data) {

            progressBar.dismiss();


            if(data!=null) {
                if (data.getSuccess()) {

                    Utility.showHelpMessages("Order Updated", "Your Order " + orderNumber + " is updated. Please go to Order Dashboard to create another order or View your Order History.", OrderList.this, new OnDialogClick() {
                        @Override
                        public void onDialogOkClick() {

                            Utility.traverseToNewOrder(OrderList.this);
                        }

                        @Override
                        public void onDialogCancleClick() {

                        }
                    });


                } else {

                }

            } else {
                Utility.showSnackbar(root,"There might be some technical problem. Please have patience and try Again !! ");
            }

            getSupportLoaderManager().destroyLoader(7);
        }

        @Override
        public void onLoaderReset(Loader<UpdateOrderResponse> loader) {

        }
    };

    private int getCalanderDay(String dayString){

        int dayId = -1;
        switch (dayString){

            case "Monday":
                dayId = Calendar.MONDAY;
                break;

            case "Tuesday":
                dayId = Calendar.TUESDAY;
                break;

            case "Wednesday":
                dayId = Calendar.WEDNESDAY;
                break;

            case "Thursday":
                dayId = Calendar.THURSDAY;
                break;

            case "Friday":
                dayId = Calendar.FRIDAY;
                break;

            case "Saturday":
                dayId = Calendar.SATURDAY;
                break;

            case "Sunday":
                dayId = Calendar.SUNDAY;
                break;
        }

        return dayId;
    }

    private Calendar getDateOfDay(int day){

        Calendar calObj = Calendar.getInstance();

        if(calObj.get(Calendar.DAY_OF_WEEK)!=day){

            while(calObj.get(Calendar.DAY_OF_WEEK)!=day){
                calObj.add(Calendar.DATE,1);
            }

        }
        return calObj;
    }

    private String getDay(){

        String day="";
        MenuResponseDataMenu menuResponseDataMenuObj = getMenu();
        if(orderNumber.length()>0){
           if(mapOrder!=null){
               if(mapOrder.size()>0){
                   for(MenuResponseDataDay menuResponseDataDayObj :menuResponseData.getWeek_data()){

                       if(menuResponseDataDayObj.getDay_id().equalsIgnoreCase(menuResponseDataMenuObj.getDay_id())){

                           for(MenuResponseDataMeal mObj :menuResponseDataDayObj.getDay_data()){

                              if(mObj.getMeal_id().equalsIgnoreCase(menuResponseDataMenuObj.getMeal_id())){

                                   mealType = mObj.getMeal_value();
                                   day = menuResponseDataDayObj.getDay_value();

                               }
                           }
                       }
                   }
               }
           }
        }

        return day;
    }

    private MenuResponseDataMenu getMenu(){

        MenuResponseDataMenu menuResponseDataMenuObj = new MenuResponseDataMenu();
            if(mapOrder!=null) {
                if (mapOrder.size() > 0) {

                    for (Map.Entry<String, MenuResponseDataMenu> entry : mapOrder.entrySet()) {
                        menuResponseDataMenuObj= entry.getValue();
                        break;
                    }
                }
            }

            return menuResponseDataMenuObj;
    }

    @Override
    protected void onResume() {
        super.onResume();

        IntentFilter intentFilter = new IntentFilter(
                "android.intent.action.MyAction");

        notificationBroadCastReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context context, Intent intent) {
                //extract our message from intent
                String msg_for_me = intent.getStringExtra("notification");
                //log our message value
                Utility.showLog("TAG",msg_for_me);

                NotificationResponse notificationResponse = new Gson().fromJson(msg_for_me,NotificationResponse.class);


                if(notificationResponse.getChangedata()!=null){
                    if(notificationResponse.getChangedata().size()>0){

                        String position = "";


                            for(MenuResponseDataDay menuResponseDataDay :menuResponseData.getWeek_data()){

                                for(MenuResponseDataMeal menuResponseDataMeal :menuResponseDataDay.getDay_data()){

                                    for(MenuResponseCategory menuResponseCategory :menuResponseDataMeal.getMeal_data()){

                                        if(menuResponseCategory.getCategory_id().equalsIgnoreCase(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getCategory_id())){
                                            menuResponseCategory.getCategory_data().clear();
                                            menuResponseCategory.getCategory_data().addAll(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data().get(0).getCategory_data());
                                            position = menuResponseCategory.getCategory_id();
                                        }
                                    }
                                }
                        }

                        if(position.equalsIgnoreCase("")){


                                for(MenuResponseDataDay menuResponseDataDay :menuResponseData.getWeek_data()){

                                    for(MenuResponseDataMeal menuResponseDataMeal :menuResponseDataDay.getDay_data()){

                                        if(menuResponseDataMeal.getMeal_id().equalsIgnoreCase(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_id())){
                                            menuResponseDataMeal.getMeal_data().addAll(notificationResponse.getChangedata().get(0).getWeek_data().get(0).getDay_data().get(0).getMeal_data());
                                            setMealTypeData();
                                        }
                                    }
                                }
                        }


                        if(adapter!=null){


                            adapter.notifyDataSetChanged();

                        }
                    }
                }  else {
                    if(notificationResponse.getId()!=null){



                            for(MenuResponseDataDay menuResponseDataDay :menuResponseData.getWeek_data()){

                                for(MenuResponseDataMeal menuResponseDataMeal :menuResponseDataDay.getDay_data()){

                                    menuResponseDataMeal.setMeal_data(Utility.removeDataFromArrayList(menuResponseDataMeal.getMeal_data(),notificationResponse.getId()));

                                }
                            }

                    }
                }
            }
        };
        //registering our receiver
        this.registerReceiver(notificationBroadCastReceiver, intentFilter);
    }


    private String getDateByMap(String mealId){

        String dayId = "";
        if(mapOrder!=null) {
            if (mapOrder.size() > 0) {

                for (Map.Entry<String, MenuResponseDataMenu> entry : mapOrder.entrySet()) {

                      if(entry.getValue().getMeal_id().equals(mealId)){
                          dayId = entry.getValue().getDay_id();
                          break;
                      }
                }
            }
        }

        return dayId;
    }

    private String getDayByDayId(String dayId){
        String day = "";

      /*  if (dayId.length()==0)
            dayId = menuResponseData.getWeek_data().get(0).getDay_id();*/

        switch (dayId){

            case "0":
                 day = "Monday";
                 break;

            case "1":
                day = "Tuesday";
                break;

            case "2":
                day = "Wednesday";
                break;

            case "3":
                day = "Thursday";
                break;

            case "4":
                day = "Friday";
                break;

            case "5":
                day = "Saturday";
                break;

            case "6":
                day = "Sunday";
                break;

            default:
                day =Utility.getDayMonthAndDate(Calendar.getInstance()).split(" ")[0].trim();
                break;
        }

        return day;
    }
}

