package com.example.moni.imenusoft.serverRequests.orderServerRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class Data implements Serializable {
  @SerializedName("order_num")
  @Expose
  private String order_num;
  @SerializedName("order_details")
  @Expose
  private List<Order_details> order_details;
  public Data(){
  }
  public Data(String order_num, List<Order_details> order_details){
   this.order_num=order_num;
   this.order_details=order_details;
  }
  public void setOrder_num(String order_num){
   this.order_num=order_num;
  }
  public String getOrder_num(){
   return order_num;
  }
  public void setOrder_details(List<Order_details> order_details){
   this.order_details=order_details;
  }
  public List<Order_details> getOrder_details(){
   return order_details;
  }
}