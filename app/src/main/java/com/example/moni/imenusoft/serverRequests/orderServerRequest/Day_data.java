package com.example.moni.imenusoft.serverRequests.orderServerRequest;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class Day_data implements Serializable {
  @SerializedName("meal_value")
  @Expose
  private String meal_value;
  @SerializedName("meal_id")
  @Expose
  private String meal_id;
  @SerializedName("meal_data")
  @Expose
  private List<Meal_data> meal_data;

  private String dayValue;
  private String status;
  public Day_data(){
  }
  public Day_data(String meal_value, String meal_id, List<Meal_data> meal_data){
   this.meal_value=meal_value;
   this.meal_id=meal_id;
   this.meal_data=meal_data;
  }
  public void setMeal_value(String meal_value){
   this.meal_value=meal_value;
  }
  public String getMeal_value(){
   return meal_value;
  }
  public void setMeal_id(String meal_id){
   this.meal_id=meal_id;
  }
  public String getMeal_id(){
   return meal_id;
  }
  public void setMeal_data(List<Meal_data> meal_data){
   this.meal_data=meal_data;
  }
  public List<Meal_data> getMeal_data(){
   return meal_data;
  }

    public String getDayValue() {
        return dayValue;
    }

    public void setDayValue(String dayValue) {
        this.dayValue = dayValue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}