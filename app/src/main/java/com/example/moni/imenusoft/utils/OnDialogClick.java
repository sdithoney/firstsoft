package com.example.moni.imenusoft.utils;

/**
 * Created by enuke on 21/10/17.
 */

public interface OnDialogClick {
    void onDialogOkClick();
    void onDialogCancleClick();
}
