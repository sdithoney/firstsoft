package com.example.moni.imenusoft.serverRequests;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Awesome Pojo Generator
 * */
public class CancelOrderResponse{

  @SerializedName("data")
  @Expose
  private CancelOrderResponseData data;

  @SerializedName("success")
  @Expose
  private Boolean success;
  @SerializedName("massage")
  @Expose
  private String massage;

  public CancelOrderResponse(){
  }

  public CancelOrderResponse(CancelOrderResponseData data, Boolean success, String massage){
   this.data=data;
   this.success=success;
   this.massage=massage;
  }

  public CancelOrderResponse(CancelOrderResponseData data, Boolean success) {
        this.data = data;
        this.success = success;
    }

  public void setData(CancelOrderResponseData data){
   this.data=data;
  }
  public CancelOrderResponseData getData(){
   return data;
  }

  public void setSuccess(Boolean success){
   this.success=success;
  }
  public Boolean getSuccess(){
   return success;
  }

  public void setMassage(String massage){
   this.massage=massage;
  }
  public String getMassage(){
   return massage;
  }
}