package com.example.moni.imenusoft.adapters;

import android.app.Activity;
import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.moni.imenusoft.PrintActivity;
import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Category_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Day_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Meal_data;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Order_data;
import com.example.moni.imenusoft.utils.Utility;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by enuke on 4/11/17.
 */

public class PrintAdapter extends RecyclerView.Adapter<PrintAdapter.ViewHolder> {


    Context context;
    ArrayList<Day_data> facilityList;
    private Map<String, String> orderStatus;



    public PrintAdapter(Context context, ArrayList<Day_data> facilityList) {
        this.context = context;
        this.facilityList = facilityList;
        orderStatus = new HashMap<>();

        orderStatus.put("0", "Created");
        orderStatus.put("1", "Completed");
        orderStatus.put("2", "Canceled");

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.print_category_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Day_data day_data = facilityList.get(holder.getPosition());

       // holder.mMealHeader.setText(day_data.getMeal_value()+"\n ("+day_data.getDayValue()+")");

        PrintActivity printActivity = (PrintActivity) context;
        if(printActivity!=null){
            holder.mMealHeader.setText( day_data.getMeal_value()+" ("+ Utility.getYYYYMMDD(printActivity.getDateOfDay(printActivity.getCalanderDay(day_data.getDayValue())))+" "+day_data.getDayValue()+")");
        }

        holder.mData.setText("");
        for (Meal_data meal_data : day_data.getMeal_data()) {
            for (Order_data order_data : meal_data.getOrder_data()) {

                holder.mData.append(""+order_data.getCategory_value()+ " ");

                for (Category_data category_data : order_data.getCategory_data()) {

                    holder.mData.append("*"+category_data.getMenu_name() +" ");
                }
            }
        }
    }

    @Override
    public int getItemCount() {
        return facilityList.size();
    }



    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mMealHeader;
        private TextView mData;

        public ViewHolder(View convertView) {
            super(convertView);


            mMealHeader = convertView.findViewById(R.id.meal_header);
            mData = convertView.findViewById(R.id.data);
        }
    }
}
