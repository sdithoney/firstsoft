package com.example.moni.imenusoft.firebasenotification.notificationJson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Awesome Pojo Generator
 * */
public class Week_data{
  @SerializedName("day_value")
  @Expose
  private String day_value;
  @SerializedName("day_data")
  @Expose
  private List<Day_data> day_data;
  @SerializedName("day_id")
  @Expose
  private String day_id;
  public void setDay_value(String day_value){
   this.day_value=day_value;
  }
  public String getDay_value(){
   return day_value;
  }
  public void setDay_data(List<Day_data> day_data){
   this.day_data=day_data;
  }
  public List<Day_data> getDay_data(){
   return day_data;
  }
  public void setDay_id(String day_id){
   this.day_id=day_id;
  }
  public String getDay_id(){
   return day_id;
  }
}