package com.example.moni.imenusoft.data;

import android.app.Application;

import com.facebook.drawee.backends.pipeline.Fresco;

/**
 * Created by enuke on 29/9/17.
 */

public class ApplicationController extends Application {

    public static final String TAG = ApplicationController.class
            .getSimpleName();

    private static ApplicationController instance;

    public static ApplicationController getInstance() {
        if (instance == null)
            throw new IllegalStateException();
        return instance;
    }

    private void initFasco() {
        Fresco.initialize(this);
    }

    @Override
    public void onCreate() {
        super.onCreate();

        instance = this;
        initFasco();
    }
}
