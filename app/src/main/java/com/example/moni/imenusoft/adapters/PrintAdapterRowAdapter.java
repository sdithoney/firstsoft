package com.example.moni.imenusoft.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.serverRequests.orderServerRequest.Category_data;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by enuke on 4/11/17.
 */

public class PrintAdapterRowAdapter extends RecyclerView.Adapter<PrintAdapterRowAdapter.ViewHolder> {


    Context context;
    ArrayList<Category_data> facilityList;
    private Map<String, String> orderStatus;



    public PrintAdapterRowAdapter(Context context, ArrayList<Category_data> facilityList) {
        this.context = context;
        this.facilityList = facilityList;
        orderStatus = new HashMap<>();

        orderStatus.put("0", "Created");
        orderStatus.put("1", "Completed");
        orderStatus.put("2", "Canceled");

    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.print_menu_row, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {

        Category_data category_data = facilityList.get(holder.getPosition());

        holder.mMenuName.setText("Menu :"+category_data.getMenu_name());
        holder.mMenuCat.setText("Category : " + category_data.getCategory());
        holder.mMenuStatus.setText("Current Status : " + orderStatus.get(category_data.getStatus()));
        holder.mMenuDay.setText("Order By :" + category_data.getDay());

    }


    @Override
    public int getItemCount() {
        return facilityList.size();
    }

    private void initView() {

    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        private TextView mMealHeader;
        private TextView mMenuName;
        private TextView mMenuCat;
        private TextView mMenuDay;
        private TextView mMenuStatus;


        public ViewHolder(View convertView) {
            super(convertView);

            mMealHeader = convertView.findViewById(R.id.meal_header);
            mMenuName = convertView.findViewById(R.id.menuName);
            mMenuCat = convertView.findViewById(R.id.menuCat);
            mMenuDay = convertView.findViewById(R.id.menuDay);
            mMenuStatus = convertView.findViewById(R.id.menuStatus);
        }
    }

}
