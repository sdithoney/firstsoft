package com.example.moni.imenusoft;

import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.ImageView;

import  com.example.moni.imenusoft.R;
import  com.example.moni.imenusoft.utils.NetworkChangeReceiver;
import  com.example.moni.imenusoft.utils.NetworkStateChanged;
import  com.example.moni.imenusoft.utils.Preference;
import  com.example.moni.imenusoft.utils.Utility;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

public class MainActivity extends AppCompatActivity {

    private ImageView img;
    EventBus eventBus = EventBus.getDefault();
    BroadcastReceiver networkReceiver ;
    IntentFilter intentfilter;

    AlertDialog alert;

    int SPLASH_TIME = 3000;



    @Override
    protected void onPause() {
        super.onPause();

        if(alert!=null){
            alert.dismiss();
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        initView();

        intentfilter = new IntentFilter();

        intentfilter.addAction("android.net.conn.CONNECTIVITY_CHANGE");

        networkReceiver = new NetworkChangeReceiver();

        registerReceiver(networkReceiver,intentfilter);
        eventBus.register(this);
    }

    private void initView() {
        img = findViewById(R.id.img);
    }


    @Override
    protected void onStop() {
        super.onStop();

        eventBus.unregister(this);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        if(networkReceiver != null)
            unregisterReceiver(networkReceiver);

        eventBus.unregister(this);
    }

    public void showAlert() {
        alert = new AlertDialog.Builder(this).create();
        alert.setCancelable(false);
        alert.setTitle("No Internet");
        alert.setMessage("Please connect to WIFI or switch on your mobile data to get connected to internet.");

        alert.setButton(DialogInterface.BUTTON_POSITIVE, "Setting", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                startActivity(new Intent(WifiManager.ACTION_PICK_WIFI_NETWORK));
            }
        });

        alert.setButton(DialogInterface.BUTTON_NEGATIVE, " Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                finish();
            }
        });
        alert.show();
    }
    @Subscribe
    public void onEvent(NetworkStateChanged event) {
        if (!event.isInternetConnected()) {
            showAlert();//Toast.makeText(this, "No Internet connection!", Toast.LENGTH_SHORT).show();
        }
        else
            new BackgroundTask().execute();
    }

    private class BackgroundTask extends AsyncTask {
        Intent intent;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //  intent = new Intent(SplashScreen.this,MainActivity.class);
        }

        @Override
        protected Object doInBackground(Object[] params) {
    /* Use this method to load background
    * data that your app needs. */
            // if (!Utility.isInternetConnected(SplashScreen.this)) {
            try {
                Log.i("thread time","ok");
                Thread.sleep(SPLASH_TIME);
            } catch (Exception e) {
                e.printStackTrace();
                // }
            }
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            super.onPostExecute(o);
            if (Utility.isInternetConnected(MainActivity.this)) {

                Utility.showLog("Value",""+Preference.isLogin());

                if(Preference.isLogin()){
                    Utility.traverseToHomePage(MainActivity.this);
                } else {
                    Utility.traverseToLogin(MainActivity.this);
                }

            } else {
                showAlert();
            }
        }
    }

}
