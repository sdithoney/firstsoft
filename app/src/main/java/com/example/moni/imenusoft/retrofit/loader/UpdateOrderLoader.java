package com.example.moni.imenusoft.retrofit.loader;

import android.content.Context;
import android.support.v4.content.AsyncTaskLoader;
import android.util.Log;

import  com.example.moni.imenusoft.retrofit.CommunicationLink;
import  com.example.moni.imenusoft.serverRequests.CancelOrderResponse;
import  com.example.moni.imenusoft.serverRequests.CancelRequest;
import  com.example.moni.imenusoft.serverRequests.UpdateOrderRequest;
import  com.example.moni.imenusoft.serverRequests.UpdateOrderResponse;
import com.google.gson.GsonBuilder;

/**
 * Created by enuke on 20/10/17.
 */

public class UpdateOrderLoader extends AsyncTaskLoader<UpdateOrderResponse> {

    private Context mContext;
    private String accessToken;
    private int interestId;
    UpdateOrderRequest jsonObject;

    public UpdateOrderLoader(Context context, UpdateOrderRequest jsonObject) {

        super(context);
        mContext = context;
        this. accessToken=accessToken;
        this.interestId=interestId;
        this.jsonObject = jsonObject;
    }

    @Override
    public UpdateOrderResponse loadInBackground() {
        try {

            String json = new GsonBuilder().create().toJson(jsonObject);
            Log.i("request",""+json);
            UpdateOrderResponse responsePlan = CommunicationLink.actionUpdateOrder(jsonObject);
            return responsePlan;

        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected void onStartLoading() {
        forceLoad();
    }

}
